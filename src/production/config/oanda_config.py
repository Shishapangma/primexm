# Live FIX account
# MarketCompID = 'PROD.MKT.AU000001'
# TraderCompID = 'PROD.TRD.AU000001'
# TargetCompID = 'LFX'
# MarketSocketConnectPort = 9721
# TradeSocketConnectPort = 9720
# SocketConnectHost = '172.97.124.146'

# # Demo FIX account
MarketCompID = 'UAT2.MKT.PAUL'
TraderCompID = 'UAT2.TRD.PAUL'
TargetCompID = 'LFX'
MarketSocketConnectPort = 9721
TradeSocketConnectPort = 9720
SocketConnectHost = '172.97.124.157'

# Demo REST account
ENVIRONMENTS = {
    "streaming": {
        "real": "stream-fxtrade.oanda.com",
        "practice": "stream-fxpractice.oanda.com",
        "sandbox": "stream-sandbox.oanda.com"
    },
    "api": {
        "real": "api-fxtrade.oanda.com",
        "practice": "api-fxpractice.oanda.com",
        "sandbox": "api-sandbox.oanda.com"
    }
}
DOMAIN = 'practice'
STREAM_DOMAIN = ENVIRONMENTS["streaming"][DOMAIN]
API_DOMAIN = ENVIRONMENTS["api"][DOMAIN]
OANDA_ACCESS_TOKEN = '6f2cf9ed9796ac6be0c61afe4504e5a7-cbc307faa9d28899d78e42a1ea7b3ba7'
ACCOUNT_ID = 5977681
