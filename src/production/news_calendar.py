import pandas as pd
import numpy as np
from datetime import datetime

import src.production.settings as SET
import src.production.stat_functions as sf
from src.production.fx_logger import logger


class Calender:
    MAX_LIFE = 25
    # decay duration
    ACTIVE = 0
    # default stats is deactive
    DEACTIVE = 1
    # pre action decay completed
    PRE_ACTION_EXPIRED = 2
    # during between PRE_ACTION_EXPIRED and re ACTIVE
    ACTIVE_HALT = 3
    # Post action decay completed
    POST_ACTION_EXPIRED = 4
    # full completed
    END = 5
    # internally using for indicating decay direction
    PRE_ACTION = 0
    POST_ACTION = 1

    def __init__(self, instrument, news, pre_action_time, post_action_time, t_time=None):
        self.instrument = instrument
        self.news = news
        self.stat = Calender.DEACTIVE
        self.pre_action_time = pre_action_time
        self.post_action_time = post_action_time
        self.flag = Calender.PRE_ACTION
        self.left_time = 0

        self.initial_decay(t_time)

    def decay(self):
        # positive decay- decrease
        if self.flag == Calender.PRE_ACTION:
            self.left_time += 1
            if self.left_time >= Calender.MAX_LIFE:
                self.stat = Calender.PRE_ACTION_EXPIRED
                self.flag = Calender.POST_ACTION
        # negative decay-increase
        elif self.flag == Calender.POST_ACTION:
            self.left_time -= 1
            if self.left_time <= 0:
                self.stat = Calender.POST_ACTION_EXPIRED

    def initial_decay(self, t_time=None):
        """
        :param t_time: format: '2017-05-08 22:00:00'. Indicate if any specific starting point
        :return: 
        """
        # specific start point
        if t_time:
            utc_now = datetime.strptime(t_time, '%Y%m%d %H:%M:%S')
            current_utc_time = pd.to_datetime(utc_now)
            current_utc_time = current_utc_time.tz_localize('UTC')
        # current time
        else:
            utc_now = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
            current_utc_time = pd.to_datetime(utc_now)
            current_utc_time = current_utc_time.tz_localize('UTC')

        time_elapse = (current_utc_time - self.pre_action_time).total_seconds()

        # current time before pre_action. Do nothing
        if time_elapse < 0:
            pass
        # current time beyond pre_action, but still processing decay
        elif time_elapse > 0 and int(time_elapse / 60) <= Calender.MAX_LIFE:
            self.stat = Calender.ACTIVE
            self.left_time = int(time_elapse / 60)
            logger.info('%s %s PRE Activated with %s' % (self.instrument, self.news, self.left_time))
        # Pre action expired and still earlier than post action time
        elif int(time_elapse / 60) >= Calender.MAX_LIFE and current_utc_time < self.post_action_time:
            self.stat = Calender.ACTIVE_HALT
            self.left_time = Calender.MAX_LIFE
            self.flag = Calender.POST_ACTION
            logger.info('%s %s ACTIVE HALT' % (self.instrument, self.news))
        # current time after post_action but before expired
        elif current_utc_time >= self.post_action_time and int(
                ((current_utc_time - self.post_action_time).total_seconds() / 60)) < Calender.MAX_LIFE:
            self.left_time = Calender.MAX_LIFE - int(((current_utc_time - self.post_action_time).total_seconds() / 60))
            self.flag = Calender.POST_ACTION
            self.stat = Calender.ACTIVE
            logger.info('%s %s POST Activated with %s' % (self.instrument, self.news, self.left_time))
        # post action end
        elif current_utc_time >= self.post_action_time and int(
                ((current_utc_time - self.post_action_time).total_seconds() / 60)) >= Calender.MAX_LIFE:
            self.stat = Calender.END
            self.left_time = 0
            logger.info('%s %s POST EXPIRED' % (self.instrument, self.news))


class InstrumentCalender:
    """
    Calender manager is able to deal with multiple news parallel
    """

    def __init__(self, instrument):
        self.instrument = instrument
        self.calender_list = []
        self.alpha_weigh_list = []
        self.exp_decay_half_life = sf.get_exponential_weight(10)
        self.current_exp_weight = 1

    def update_alpha_weight(self):
        """
        Update multiple ongoing news
        :return: the restrict alpha weight will be return
        """
        for action_calender in self.calender_list:
            if action_calender.stat == Calender.ACTIVE:
                self.alpha_weigh_list[
                    self.calender_list.index(action_calender)] = self.exp_decay_half_life ** action_calender.left_time
                self.current_exp_weight = min(self.alpha_weigh_list)
            elif action_calender.stat == Calender.PRE_ACTION_EXPIRED:
                self.alpha_weigh_list[self.calender_list.index(action_calender)] = 0
                action_calender.stat = Calender.ACTIVE_HALT
                self.current_exp_weight = min(self.alpha_weigh_list)
            elif action_calender.stat == Calender.ACTIVE_HALT:
                self.alpha_weigh_list[self.calender_list.index(action_calender)] = 0
                self.current_exp_weight = min(self.alpha_weigh_list)
            elif action_calender.stat == Calender.POST_ACTION_EXPIRED:
                self.alpha_weigh_list[self.calender_list.index(action_calender)] = 1
                action_calender.stat = Calender.END
                self.current_exp_weight = min(self.alpha_weigh_list)

        return self.current_exp_weight


class EventCalendar:
    """
    One instance manage all instruments calender(InstrumentCalender) instance
    """
    # 3 hours in advance of event in seconds to take action
    NEWS_PRE_ACTION_RANGE = 1800
    # 2.5 hours beyond of event in seconds to restore
    NEWS_POST_ACTION_RANGE = 3600

    def __init__(self, t_time=None):
        # Thread.__init__(self)
        self.calenders = {}
        self.instrument_weight = {}
        for instrument in SET.INSTRUMENT:
            self.calenders[instrument] = InstrumentCalender(instrument)
            self.instrument_weight[instrument] = 1
        for instrument in SET.CALENDER:
            news_list = SET.CALENDER[instrument]
            for news in news_list:
                news_date = pd.to_datetime(news)
                news_date = news_date.tz_localize('UTC')
                pre_action_date = news_date - pd.offsets.Second(EventCalendar.NEWS_PRE_ACTION_RANGE)
                post_action_date = news_date + pd.offsets.Second(EventCalendar.NEWS_POST_ACTION_RANGE)
                self.calenders[instrument].calender_list.append(
                    Calender(instrument, news, pre_action_date, post_action_date, t_time))
                self.calenders[instrument].alpha_weigh_list.append(self.calenders[instrument].current_exp_weight)

        self.active = True

    # def run(self):
    #     """
    #     Thread run
    #     :return:
    #     """
    #     while self.active:
    #         self.update_alpha_weight()
    #         time.sleep(60)

    def update_alpha_weight(self, instrument=None, t_date=None, t_time=None):
        """
        apply market data to trigger time relative action
        call this method after trading weight update, it will cause one minute lag but avoid trading order generating delay.
        :param event:
        :return:
        """
        if np.logical_and(t_date is not None, t_time is not None):
            current_utc_time = datetime.strptime((t_date + ' ' + t_time), '%Y%m%d %H:%M:%S.%f')
            current_utc_time = pd.to_datetime(current_utc_time)
            current_utc_time = current_utc_time.tz_localize('UTC')
        else:
            current_utc_time = pd.to_datetime(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'))
            current_utc_time = current_utc_time.tz_localize('UTC')

        # Common News Event
        if instrument:
            instrument_calender = self.calenders[instrument]
            # action section
            for action_calender in instrument_calender.calender_list:
                pre_action_time = action_calender.pre_action_time
                post_action_time = action_calender.post_action_time
                if (
                            pre_action_time - current_utc_time).total_seconds() <= 0 and action_calender.stat == Calender.DEACTIVE:
                    action_calender.stat = Calender.ACTIVE
                    logger.info('%s %s PRE Activated' % (instrument, action_calender.news))
                # Calender life elapsed after activated
                elif (
                            current_utc_time - post_action_time).total_seconds() >= 0 and action_calender.stat == Calender.ACTIVE_HALT:
                    action_calender.stat = Calender.ACTIVE
                    logger.info('%s %s POST_ACTION Activated' % (instrument, action_calender.news))
                elif action_calender.stat == Calender.ACTIVE:
                    action_calender.decay()
                    logger.info(
                        '%s %s ACTION Decay: %s' % (instrument, action_calender.news, str(action_calender.left_time)))

            news_alpha_weight = instrument_calender.update_alpha_weight()

            # Overnight Event
            overnight_alpha_weight = np.nan
            if current_utc_time.hour == SET.OVERNIGHT['OVERNIGHT_HOUR_PRE']:
                if current_utc_time.minute > SET.OVERNIGHT['CUT_OFF_MIN_PRE']:
                    overnight_alpha_weight = 0
                else:
                    overnight_alpha_weight = instrument_calender.exp_decay_half_life ** current_utc_time.minute
            elif current_utc_time.hour == SET.OVERNIGHT['OVERNIGHT_HOUR_POST']:
                if current_utc_time.minute < SET.OVERNIGHT['CUT_OFF_MIN_POST']:
                    overnight_alpha_weight = 0
                else:
                    overnight_alpha_weight = instrument_calender.exp_decay_half_life ** (60 - current_utc_time.minute)

            if not np.isnan(overnight_alpha_weight):
                # logger.info('%s weight: %s' % (event.instrument, str(min(news_alpha_weight, overnight_alpha_weight))))
                self.instrument_weight[instrument] = min(news_alpha_weight, overnight_alpha_weight)
            else:
                # logger.info('%s weight: %s' % (event.instrument, str(news_alpha_weight)))
                self.instrument_weight[instrument] = news_alpha_weight

        else:
            for instrument in SET.INSTRUMENT:
                instrument_calender = self.calenders[instrument]
                # action section
                for action_calender in instrument_calender.calender_list:
                    pre_action_time = action_calender.pre_action_time
                    post_action_time = action_calender.post_action_time
                    if (
                        pre_action_time - current_utc_time).total_seconds() <= 0 and action_calender.stat == Calender.DEACTIVE:
                        action_calender.stat = Calender.ACTIVE
                        logger.info('%s %s PRE Activated' % (instrument, action_calender.news))
                    # Calender life elapsed after activated
                    elif (
                        current_utc_time - post_action_time).total_seconds() >= 0 and action_calender.stat == Calender.ACTIVE_HALT:
                        action_calender.stat = Calender.ACTIVE
                        logger.info('%s %s POST_ACTION Activated' % (instrument, action_calender.news))
                    elif action_calender.stat == Calender.ACTIVE:
                        action_calender.decay()
                        logger.info(
                            '%s %s ACTION Decay: %s' % (instrument, action_calender.news, str(action_calender.left_time)))

                news_alpha_weight = instrument_calender.update_alpha_weight()

                # Overnight Event
                overnight_alpha_weight = np.nan
                if current_utc_time.hour == SET.OVERNIGHT['OVERNIGHT_HOUR_PRE']:
                    if current_utc_time.minute > SET.OVERNIGHT['CUT_OFF_MIN_PRE']:
                        overnight_alpha_weight = 0
                    else:
                        overnight_alpha_weight = instrument_calender.exp_decay_half_life ** current_utc_time.minute
                elif current_utc_time.hour == SET.OVERNIGHT['OVERNIGHT_HOUR_POST']:
                    if current_utc_time.minute < SET.OVERNIGHT['CUT_OFF_MIN_POST']:
                        overnight_alpha_weight = 0
                    else:
                        overnight_alpha_weight = instrument_calender.exp_decay_half_life ** (60 - current_utc_time.minute)

                if not np.isnan(overnight_alpha_weight):
                    # logger.info('%s weight: %s' % (event.instrument, str(min(news_alpha_weight, overnight_alpha_weight))))
                    self.instrument_weight[instrument] = min(news_alpha_weight, overnight_alpha_weight)
                else:
                    # logger.info('%s weight: %s' % (event.instrument, str(news_alpha_weight)))
                    self.instrument_weight[instrument] = news_alpha_weight

    def get_alpha_weight(self, instrument):
        """
        feedback current weight with specific instrument
        :param instrument: 
        :return: 
        """
        return self.instrument_weight[instrument]
