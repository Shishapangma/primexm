from enum import Enum


class OrderType(Enum):
    MARKET_ORDER = 'MARKET'
    LIMIT_ORDER = 'LIMIT'


##########################################################
######### ROUTINE MAINTAINING PARAMETERS ################
INSTRUMENT = ['AUD/USD', 'EUR/CHF', 'EUR/GBP', 'GBP/USD', 'EUR/USD', 'USD/SGD']

# 'OrderType.LIMIT_ORDER'
INSTRUMENT_ORDER_TYPE = {'AUD/USD': [OrderType.MARKET_ORDER],
                         'EUR/CHF': [OrderType.MARKET_ORDER],
                         'EUR/GBP': [OrderType.MARKET_ORDER],
                         'GBP/USD': [OrderType.MARKET_ORDER],
                         'EUR/USD': [OrderType.MARKET_ORDER],
                         'USD/SGD': [OrderType.MARKET_ORDER], }

EXCHANGE_RATE = {'AUD/USD': 1.0,
                 'EUR/CHF': 1.0,
                 'EUR/GBP': 1.0,
                 'GBP/USD': 1.0,
                 'EUR/USD': 1.0,
                 'USD/SGD': 1.0,
                 }

REMAIN_POSITION = {'AUD/USD': {OrderType.LIMIT_ORDER: 0, OrderType.MARKET_ORDER: 0},
                   'EUR/CHF': {OrderType.LIMIT_ORDER: 0, OrderType.MARKET_ORDER: 0},
                   'EUR/GBP': {OrderType.LIMIT_ORDER: 0, OrderType.MARKET_ORDER: 0},
                   'GBP/USD': {OrderType.LIMIT_ORDER: 0, OrderType.MARKET_ORDER: 0},
                   'EUR/USD': {OrderType.LIMIT_ORDER: 0, OrderType.MARKET_ORDER: 0},
                   'USD/SGD': {OrderType.LIMIT_ORDER: 0, OrderType.MARKET_ORDER: 0},

                   }

X = {'AUD/USD': 0.00001,
     'EUR/CHF': 0.00001,
     'EUR/GBP': 0.00001,
     'GBP/USD': 0.00001,
     'EUR/USD': 0.00001,
     'USD/SGD': 0.00001,
     }

VALVE = {'AUD/USD': 0.00001,
         'EUR/CHF': 0.00001,
         'EUR/GBP': 0.00001,
         'GBP/USD': 0.00001,
         'EUR/USD': 0.00001,
         'USD/SGD': 0.00001,
         }

INITIAL_NET_ASSET = 10.0e5

# '20170407-12:30:00'

# CALENDER = {}



CALENDER = {
    'AUD/USD': ['20180708-21:00:00', '20180708-22:00:00',
                '20180709-01:30:00', '20180709-07:00:00',
                '20180710-01:30:00', '20180710-07:00:00',
                '20180711-01:30:00', '20180711-07:00:00',
                '20180712-01:30:00', '20180712-07:00:00',
                '20180713-01:30:00', '20180713-07:00:00'],

    'EUR/CHF': ['20180708-21:00:00', '20180708-22:00:00',
                '20180709-07:00:00',
                '20180710-07:00:00',
                '20180711-07:00:00',
                '20180712-07:00:00',
                '20180713-07:00:00'],

    'EUR/GBP': ['20180708-21:00:00', '20180708-22:00:00',
                '20180709-07:00:00', '20180709-08:00:00',
                '20180710-07:00:00', '20180710-08:00:00',
                '20180711-07:00:00', '20180711-08:00:00',
                '20180712-07:00:00', '20180712-08:00:00',
                '20180713-07:00:00', '20180713-08:00:00'],

    'GBP/USD': ['20180708-21:00:00', '20180708-22:00:00',
                '20180709-07:00:00', '20180709-08:00:00',
                '20180710-07:00:00', '20180710-08:00:00',
                '20180711-07:00:00', '20180711-08:00:00',
                '20180712-07:00:00', '20180712-08:00:00',
                '20180713-07:00:00', '20180713-08:00:00'],

    'EUR/USD': ['20180708-21:00:00', '20180708-22:00:00',
                '20180709-07:00:00',
                '20180710-07:00:00',
                '20180711-07:00:00',
                '20180712-07:00:00',
                '20180713-07:00:00'],

    'USD/SGD': ['20180708-21:00:00', '20180708-22:00:00',
                '20180709-00:00:00', '20180709-18:00:00', '20180709-19:00:00', '20180709-20:00:00',
                '20180710-00:00:00', '20180710-18:00:00', '20180710-19:00:00', '20180710-20:00:00',
                '20180711-00:00:00', '20180711-18:00:00', '20180711-19:00:00', '20180711-20:00:00',
                '20180712-00:00:00', '20180712-18:00:00', '20180712-19:00:00', '20180712-20:00:00',
                '20180713-00:00:00', '20180713-18:00:00', '20180713-19:00:00', '20180713-20:00:00']}

ALPHA_WINSORIZE = {'AUD/USD': 2.5,
                   'EUR/CHF': 5,
                   'EUR/GBP': 3,
                   'GBP/USD': 2.5,
                   'EUR/USD': 2.5,
                   'USD/SGD': 7.5,
                   }

ALPHA_SCALE = {'AUD/USD': 2.5,
               'EUR/CHF': 5,
               'EUR/GBP': 3,
               'GBP/USD': 2.5,
               'EUR/USD': 2.5,
               'USD/SGD': 7.5,
               }

KAPPA = {'AUD/USD': 0.5,
         'EUR/CHF': 1,
         'EUR/GBP': 0.6,
         'GBP/USD': 0.5,
         'EUR/USD': 0.5,
         'USD/SGD': 1.5,
         }

###########################################################
# Dummy Tick Data
###########################################################
DUMMY_INSTRUMENT = ['AUD/USD', 'EUR/CHF', 'EUR/GBP', 'GBP/USD', 'EUR/USD', 'USD/SGD', 'AUD/NZD', 'NZD/USD', 'AUD/CAD',
                    'AUD/JPY']

###########################################################
###########################################################

OVERNIGHT = {'OVERNIGHT_HOUR_PRE': 20, 'OVERNIGHT_HOUR_POST': 21, 'CUT_OFF_MIN_PRE': 30, 'CUT_OFF_MIN_POST': 30}

# strategy Based Parameters
DURATION = 60
UNIT_SIZE = 10000
INITIAL_WEIGHT = 1
SHORT_HALF_LIFE = 20
LONG_HALF_LIFE = 200
SPREAD_FREQ = 1
SPREAD_HALF_LIFE = 10
POSITION_CLEAN = -100
ORDER_EXPIRED_TIME = 65

# Database
MONOGO_URI = URI = 'mongodb://shishapangmatechnologies:Shishapangma2017@cluster0-shard-00-00-a9fqu.mongodb.net:27017,cluster0-shard-00-01-a9fqu.mongodb.net:27017,cluster0-shard-00-02-a9fqu.mongodb.net:27017/local?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin'
MONOGO_LMAX_TICK_DATABASE = 'LMAX_Tick'
MONOGO_PRIMEXM_TICK_DATABASE = 'PRIMEXM_Tick'
