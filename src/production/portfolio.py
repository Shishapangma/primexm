import numpy as np
from threading import Timer
from src.production.event import LimitOrderEvent, MarketOrderEvent
from src.production.performance import PerformanceMonitor
import src.production.settings as SET
from src.production.fx_logger import logger

class Portfolio(object):
    def __init__(self, instrument, data_house, event_queue, order_type):
        self.instrument = instrument
        # data source handler
        self.data_house = data_house

        if self.instrument.count('jpy') == 1:
            self.precise = 3
        else:
            self.precise = 5

        # performance monitor handler
        self.perf_monitor = PerformanceMonitor(self, event_queue, order_type)

        # initial position is Zero
        self.current_position = 0
        self.current_weight = 0

        # order type indicator
        self.order_type = order_type

        #event queue
        self.event_queue = event_queue

        # split order list
        self.order_list = []
        # order side
        self.order_side = ''

    def init_trading(self, net_asset_value, exchange_price, position):
        self.net_asset_value = net_asset_value
        self.exchange_price = exchange_price
        self.perf_monitor.last_current_position = position
        # load the current position
        # self.update_current_weight()

    def weight_to_dollar_position(self, weight, price):
        """
        Cal funding currency position by multiple weight
        Covert funding currency position to trading currency position
        :param weight:
        :return: trading currency position
        """
        return (weight * self.net_asset_value * self.exchange_price) / price

    def dollar_position_to_weight(self, price):
        """
        Convert trading currency position to funding currency position
        Convert funding currency position to weight
        :param
        :return: funding currency weight
        """
        return (self.current_position / self.exchange_price / self.net_asset_value) * price

    def get_current_weight(self):
        """
        Fetch existing quantity of units and convert into weight
        Prefer to fetch real current quantity of units than using static update
        especially for limit order which the order will not be executed every time
        :return: current weight
        """
        return self.current_weight

    def update_current_weight(self, price):
        """
        split udpate from get. update may happen at different chance
        :return:
        """
        self.current_position = self.perf_monitor.last_current_position
        self.current_weight = self.dollar_position_to_weight(price)

    def generate_trade_order(self, alpha_result, tick_event):
        """
        Calculate new position based on target weight
        if new position differ with the current position then generate order to fill it
        :param alpha_result: 
        :param tick_event: 
        :return: 
        """

        current_weight = alpha_result['current_weight']
        target_weight = alpha_result['target_weight']
        if current_weight != target_weight and target_weight != SET.POSITION_CLEAN:
            target_position = np.int(self.weight_to_dollar_position(target_weight, (tick_event.ask + tick_event.bid) / 2))
            trade_position = target_position - self.current_position

            # if remaining position is less then minimum requirement, clean in one go
            if alpha_result['clean'] and np.abs(target_position) < 10000:
                logger.info('target position is less then minimum requirement %s' % target_position)
                target_position = 0
                trade_position = target_position - self.current_position

            logger.info('[Position] [%s]  Trade Position:%s' % (self.instrument, str(trade_position)))
            # position has to be modified
            if trade_position != 0:
                units = np.abs(trade_position)
                # Sell
                if np.sign(trade_position) == -1:
                    if self.order_type == SET.OrderType.LIMIT_ORDER:
                        # ask - offset
                        price = round((tick_event.ask - SET.X[self.instrument]), self.precise)
                        # system auto limit order price update
                        # mid price
                        # price = round((tick_event.ask + tick_event.bid) / 2, self.precise)
                        # rate_valve = self.valve_create(tick_event.ask, tick_event.bid, price, '2')
                        # pegged order at better price
                        # rate_valve = self.better_price(tick_event.ask, tick_event.bid, price, '2')
                        return LimitOrderEvent(self.instrument, units, '2', price)
                    else:
                        # return MarketOrderEvent(self.instrument, units, '2')
                        self.order_side = '2'
                        self.split_order(units)
                        self.send_order()
                # Buy
                else:
                    if self.order_type == SET.OrderType.LIMIT_ORDER:
                        # bid + offset
                        price = round((tick_event.bid + SET.X[self.instrument]), self.precise)
                        # mid price
                        # price = round((tick_event.ask + tick_event.bid) / 2, self.precise)
                        # rate_valve = self.valve_create(tick_event.ask, tick_event.bid, price, '1')
                        # pegged order at better price
                        # rate_valve = self.better_price(tick_event.ask, tick_event.bid, price, '1')
                        return LimitOrderEvent(self.instrument, units, '1', price)
                    else:
                        # return MarketOrderEvent(self.instrument, units, '1')
                        self.order_side = '1'
                        self.split_order(units)
                        self.send_order()
            else:
                return None

        # sepcial case for position empty
        elif target_weight == SET.POSITION_CLEAN:
            units = np.abs(self.current_position)
            # short position, buy back
            if np.sign(self.current_position) == -1:
                if self.order_type == SET.OrderType.LIMIT_ORDER:
                    # ask - offset
                    # price = round((tick_event.bid + SET.X[self.instrument]), self.precise)
                    # mid price
                    price = round((tick_event.ask + tick_event.bid) / 2, self.precise)
                    # rate_valve = self.valve_create(tick_event.ask, tick_event.bid, price, '1')
                    # pegged order at better price
                    rate_valve = self.better_price(tick_event.ask, tick_event.bid, price, '1')
                    return LimitOrderEvent(self.instrument, units, '1', price, False, rate_valve, True)
                else:
                    # return MarketOrderEvent(self.instrument, units, '1')
                    self.order_side = '1'
                    self.split_order(units)
                    self.send_order()
            # long position, sell out
            elif np.sign(self.current_position) == 1:
                if self.order_type == SET.OrderType.LIMIT_ORDER:
                    # buy + offset
                    # price = round((tick_event.ask - SET.X[self.instrument]), self.precise)
                    # mid price
                    price = round((tick_event.ask + tick_event.bid) / 2, self.precise)
                    # rate_valve = self.valve_create(tick_event.ask, tick_event.bid, price, '2')
                    # pegged order at better price
                    rate_valve = self.better_price(tick_event.ask, tick_event.bid, price, '2')
                    return LimitOrderEvent(self.instrument, units, '2', price, False, rate_valve, True)
                else:
                    # return MarketOrderEvent(self.instrument, units, '2')
                    self.order_side = '2'
                    self.split_order(units)
                    self.send_order()
            # NO position at all, do nothing
            else:
                return None
        else:
            return None

    def valve_create(self, ask, bid, price, side):

        offset = -1 * SET.VALVE[self.instrument] if side == '2' else SET.VALVE[self.instrument]
        if side == '2':
            offset = -1 * SET.VALVE[self.instrument]
            return min(ask, round((offset + price), self.precise))
        else:
            offset = SET.VALVE[self.instrument]
            return max(bid, round((offset + price), self.precise))

    def better_price(self, ask, bid, price, side):
        """
        pegged limit order at better price
        :param ask:
        :param bid:
        :param price:
        :param side:
        :return:
        """

        return ask if side == '1' else bid

    def split_order(self, units):
        """
        split a big order into several smaller orders and put them into queue
        :param units:
        :return:
        """
        #prod
        MAX_SIZE = 1000000
        MIN_SIZE = 1000

        #uta
        # MAX_SIZE = 10000
        # MIN_SIZE = 1000

        # must clean residual position with any reason from last trade, otherwise amount of position
        self.order_list.clear()

        steps = units / MAX_SIZE

        import math
        n = math.modf(steps)

        t = int(round(n[0] * MAX_SIZE / MIN_SIZE, 0) * MIN_SIZE)
        if t != 0:
            self.order_list.append(t)

        for i in range(0, int(n[1])):
            self.order_list.append(MAX_SIZE)

    def send_order(self):
        """
        1st order is sent immediately after 1min period expired in generate_trade_order function
        the rest of order send time is controlled by the previous order fill update

        NOTE: this version only consider every minute period standalone, in case of the order from last minute not
        filled yet,  need to consider in future
        :return:
        """
        if len(self.order_list) != 0:
            unit = self.order_list.pop()
            self.event_queue.put(MarketOrderEvent(self.instrument, unit, self.order_side))

    def order_fill_update(self, event):
        """
        API to communicate with trade_handler when order filling/filled
        :param event:
        :return:
        """
        if event.type == 'ORDERFILLING':
            self.perf_monitor.on_order_filling(event, event.order_type)
            logger.info('[Filling] [%s] -- %s' % (self.instrument, str(event.this_filled_units)))

        elif event.type == 'ORDERFILLED':
            t = Timer(0.3, self.send_order)
            t.start()
            self.perf_monitor.on_order_filled(event, event.order_type)
            logger.info('[Filled] [%s] -- %s' % (self.instrument, str(event.this_filled_units)))

    def order_cancel(self, event):
        """
        when cancel has been negatively canceled by PrimeXM
        :param event:
        :return:
        """
        #TODO: should clean the whole list or resend order
        self.order_list.clear()
        self.perf_monitor.on_order_cancel(event, event.order_type)
        logger.info('[Canceled] [%s] -- %s' % (self.instrument, str(event.units)))