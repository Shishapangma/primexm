from src.production.settings import OrderType
import numpy as np
from enum import Enum
import time

class Event(object):
    """
    Virtual Class
    """
    pass


class TickEvent(Event):
    def __init__(self, instrument, date, time, bid, ask, bid_vol, ask_vol):
        self.type = 'TICK'
        self.instrument = instrument
        self.date = date
        self.time = time
        self.bid = bid
        self.ask = ask
        self.bid_vol = bid_vol
        self.ask_vol = ask_vol

class MarketDataEvent(Event):
    def __init__(self, instrument, date, times, bid, ask, ret, order_type):
        self.type = 'MARKET_DATA'
        self.order_type = order_type
        self.instrument = instrument
        self.date = date
        self.time = times
        self.bid = bid
        self.ask = ask
        self.ret = ret
        self.t1 = time.time()


class SpreadEvent(Event):
    def __init__(self, instrument, date, time, bid, ask):
        self.type = 'SPREAD'
        self.instrument = instrument
        self.date = date
        self.time = time
        self.bid = bid
        self.ask = ask


class HistoriyDataEvent(Event):
    def __init__(self, instrument, candles):
        self.type = 'HISTORY_DATA'
        self.instrument = instrument
        self.candles = candles


class MarketOrderEvent(Event):
    def __init__(self, instrument, units, side):
        self.type = 'MARKET_ORDER'
        self.instrument = instrument
        self.units = units
        self.order_type = OrderType.MARKET_ORDER
        self.side = side


class LimitOrderEvent(Event):
    def __init__(self, instrument, units, side, price, pegged_order=False, rate_valve=0, clean=False):
        self.type = 'LIMIT_ORDER'
        self.instrument = instrument
        self.units = units
        self.order_type = OrderType.LIMIT_ORDER
        self.side = side
        self.price = price
        self.rate_valve = rate_valve
        self.pegged_order = pegged_order
        self.clean = clean
        # self.expire_time = expire_time


class ExchangeRateEvent(Event):
    def __init__(self):
        self.type = 'RATE'


class OrderFillingEvent(Event):
    def __init__(self, instrument, date, time, this_filled_units, total_leaves_units, price, orderId, order_type):
        self.type = 'ORDERFILLING'
        self.instrument = instrument
        self.order_type = order_type
        self.date = date
        self.time = time
        self.this_filled_units = this_filled_units
        self.total_leaves_units = total_leaves_units
        self.price = price
        self.orderId = orderId


class OrderFilledEvent(Event):
    def __init__(self, instrument, date, time, this_filled_units, total_filled_units, price, orderId, order_type):
        self.type = 'ORDERFILLED'
        self.instrument = instrument
        self.order_type = order_type
        self.this_filled_units = this_filled_units
        self.total_filled_units = total_filled_units
        self.date = date
        self.time = time
        self.price = price
        self.orderId = orderId


class OrderExpiredEvent(Event):
    def __init__(self, time, orderId):
        self.type = 'ORDEREXPIRED'
        self.time = time
        self.orderId = orderId


class OrderCancelEvent(Event):
    def __init__(self, instrument, date, time, order_type, units):
        self.type = 'ORDERCANCEL'
        self.instrument = instrument
        self.order_type = order_type
        self.date = date
        self.time = time
        self.units = units


class OrderCancelReplaceEvent(Event):
    def __init__(self, date, time, instrument, units, price, side, clOrderId, orderId, order_type):
        self.type = 'ORDERCANCELREPLACE'
        self.date = date
        self.time = time
        self.instrument = instrument
        self.order_type = order_type
        self.units = units
        self.price = price
        self.side = side
        self.clOrderId = clOrderId
        self.orderId = orderId


class OrderCreateEvent(Event):
    def __init__(self, date, time, instrument, units, price, side, clOrderId, orderId, order_type):
        self.type = 'ORDERCREATE'
        self.date = date
        self.time = time
        self.instrument = instrument
        self.order_type = order_type
        self.units = units
        self.price = price
        self.side = side
        self.clOrderId = clOrderId
        self.orderId = orderId


class OrderCancelRejectEvent(Event):
    def __init__(self, date, time, OrigClOrdID):
        self.type = 'ORDERCANCELREJECT'
        self.date = date
        self.time = time
        self.origClOrdID = OrigClOrdID


class OrderCreateRejectEvent(Event):
    def __init__(self, instrument):
        self.type = 'ORDERCREATEREJECT'
        self.instrument = instrument

class Reconnection(Event):
    def __init__(self):
        self.type = 'RECONNECTION'

class OrderHandler:
    class OrderStats(Enum):
        CREATING = 1
        CREATED = 2
        FILLING = 3
        FILLED = 4
        CANCELLING = 5
        REPLACING = 6

    def __init__(self, order_event, clOrderId):
        self.state = np.nan
        self.instrument = order_event.instrument
        self.clOrderId = clOrderId
        self.order_saver = None
        self.rate_valve = 0.0
        self.rate_valve = order_event.rate_valve if order_event.type == 'LIMIT_ORDER' else np.nan


    def update(self, unit, side, orderId):
        self.unit = unit
        self.total_leaves_unit = unit
        self.side = side
        self.orderId = orderId


class ValveEvent(Event):
    def __init__(self, instrument):
        self.type = 'VALVETICK'
        self.instrument = instrument


class PeggedOrderUpdateEvent(Event):
    def __init__(self, date, time, clOrderId, OrigClOrdId, price):
        self.type = 'PEGGEDORDERUPDATE'
        self.date = date
        self.time = time
        self.clOrderId = clOrderId
        self.origClOrdId = OrigClOrdId
        self.price = price




