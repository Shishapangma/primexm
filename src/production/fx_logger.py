import logging
from logging import handlers
import src.production.config.path as PATH
import platform
import sys

if platform.system() == 'Windows':
    path = PATH.LOG_PATH_WIN
else:
    path = PATH.LOG_PATH_POSIX

# create logger
logger = logging.getLogger('fx_system')
logger.setLevel(logging.DEBUG)

# create disk file handle
hdlr = logging.FileHandler(path, 'a')

# create format
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

# change to GMT
import time
formatter.converter = time.gmtime
hdlr.setFormatter(formatter)

# create memory handler
memoryhandler = handlers.MemoryHandler(1024*10, logging.DEBUG, target=hdlr)

# add file handler
logger.addHandler(memoryhandler)

# add stdout handler
s = logging.StreamHandler(sys.stdout)
s.setFormatter(formatter)
logger.addHandler(s)
