import queue
import sys

from src.api_adapter.primexm.primexm_market_wrapper import PrimeXmMarketWrapper
from src.api_adapter.primexm.primexm_trade_wrapper import PrimeXmTradeWrapper
from src.api_adapter.oanda_rest.oanda_rest_wrapper import OandaRestWrapper
from src.production.trade_handler import TradeHandler
from src.database.Tick.DummyMarketStreamHandler import DummyMarketStreamHandler
from src.production.market_stream_handler import MarketStreamHandler
from src.production.config.primexm_config import MarketData, Trading


def startup():

    market_wrapper = None
    trade_wrapper = None
    trader = None

    try:

        queues = {'TRADE': queue.Queue(), 'MARKET': queue.Queue(), 'DUMMY_MARKET': queue.Queue()}

        market_wrapper = market_wrapper = PrimeXmMarketWrapper(queues['MARKET'],
                                                               queues['DUMMY_MARKET'],
                                                               MarketData['DNS'],
                                                               MarketData['Port'],
                                                               MarketData['SSL'],
                                                               MarketData['TargetCompId'],
                                                               MarketData['SenderCompId'],
                                                               MarketData['Username'],
                                                               MarketData['Password'])

        trade_wrapper = PrimeXmTradeWrapper(queues['TRADE'],
                                            Trading['DNS'],
                                            Trading['Port'],
                                            Trading['SSL'],
                                            Trading['TargetCompId'],
                                            Trading['SenderCompId'],
                                            Trading['Username'],
                                            Trading['Password'])

        # hist_data = HistData(queues['TRADE'])
        rest_wrapper = OandaRestWrapper(queues=queues)

        trader = TradeHandler(fix_wrapper=trade_wrapper, queues=queues)
        streamer = MarketStreamHandler(trader=trader, queue=queues['MARKET'])
        dummy_streamer = DummyMarketStreamHandler(queue=queues['DUMMY_MARKET'])

        trader.start()
        streamer.start()
        dummy_streamer.start()

        rest_wrapper.get_history_prices()
        # hist_data.get_hist_data()

        market_wrapper.start()
        trade_wrapper.start()

        trader.join()
        market_wrapper.join()
        # trade_wrapper.join()

    except KeyboardInterrupt:
        # market_wrapper.logout()
        # trade_wrapper.logout()
        pass

    except BaseException:
        market_wrapper.logout()
        sys.exit(1)
