from queue import Empty
from threading import Thread, Timer
import time
from datetime import datetime

from src.production.strategy_wrapper import *
from src.production.event import OrderHandler
import src.production.settings as SET
from src.production.fx_logger import logger
from src.production.news_calendar import EventCalendar
from lib.broker_api_lib.fix_primexm.pyfix.connection import FIXException


class TradeTimer(Thread):
    def __init__(self, portfolio):
        Thread.__init__(self)

        self.portfolio = portfolio
        self.trade = True
        self.interval = None

    def run(self):

        # identify the Second to trigger trade
        while 1:
            if datetime.now().second == 58:
                # self.elapse = time.perf_counter()
                break
            time.sleep(0.5)

        while 1:
            # cur_elapse = time.perf_counter()

            if datetime.now().second == 58 and self.trade:

                for instrument in SET.INSTRUMENT:
                    for order_type in SET.INSTRUMENT_ORDER_TYPE[instrument]:
                        self.portfolio[instrument][order_type].perf_monitor.market_data_handler.deliver_market_data()

                self.trade = False
                self.interval = Timer(30, self.unlock)
                self.interval.start()

            time.sleep(0.1)

    def unlock(self):
        self.trade = True


class TradeHandler(Thread):
    """
    the main task to manager different kind of events
    """

    def __init__(self, fix_wrapper, queues, t_time=None):
        Thread.__init__(self)

        # Event queue is to collebrating the time series between different tasks
        self.trade_queue = queues['TRADE']

        # Switch to control ON/OFF trading process
        self.active = True
        # Swtich to control ON/OFF trade
        self.halt = False

        # Handles of all kind of trading relevant instance
        self.fix_wrapper = fix_wrapper

        # Handles of individual instrument components
        self.fx_strategy = {}
        self.portfolio = {}
        # store current unique order of per instrument
        self.orders = {}

        if t_time:
            self.event_calendar = EventCalendar(t_time)
        else:
            self.event_calendar = EventCalendar()

        for ins in SET.INSTRUMENT:
            # portfolio instance
            self.portfolio[ins] = {}
            self.fx_strategy[ins] = {}

            for order_type in SET.INSTRUMENT_ORDER_TYPE[ins]:
                self.portfolio[ins][order_type] = Portfolio(ins, self.fix_wrapper, self.trade_queue, order_type)
                if order_type == SET.OrderType.LIMIT_ORDER:
                    self.fx_strategy[ins][order_type] = LimitFxStrategy(self, ins, self.portfolio[ins][order_type])
                else:
                    self.fx_strategy[ins][order_type] = MarketFxStrategy(self, ins, self.portfolio[ins][order_type])

                    # self.portfolio[ins][order_type].perf_monitor.market_data_handler.set_delay(i * duration)
                    # self.portfolio[ins][order_type].perf_monitor.market_data_handler.set_delay(0.0)
                    # self.portfolio[ins][order_type].perf_monitor.market_data_handler.start()

            # order stats instance
            self.orders[ins] = {}

        # trade timer
        self.trade_timer = TradeTimer(self.portfolio)
        self.trade_timer.start()

        self.fix_wrapper.set_handler(self)

    def init_portfolio(self, instrument):
        """
        initialize portfolio with on time exchange rate
        Note: will be replaced
        :return: None
        """
        for order_type in self.portfolio[instrument]:
            self.portfolio[instrument][order_type].init_trading(SET.INITIAL_NET_ASSET,
                                                                SET.EXCHANGE_RATE[instrument],
                                                                SET.REMAIN_POSITION[instrument][order_type])
            logger.info('%s Type %s Initial position: %s',
                        instrument, order_type, str(SET.REMAIN_POSITION[instrument][order_type]))

    def close(self):
        """
        Do something before shutdown application
        :return:
        """
        # Dump final database
        for ins in SET.INSTRUMENT:
            for order_type in SET.INSTRUMENT_ORDER_TYPE[ins]:
                self.portfolio[ins][order_type].per_monitor.dump_log()
                self.portfolio[ins][order_type].per_monitor.timer_dp.close()

    def run(self):
        """
        infinite loop to license trading relative event and call handler accordingly
        :return: None
        """
        # Position store
        for instrument in SET.INSTRUMENT:
            self.init_portfolio(instrument)

        # active calender manager
        # self.event_calendar.start()

        # into loop
        while 1:
            try:
                event = self.trade_queue.get(block=False)
                self.process(event)
            except Empty:
                pass

    def process(self, event):
        """
        main distributor of different event handler
        :param event: Transfer the Event handler with varies data
        :return: None
        """
        if event.type == 'SPREAD':
            # udpate TC based on second for market order
            # self.fx_strategy[event.instrument][SET.OrderType.MARKET_ORDER].transaction_cost.update_transaction_cost(
            #     event.bid, event.ask)
            pass

        elif event.type == 'MARKET_DATA':
            # trigger AlphaSignal model based on specific duration
            # Signal Generation
            order_event = None

            # update new event by min tick
            self.event_calendar.update_alpha_weight(event.instrument, event.date, event.time)
            alpha_weight = self.event_calendar.get_alpha_weight(event.instrument)
            alpha_result = self.fx_strategy[event.instrument][event.order_type].get_target_weight(event,
                                                                                                  alpha_weight)
            self.portfolio[event.instrument][event.order_type].generate_trade_order(
                alpha_result=alpha_result,
                tick_event=event)
        elif event.type == 'MARKET_ORDER':
            # Order Execution
            order_event = event

            # clean existing order. it residual is due to network. Market order has no order cancel
            if order_event.order_type in self.orders[order_event.instrument]:
                self.orders[event.instrument].pop(event.order_type)

            # execute order
            try:
                clOrderId = self.fix_wrapper.new_order_single(order_event)
                if clOrderId != 0:
                    self.orders[event.instrument][event.order_type] = OrderHandler(order_event, clOrderId)
                    self.orders[event.instrument][event.order_type].state = OrderHandler.OrderStats.CREATING
            except FIXException as e:
                logger.error('[FIX Exception Create Order] [%s] Type %s %s' % (
                    order_event.instrument, order_event.order_type, e))
            except Exception as e:
                logger.error('[System Exception Create Order] [%s] Type %s %s' % (
                    order_event.instrument, order_event.order_type, e))

                from lib.twilio_alarm.twilio_notifications.middleware import TwilioNotificationsMiddleware
                notifier = TwilioNotificationsMiddleware()
                notifier.process_exception(None, '[Unrecovery Error in System Exception Create Order] %s' % (e))

        # Order created
        elif event.type == 'ORDERCREATE':
            # create order instance to track the stats
            if self.orders[event.instrument][event.order_type].state == OrderHandler.OrderStats.CREATING:
                self.orders[event.instrument][event.order_type].update(event.units, event.side, event.orderId)
                self.orders[event.instrument][event.order_type].state = OrderHandler.OrderStats.CREATED
                self.portfolio[event.instrument][event.order_type].perf_monitor.on_order_create(event, event.order_type)
        # Order filling event
        elif event.type == 'ORDERFILLING':
            if event.order_type in self.orders[event.instrument]:
                self.orders[event.instrument][event.order_type].total_leaves_unit = event.total_leaves_units
                self.orders[event.instrument][event.order_type].state = OrderHandler.OrderStats.FILLING
                self.portfolio[event.instrument][event.order_type].order_fill_update(event)
        # Order filled event
        elif event.type == 'ORDERFILLED':
            # Only when the order be fully filled then set empty
            if event.order_type in self.orders[event.instrument]:
                self.portfolio[event.instrument][event.order_type].order_fill_update(event)
                self.orders[event.instrument].pop(event.order_type)

        # Order Cancel in market order is only triggered by PrimeXM when providers identify order has been Time In Force exhausted
        # sequential action after receiving Order Cancel is to clean order stats
        elif event.type == 'ORDERCANCEL':
            if event.order_type in self.orders[event.instrument]:
                self.portfolio[event.instrument][event.order_type].order_cancel(event)
                self.orders[event.instrument].pop(event.order_type)

        # Order create reject
        elif event.type == 'ORDERCREATEREJECT':
            if SET.OrderType.LIMIT_ORDER in self.orders[event.instrument]:
                if self.orders[event.instrument][SET.OrderType.LIMIT_ORDER].state == OrderHandler.OrderStats.CREATING:
                    self.orders[event.instrument].pop(SET.OrderType.LIMIT_ORDER)
                elif self.orders[event.instrument][
                    SET.OrderType.LIMIT_ORDER].state == OrderHandler.OrderStats.REPLACING:
                    self.orders[event.instrument][SET.OrderType.LIMIT_ORDER].state = OrderHandler.OrderStats.FILLING
            elif SET.OrderType.MARKET_ORDER in self.orders[event.instrument]:
                if self.orders[event.instrument][SET.OrderType.MARKET_ORDER].state == OrderHandler.OrderStats.CREATING:
                    self.orders[event.instrument].pop(SET.OrderType.MARKET_ORDER)
                elif self.orders[event.instrument][
                    SET.OrderType.MARKET_ORDER].state == OrderHandler.OrderStats.REPLACING:
                    self.orders[event.instrument][SET.OrderType.MARKET_ORDER].state = OrderHandler.OrderStats.FILLING

        # Trade session restore event
        # TODO: implment reconnection
        elif event.type == 'RECONNECTION':
            for instrument in self.orders:
                for order_type in self.orders[instrument]:
                    logger.info('[Order Status Request] [%s] Type %s Order Stats %s OrderId %s',
                                instrument,
                                str(order_type),
                                str(self.orders[instrument][order_type].state),
                                self.orders[instrument][order_type].clOrderId)
                    try:
                        self.fix_wrapper.order_status_request(self.orders[instrument][order_type])
                    except FIXException as e:
                        logger.info('[Exception Order Status Request] [%s] Type %s Order Stats %s OrderId %s %s',
                                    instrument,
                                    str(order_type),
                                    str(self.orders[instrument][order_type].state),
                                    self.orders[instrument][order_type].clOrderId,
                                    e)

        # System initialized event
        elif event.type == 'HISTORY_DATA':
            if event.instrument in SET.INSTRUMENT:
                for order_type in self.fx_strategy[event.instrument]:
                    results = self.fx_strategy[event.instrument][order_type].init_historical_alpha(event)
                    self.portfolio[event.instrument][
                        order_type].perf_monitor.market_data_handler.initialize_market_data(
                        results['time'],
                        results['bid'],
                        results['ask'])
                    self.portfolio[event.instrument][order_type].update_current_weight(
                        (results['bid'] + results['ask']) / 2)
