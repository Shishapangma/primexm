import pandas as pd
import numpy as np
from collections import deque
from datetime import datetime
from threading import Timer
import threading
from threading import Thread

from src.production.event import MarketDataEvent, SpreadEvent, ValveEvent
import src.production.settings as SET
import src.production.config.path as PATH
from src.production.fx_logger import logger


class MarketDataHandler(Thread):
    """
    Handler of Market Data event, for instance regarding interim stats
    """

    def __init__(self, performancer, instrument, queue, order_type):
        Thread.__init__(self)

        self.performancer = performancer
        self.instrument = instrument
        self.event_queus = queue
        self.market_ticks = deque(maxlen=2)
        self.last_bid = np.nan
        self.last_ask = np.nan
        self.last_ret = np.nan
        self.last_tick_date = datetime.utcnow().strftime('%Y%m%d')
        self.last_tick_time = datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f')[:-3]
        self.order_type = order_type
        self.lock = threading.Lock()
        self.flag = True
        self.valve = 0
        self.current_order_side = ''

        # only valid for market order to update spread calc
        if self.order_type == SET.OrderType.MARKET_ORDER:
            self.timer_spread_data = Timer(SET.SPREAD_FREQ, self.deliver_second_spread)
            self.timer_spread_data.start()


    def update_tick(self, bid, ask, tick_date, tick_time):
        """
        Interface to update tick
        :param bid: 
        :param ask: 
        :param tick_date: 
        :param tick_time: 
        :return: 
        """
        self.lock.acquire()
        self.last_bid = bid
        self.last_ask = ask
        self.last_tick_date = tick_date
        self.last_tick_time = tick_time
        self.lock.release()

        if self.current_order_side != '':
            # Buy
            if self.current_order_side == '1' and self.last_bid > self.valve:
                valve_event = ValveEvent(self.instrument)
                self.event_queus.put(valve_event)
            # Sell
            elif self.current_order_side == '2' and self.last_ask < self.valve:
                valve_event = ValveEvent(self.instrument)
                self.event_queus.put(valve_event)

            # print('ask', self.last_ask, 'bid', self.last_bid, 'valve', self.valve)


        # sec = datetime.strptime(self.last_tick_time, '%Y%m%d-%H:%M:%S.%f').second
        #
        # if self.flag:
        #     if np.logical_or(np.logical_and(sec >= 56, sec <= 59), np.logical_and(sec >= 0, sec <= 2)):
        #         self.flag = False
        #         self.deliver_market_data()

    def deliver_market_data(self):
        """
        handler for market data deliver
        :return:
        """
        self.lock.acquire()
        date = self.last_tick_date
        t_time = self.last_tick_time
        bid = self.last_bid
        ask = self.last_ask
        self.lock.release()

        if not np.isnan(ask):
            self.market_ticks.append((bid, ask))
        if len(self.market_ticks) == 2:
            pre_mid = (self.market_ticks[0][0] + self.market_ticks[0][1]) / 2
            last_mid = (self.market_ticks[1][0] + self.market_ticks[1][1]) / 2
            self.last_ret = (last_mid - pre_mid) / pre_mid
            market_data_event = MarketDataEvent(self.instrument, date, t_time, bid, ask, self.last_ret, self.order_type)
            self.performancer.on_interval_generate(market_data_event, self.order_type, bid, ask)
            self.event_queus.put(market_data_event)

        logger.info('[Tick Data] [%s] return %s bid %s ask %s', self.instrument, str(self.last_ret), self.last_bid, self.last_ask)

    def deliver_second_spread(self):
        if not np.isnan(self.last_ask):
            spread_event = SpreadEvent(self.instrument, self.last_tick_date, self.last_tick_time, self.last_bid,
                                       self.last_ask)
            self.event_queus.put(spread_event)
        self.timer_spread_data = Timer(SET.SPREAD_FREQ, self.deliver_second_spread)
        self.timer_spread_data.start()

    def initialize_market_data(self, time, bid, ask):
        self.market_ticks.append((bid, ask))


class PerformanceMonitor:
    def __init__(self, portfolior, event_queue, order_type):
        # Handler of portfolior
        self.portfolior = portfolior

        self.order_type = order_type

        # Market Data Hander
        self.market_data_handler = MarketDataHandler(self, portfolior.instrument, event_queue, order_type)

        # track every tick update using for slippage study during order execution
        self.last_target_position = 0
        self.last_current_position = 0

        # allocate Dic for saving minutes return
        self.data_columns = ['local timestamp',
                             'date',
                             'time',
                             'instrument',
                             'event',
                             'order',
                             'bid',
                             'ask',
                             'bid vol',
                             'ask vol',
                             'return',
                             'current position',
                             'target position',
                             'traded position',
                             'target price',
                             'trade price']

        self.records = []
        self.record_size = 0

        # for filled bid/ask update
        self.current_filled = -1
        self.trade_size = 0

        import platform

        if platform.system() == 'Windows':
            self.path = PATH.TRADE_LOG_WIN
        else:
            self.path = PATH.TRADE_LOG_POSIX

    def on_interval_generate(self, event, order_type, bid, ask):
        """
        :param event: 
        :param order_type: 
        :param bid: 
        :param ask: 
        :return: 
        """
        order_type = 'MARKET' if order_type == SET.OrderType.MARKET_ORDER else 'LIMIT'

        self.records.append((datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f'),
                             event.date,
                             event.time,
                             event.instrument,
                             'One_Min_Tick',
                             order_type,
                             bid,
                             ask,
                             0,
                             0,
                             self.market_data_handler.last_ret,
                             self.last_current_position,
                             0,
                             0,
                             0,
                             0))

    def on_order_create(self, event, order_type):
        """
        order accepted confirm created
        :param event:
        :return:
        """
        self.market_data_handler.lock.acquire()
        bid = self.market_data_handler.last_bid
        ask = self.market_data_handler.last_ask
        self.market_data_handler.lock.release()

        order_type = 'MARKET' if order_type == SET.OrderType.MARKET_ORDER else 'LIMIT'

        self.last_target_position = self.last_current_position + event.units
        self.records.append((datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f'),
                             event.date,
                             event.time,
                             event.instrument,
                             'Order_Create',
                             order_type,
                             bid,
                             ask,
                             0,
                             0,
                             self.market_data_handler.last_ret,
                             self.last_current_position,
                             self.last_current_position + event.units,
                             0,
                             event.price,
                             0))

        # if order_type == 'LIMIT':
        #     self.market_data_handler.active_valve(event.price, event.side)

    def on_pegged_price_update(self, event):

        self.market_data_handler.lock.acquire()
        bid = self.market_data_handler.last_bid
        ask = self.market_data_handler.last_ask
        self.market_data_handler.lock.release()

        self.records.append((datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f'),
                             event.date,
                             event.time,
                             event.instrument,
                             'Pegged_Update',
                             'LIMIT',
                             bid,
                             ask,
                             0,
                             0,
                             0,
                             0,
                             0,
                             0,
                             event.price,
                             0))

    def on_order_cancel(self, event, order_type):
        """
        :param event:
        :param order_type:
        :return:
        """
        self.market_data_handler.lock.acquire()
        bid = self.market_data_handler.last_bid
        ask = self.market_data_handler.last_ask
        self.market_data_handler.lock.release()

        self.records.append((datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f'),
                             event.date,
                             event.time,
                             event.instrument,
                             'Order_Cancel',
                             order_type,
                             bid,
                             ask,
                             0,
                             0,
                             self.market_data_handler.last_ret,
                             self.last_current_position,
                             0,
                             event.units,
                             0,
                             0))

    def on_order_replace(self, event, order_type):
        """

        :param event:
        :param order_type:
        :return:
        """

        # self.market_data_handler.lock.acquire()
        # bid = self.market_data_handler.last_bid
        # ask = self.market_data_handler.last_ask
        # self.market_data_handler.lock.release()
        #
        # self.records.append((datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f'),
        #                      event.date,
        #                      event.time,
        #                      event.instrument,
        #                      'Order_Replace',
        #                      order_type,
        #                      bid,
        #                      ask,
        #                      0,
        #                      0,
        #                      self.market_data_handler.last_ret,
        #                      self.last_current_position,
        #                      0,
        #                      0,
        #                      event.price,
        #                      0))
        pass

    def on_order_filling(self, event, order_type):
        """
        fill when order be filling
        :param event:
        :return:
        """
        self.market_data_handler.lock.acquire()
        bid = self.market_data_handler.last_bid
        ask = self.market_data_handler.last_ask
        self.market_data_handler.lock.release()

        self.last_current_position += event.this_filled_units

        order_type = 'MARKET' if order_type == SET.OrderType.MARKET_ORDER else 'LIMIT'

        self.records.append((datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f'),
                             event.date,
                             event.time,
                             event.instrument,
                             'Order_Filling',
                             order_type,
                             bid,
                             ask,
                             0,
                             0,
                             self.market_data_handler.last_ret,
                             self.last_current_position,
                             self.last_target_position,
                             event.this_filled_units,
                             0,
                             event.price))


        # positively push to update position/weight
        self.portfolior.update_current_weight((bid + ask) / 2)

    def on_order_filled(self, event, order_type):
        """
        order filled event handler
        :param event:
        :return:
        """
        self.market_data_handler.lock.acquire()
        bid = self.market_data_handler.last_bid
        ask = self.market_data_handler.last_ask
        self.market_data_handler.lock.release()

        # update position
        self.last_current_position += event.this_filled_units

        order_type = 'MARKET' if order_type == SET.OrderType.MARKET_ORDER else 'LIMIT'

        self.records.append((datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f'),
                             event.date,
                             event.time,
                             event.instrument,
                             'Order_Filled',
                             order_type,
                             bid,
                             ask,
                             0,
                             0,
                             self.market_data_handler.last_ret,
                             self.last_current_position,
                             self.last_target_position,
                             event.this_filled_units,
                             0,
                             event.price))

        # positively push to update position/weight
        self.portfolior.update_current_weight((bid + ask) / 2)

        self.save_file()

    def on_update_tick(self, event):
        """
        tick receive event handler
        :param event:
        :return:
        """
        self.market_data_handler.update_tick(event.bid, event.ask, event.date, event.time)

    def save_file(self):
        """
        save data in configured interval
        :return:
        """

        if self.order_type == SET.OrderType.MARKET_ORDER:
            suffix = '.market'
        else:
            suffix = '.limit'
        instrument = self.portfolior.instrument
        instrument = instrument.replace('/', '_')
        file_name = self.path + instrument + suffix + '.csv'
        data_store = pd.DataFrame(self.records, columns=self.data_columns)
        try:
            data_store.to_csv(file_name, index=False, header=True, mode='a')
        except BaseException as e:
            logger.info('[Trade Record Output Exception] %s' % (e))

        self.records = []
