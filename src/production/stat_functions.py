import numpy as np


def get_exponential_weight(half_life):
    return np.exp(np.log(0.5)/half_life)


def get_exponential_weight_origin(half_life):
    return np.exp(np.log(0.5)/half_life)


def get_exponential_weights_vector(half_life, length):
    delta = np.exp(np.log(0.5) / half_life)

    weights = np.array([(1 - delta) * np.power(delta, i-1) for i in range(length, 0, -1)])

    return weights / weights.sum()


def ornstein_uhlenbeck_calib(data, delta=1):

    n = data.shape[0]

    Sx = np.sum(data[0:-1])
    Sy = np.sum(data[1:])
    Sxx = np.sum(np.power(data[0:-1],2))
    Syy = np.sum(np.power(data[1:],2))
    Sxy = np.sum(data[0:-1] * data[1:])

    mu  = (Sy*Sxx - Sx*Sxy) / ( n*(Sxx - Sxy) - (Sx*Sx - Sx*Sy) );
    lambda_ = -np.log( (Sxy - mu*Sx - mu*Sy + n*mu*mu) / (Sxx -2*mu*Sx + n*mu*mu) ) / delta;
    a = np.exp(-lambda_*delta);
    sigmah2 = (Syy - 2*a*Sxy + a*a*Sxx - 2*mu*(1-a)*(Sy - a*Sx) + n*mu*mu*(1-a)*(1-a))/n;
    sigma = np.sqrt(sigmah2*2*lambda_/(1-a*a))

    return mu,sigma,lambda_


def ornstein_uhlenbeck_forecast(params, start, horizon):

    """
    :param params: mu, sigma, lambd[a]
    :param horizon:
    :return:
    """

    (mu, sigma, lambd) = params

    forecast = start * np.exp(-lambd * horizon) + mu * (1 - np.exp(-lambd * horizon))
    vol = sigma * (1 - np.exp(-2 * lambd * horizon)) / (2 * lambd)

    return forecast, vol


def ornstein_uhlenbeck_fit_forecast(data, ou_fitting_period=10000, ou_forecast=5, mean_reversion_threshold=15):
    """
    for each date (line) and each symbol (column), fit an OU process on the last ou_fitting_period days, and do a
    ou_ou_forecast days forecast
    :param data:
    :param ou_fitting_period:
    :param ou_forecast:
    :return:
    """

    data = data.values
    T = len(data)

    forecasts = np.empty(T)
    lambdas = np.empty(T)
    forecasts[:] = np.nan
    lambdas[:] = np.nan
    forecasts_p = forecasts.copy()


    for date in range(ou_fitting_period, T):

        y = data[(date - ou_fitting_period):date]
        y = np.where(np.isnan(y), 0., y)
        mu, sigma, lambd = ornstein_uhlenbeck_calib(y)

        if np.isnan(mu) \
                or np.isnan(sigma) \
                or np.isnan(lambd) \
                or (lambd <= 0):
                # or (np.log(2.) / lambd > mean_reversion_threshold):
            continue

        y_f, vol_f = ornstein_uhlenbeck_forecast((mu, sigma, lambd), y[-1], ou_forecast)

        forecasts[date] = (y_f - y[-1]) / vol_f
        lambdas[date] = lambd
        forecasts_p[date] = y_f

    return forecasts, forecasts_p, lambdas
