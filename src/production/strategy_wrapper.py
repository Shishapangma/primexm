import math

import src.production.stat_functions as sf
from src.production.portfolio import *
import src.production.settings as SET
from src.production.fx_logger import logger


class AlphaSignal(object):
    def __init__(self, alpha_winsorize, alpha_scale):
        self.alpha_signal_t = 0
        self.short_ema_t = 0
        self.long_ema_t = 0
        self.sigma_ema_t = 0
        self.short_half_life = SET.SHORT_HALF_LIFE
        self.long_half_life = SET.LONG_HALF_LIFE
        self.alpha_winsorize = alpha_winsorize
        self.alpha_scale = alpha_scale
        self.exp_short_life_weight = sf.get_exponential_weight(self.short_half_life)
        self.exp_long_life_weight = sf.get_exponential_weight(self.long_half_life)
        self.exp_decay_half_life = sf.get_exponential_weight(10)

    def get_simple_reversion_alpha(self, return_t, short_ema_pre, long_ema_pre, sigma_ema_pre, alpha_scale):
        self.short_ema_t = return_t * (1 - self.exp_short_life_weight) + short_ema_pre * self.exp_short_life_weight
        self.long_ema_t = return_t * (1 - self.exp_long_life_weight) + long_ema_pre * self.exp_long_life_weight
        self.sigma_ema_t = (np.sqrt(self.exp_long_life_weight * (
            sigma_ema_pre ** 2 + (1 - self.exp_long_life_weight) * ((return_t - long_ema_pre) ** 2))))
        self.alpha_signal_t = (self.long_ema_t - self.short_ema_t) / self.sigma_ema_t * alpha_scale
        if math.isnan(self.alpha_signal_t):
            self.alpha_signal_t = 0
            self.short_ema_t = 0
            self.long_ema_t = 0
            self.sigma_ema_t = 0
        if self.alpha_signal_t > self.alpha_winsorize:
            self.alpha_signal_t = self.alpha_winsorize
        elif self.alpha_signal_t < -self.alpha_winsorize:
            self.alpha_signal_t = -self.alpha_winsorize

    def get_alpha_scale_with_weight(self, scale_weight):
        return self.alpha_scale * scale_weight


class TransactionCostModel(object):
    def __init__(self, kappa, half_life):
        self.tc = 0
        self.kappa = kappa
        self.spread_half_life = half_life
        self.spread_bid_ema = 0
        self.spread_ask_ema = 0
        self.exp_life_weight = sf.get_exponential_weight(self.spread_half_life)

    def update_transaction_cost(self, bid=0, ask=0, tc_rebate=0, commission=0):
        pass


class LimitTransactionCostModel(TransactionCostModel):
    def __init__(self, kappa, half_life):
        TransactionCostModel.__init__(self, kappa, half_life)

    def update_transaction_cost(self, bid=0, ask=0, tc_rebate=0, commission=0):
        """
        Should be turned based on condition
        :param bid:
        :param ask:
        :param tc_rebate:
        :param commission:
        :return:
        """
        self.tc = 0.08


class MarketTransactionCostModel(TransactionCostModel):
    def __init__(self, kappa, half_life):
        TransactionCostModel.__init__(self, kappa, half_life)

    def update_transaction_cost(self, bid=0, ask=0, tc_rebate=0, commission=0):
        # self.spread_bid_ema = bid * (1 - self.exp_life_weight) + self.spread_bid_ema * self.exp_life_weight
        # self.spread_ask_ema = ask * (1 - self.exp_life_weight) + self.spread_ask_ema * self.exp_life_weight
        # self.tc = (((self.spread_ask_ema - self.spread_bid_ema - tc_rebate / 10000) /
        #             (self.spread_bid_ema + self.spread_ask_ema) + commission) * 10000) + 0.09

        self.tc = ((ask - bid) / ((ask + bid) + commission) * 10000) + 0.09


class FxStrategy(object):
    def __init__(self, trader, instrument, portfolio):
        # handler of Trader instance, in case of requiring high level colleborated task
        self.trader = trader

        # Specific instrument Index
        self.instrument = instrument

        # Handler of AlphaSignal instance
        self.alpha = AlphaSignal(SET.ALPHA_WINSORIZE[self.instrument], SET.ALPHA_SCALE[self.instrument])

        # Handler of Portfolio instance
        self.portfolio = portfolio

        # Handler of TransactionCostModel instance
        self.transaction_cost = None

    def get_weight_simple_alpha_with_tc(self, weight_current):
        if self.alpha.alpha_signal_t > (weight_current + self.transaction_cost.tc * self.transaction_cost.kappa):
            target_weight_t = self.alpha.alpha_signal_t - self.transaction_cost.tc * self.transaction_cost.kappa
        elif self.alpha.alpha_signal_t < (weight_current - self.transaction_cost.tc * self.transaction_cost.kappa):
            target_weight_t = self.alpha.alpha_signal_t + self.transaction_cost.tc * self.transaction_cost.kappa
        else:
            target_weight_t = weight_current
        return target_weight_t

    def init_historical_alpha(self, event):
        """
        Initilize system with 400 historical data at the beginning of startup
        :param event: Event contains 400 past observation
        :return: None
        """
        pass

    def get_target_weight(self, event, scale_weight):
        """
        Function is valid when after historical data initialized completed
        :param event: Event contains return of bid
        :return: updated weight
        """
        pass


class LimitFxStrategy(FxStrategy):
    def __init__(self, trader, instrument, portfolio):
        FxStrategy.__init__(self, trader, instrument, portfolio)
        self.transaction_cost = LimitTransactionCostModel(SET.KAPPA[self.instrument], SET.SPREAD_HALF_LIFE)

    def init_historical_alpha(self, event):
        """
        Initilize system with 400 historical data at the beginning of startup
        :param event: Event contains 400 past observation
        :return: None
        """
        candles = event.candles
        # bids = candles['bid']
        # asks = candles['ask']
        bids = candles['closeBid']
        asks = candles['closeAsk']
        ret = candles['ret']
        for i in range(0, len(ret)):
            self.alpha.get_simple_reversion_alpha(return_t=ret[i],
                                                  short_ema_pre=self.alpha.short_ema_t,
                                                  long_ema_pre=self.alpha.long_ema_t,
                                                  sigma_ema_pre=self.alpha.sigma_ema_t,
                                                  alpha_scale=self.alpha.alpha_scale)
        logger.info('[Init Historical Alpha] [%s] time %s bid %s ask %s' % (self.instrument, candles.index[-1],
                                                                            str(bids[-1]), str(asks[-1])))
        return {'time': candles.index[-1], 'bid': bids[-1], 'ask': asks[-1]}

    def get_target_weight(self, event, scale_weight):
        """
        Function is valid when after historical data initialized completed
        :param event: Event contains return of bid
        :param scale_weight: 
        :return: updated weight
        """
        ret = event.ret
        alpha_scale = self.alpha.get_alpha_scale_with_weight(scale_weight)
        self.alpha.get_simple_reversion_alpha(return_t=ret,
                                              short_ema_pre=self.alpha.short_ema_t,
                                              long_ema_pre=self.alpha.long_ema_t,
                                              sigma_ema_pre=self.alpha.sigma_ema_t, alpha_scale=alpha_scale)
        self.transaction_cost.update_transaction_cost(event.bid, event.ask)
        current_weight = self.portfolio.get_current_weight()
        target_weight = self.get_weight_simple_alpha_with_tc(weight_current=current_weight)

        # No alpha turning
        if alpha_scale == SET.ALPHA_SCALE[self.instrument]:
            return {'current_weight': current_weight, 'target_weight': target_weight, 'clean': False,
                    'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc, 'alpha scale': alpha_scale, 'kappa:': self.transaction_cost.kappa}
        else:
            if alpha_scale != SET.ALPHA_SCALE[self.instrument] and alpha_scale != 0:
                return {'current_weight': current_weight, 'target_weight': target_weight, 'clean': True,
                        'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc,
                        'alpha scale': alpha_scale, 'kappa:': self.transaction_cost.kappa}
            elif alpha_scale == 0:
                return {'current_weight': current_weight, 'target_weight': SET.POSITION_CLEAN, 'clean': True,
                        'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc,
                        'alpha scale': alpha_scale, 'kappa:': self.transaction_cost.kappa}


class MarketFxStrategy(FxStrategy):
    def __init__(self, trader, instrument, portfolio):
        FxStrategy.__init__(self, trader, instrument, portfolio)
        self.transaction_cost = MarketTransactionCostModel(SET.KAPPA[self.instrument], SET.SPREAD_HALF_LIFE)

    def init_historical_alpha(self, event):
        """
        Initilize system with 400 historical data at the beginning of startup
        :param event: Event contains 400 past observation
        :return: None
        """
        candles = event.candles
        # bids = candles['bid']
        # asks = candles['ask']
        bids = candles['closeBid']
        asks = candles['closeAsk']
        ret = candles['ret']
        for i in range(0, len(ret)):
            self.alpha.get_simple_reversion_alpha(return_t=ret[i],
                                                  short_ema_pre=self.alpha.short_ema_t,
                                                  long_ema_pre=self.alpha.long_ema_t,
                                                  sigma_ema_pre=self.alpha.sigma_ema_t,
                                                  alpha_scale=self.alpha.alpha_scale)
            # self.transaction_cost.update_transaction_cost(bid=bids[i], ask=asks[i])
        logger.info('[Init Historical Alpha] [%s] time %s bid %s ask %s' % (self.instrument, candles.index[-1],
                                                                            str(bids[-1]), str(asks[-1])))
        return {'time': candles.index[-1], 'bid': bids[-1], 'ask': asks[-1]}

    def get_target_weight(self, event, scale_weight):
        """
        Function is valid when after historical data initialized completed
        :param event: Event contains return of bid
        :return: updated weight
        """
        ret = event.ret
        alpha_scale = self.alpha.get_alpha_scale_with_weight(scale_weight)
        self.alpha.get_simple_reversion_alpha(return_t=ret,
                                              short_ema_pre=self.alpha.short_ema_t,
                                              long_ema_pre=self.alpha.long_ema_t,
                                              sigma_ema_pre=self.alpha.sigma_ema_t, alpha_scale=alpha_scale)
        self.transaction_cost.update_transaction_cost(event.bid, event.ask)
        current_weight = self.portfolio.get_current_weight()
        target_weight = self.get_weight_simple_alpha_with_tc(weight_current=current_weight)

        # No alpha turning
        if alpha_scale == SET.ALPHA_SCALE[self.instrument]:
            return {'current_weight': current_weight, 'target_weight': target_weight, 'clean': False,
                    'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc, 'alpha scale': alpha_scale,'kappa': self.transaction_cost.kappa}
        else:
            if alpha_scale != SET.ALPHA_SCALE[self.instrument] and alpha_scale != 0:
                return {'current_weight': current_weight, 'target_weight': target_weight, 'clean': True,
                        'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc,
                        'alpha scale': alpha_scale, 'kappa': self.transaction_cost.kappa}
            elif alpha_scale == 0:
                return {'current_weight': current_weight, 'target_weight': SET.POSITION_CLEAN, 'clean': True,
                        'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc,
                        'alpha scale': alpha_scale, 'kappa': self.transaction_cost.kappa}