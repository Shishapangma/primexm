from queue import Empty
from threading import Thread


class MarketStreamHandler(Thread):
    """
    Module to deal with tick data
    """

    def __init__(self, trader, queue):
        Thread.__init__(self)

        self.market_queue = queue
        self.portfolio = trader.portfolio

    def run(self):
        self.active = True
        while self.active:
            try:
                event = self.market_queue.get(block=True, timeout=0.001)
                self.process(event)
            except Empty:
                pass

    def process(self, event):
        # tick event
        for order_type in self.portfolio[event.instrument]:
            self.portfolio[event.instrument][order_type].perf_monitor.on_update_tick(event)

