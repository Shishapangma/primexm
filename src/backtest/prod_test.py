import pandas as pd
from collections import deque
from datetime import datetime
import threading
import queue
from production.event import *
import math
import production.stat_functions as sf
import production.settings as SET
import research.PerformanceMeasures as pm
from production.backtest.test import *

t = A()
f()



import logging
from logging import handlers

logger = logging.getLogger('fx_system')
hdlr = logging.FileHandler('log/fx_log.txt', 'a')
formatter = logging.Formatter('%(message)s')
formatter.converter = time.gmtime
hdlr.setFormatter(formatter)
memoryhandler = handlers.MemoryHandler(1024*10, logging.INFO, target=hdlr)
logger.addHandler(memoryhandler)
logger.setLevel(logging.INFO)

logging.basicConfig(format='%(message)s', level=logging.INFO)
logging.Formatter.converter = time.gmtime


class AlphaSignal(object):
    def __init__(self, alpha_winsorize, alpha_scale):
        self.alpha_signal_t = 0
        self.short_ema_t = 0
        self.long_ema_t = 0
        self.sigma_ema_t = 0
        self.short_half_life = SET.SHORT_HALF_LIFE
        self.long_half_life = SET.LONG_HALF_LIFE
        self.alpha_winsorize = alpha_winsorize
        self.alpha_scale = alpha_scale
        self.exp_short_life_weight = sf.get_exponential_weight(self.short_half_life)
        self.exp_long_life_weight = sf.get_exponential_weight(self.long_half_life)
        self.exp_decay_half_life = sf.get_exponential_weight(10)

    def get_simple_reversion_alpha(self, return_t, short_ema_pre, long_ema_pre, sigma_ema_pre, alpha_scale):
        self.short_ema_t = return_t * (1 - self.exp_short_life_weight) + short_ema_pre * self.exp_short_life_weight
        self.long_ema_t = return_t * (1 - self.exp_long_life_weight) + long_ema_pre * self.exp_long_life_weight
        self.sigma_ema_t = (np.sqrt(self.exp_long_life_weight * (
            sigma_ema_pre ** 2 + (1 - self.exp_long_life_weight) * ((return_t - long_ema_pre) ** 2))))
        self.alpha_signal_t = (self.long_ema_t - self.short_ema_t) / self.sigma_ema_t * alpha_scale
        if math.isnan(self.alpha_signal_t):
            self.alpha_signal_t = 0
            self.short_ema_t = 0
            self.long_ema_t = 0
            self.sigma_ema_t = 0
        if self.alpha_signal_t > self.alpha_winsorize:
            self.alpha_signal_t = self.alpha_winsorize
        elif self.alpha_signal_t < -self.alpha_winsorize:
            self.alpha_signal_t = -self.alpha_winsorize

    def get_alpha_scale_with_weight(self, scale_weight):
        return self.alpha_scale * scale_weight


class TransactionCostModel(object):
    def __init__(self, kappa, half_life):
        self.tc = 0
        self.kappa = kappa
        self.spread_half_life = half_life
        self.spread_bid_ema = 0
        self.spread_ask_ema = 0
        self.exp_life_weight = sf.get_exponential_weight(self.spread_half_life)

    def update_transaction_cost(self, bid=0, ask=0, tc_rebate=0, commission=0):
        pass


class LimitTransactionCostModel(TransactionCostModel):
    def __init__(self, kappa, half_life):
        TransactionCostModel.__init__(self, kappa, half_life)

    def update_transaction_cost(self, bid=0, ask=0, tc_rebate=0, commission=0):
        """
        Should be turned based on condition
        :param bid:
        :param ask:
        :param tc_rebate:
        :param commission:
        :return:
        """
        self.tc = 0.09


class MarketTransactionCostModel(TransactionCostModel):
    def __init__(self, kappa, half_life):
        TransactionCostModel.__init__(self, kappa, half_life)

    def update_transaction_cost(self, bid=0, ask=0, tc_rebate=0, commission=0):
        # self.spread_bid_ema = bid * (1 - self.exp_life_weight) + self.spread_bid_ema * self.exp_life_weight
        # self.spread_ask_ema = ask * (1 - self.exp_life_weight) + self.spread_ask_ema * self.exp_life_weight
        # self.tc = (((self.spread_ask_ema - self.spread_bid_ema - tc_rebate / 10000) /
        #             (self.spread_bid_ema + self.spread_ask_ema) + commission) * 10000) + 0.09

        self.tc = ((ask - bid) / (ask + bid) + 0.09/10000) * 10000


class FxStrategy(object):
    def __init__(self, trader, instrument, portfolio):
        # handler of Trader instance, in case of requiring high level colleborated task
        self.trader = trader

        # Specific instrument Index
        self.instrument = instrument

        # Handler of AlphaSignal instance
        self.alpha = AlphaSignal(SET.ALPHA_WINSORIZE[self.instrument], SET.ALPHA_SCALE[self.instrument])

        # Handler of Portfolio instance
        self.portfolio = portfolio

        # Handler of TransactionCostModel instance
        self.transaction_cost = None

    def get_weight_simple_alpha_with_tc(self, weight_current):
        if self.alpha.alpha_signal_t > (weight_current + self.transaction_cost.tc * self.transaction_cost.kappa):
            target_weight_t = self.alpha.alpha_signal_t - self.transaction_cost.tc * self.transaction_cost.kappa
        elif self.alpha.alpha_signal_t < (weight_current - self.transaction_cost.tc * self.transaction_cost.kappa):
            target_weight_t = self.alpha.alpha_signal_t + self.transaction_cost.tc * self.transaction_cost.kappa
        else:
            target_weight_t = weight_current
        return target_weight_t

    def init_historical_alpha(self, event):
        """
        Initilize system with 400 historical data at the beginning of startup
        :param event: Event contains 400 past observation
        :return: None
        """
        pass

    def get_target_weight(self, event, scale_weight):
        """
        Function is valid when after historical data initialized completed
        :param event: Event contains return of bid
        :return: updated weight
        """
        pass


class LimitFxStrategy(FxStrategy):
    def __init__(self, trader, instrument, portfolio):
        FxStrategy.__init__(self, trader, instrument, portfolio)
        self.transaction_cost = LimitTransactionCostModel(SET.KAPPA[self.instrument], SET.SPREAD_HALF_LIFE)

    def init_historical_alpha(self, event):
        """
        Initilize system with 400 historical data at the beginning of startup
        :param event: Event contains 400 past observation
        :return: None
        """
        candles = event.candles
        bids = candles['closeBid']
        asks = candles['closeAsk']
        ret = candles['ret']
        for i in range(0, candles.shape[0]):
            self.alpha.get_simple_reversion_alpha(return_t=ret[i],
                                                  short_ema_pre=self.alpha.short_ema_t,
                                                  long_ema_pre=self.alpha.long_ema_t,
                                                  sigma_ema_pre=self.alpha.sigma_ema_t,
                                                  alpha_scale=self.alpha.alpha_scale)
            self.transaction_cost.update_transaction_cost(bid=bids[i], ask=asks[i])
        logger.info('[Init Historical Alpha] [%s] time %s bid %s ask %s' % (self.instrument, candles.index[-1],
                                                                            str(bids[-1]), str(asks[-1])))
        return {'time': candles.index[-1], 'bid': bids[-1], 'ask': asks[-1]}

    def get_target_weight(self, event, scale_weight):
        """
        Function is valid when after historical data initialized completed
        :param event: Event contains return of bid
        :param scale_weight: 
        :return: updated weight
        """
        ret = event.ret
        alpha_scale = self.alpha.get_alpha_scale_with_weight(scale_weight)
        self.alpha.get_simple_reversion_alpha(return_t=ret,
                                              short_ema_pre=self.alpha.short_ema_t,
                                              long_ema_pre=self.alpha.long_ema_t,
                                              sigma_ema_pre=self.alpha.sigma_ema_t, alpha_scale=alpha_scale)
        self.transaction_cost.update_transaction_cost(event.bid, event.ask)
        current_weight = self.portfolio.get_current_weight()
        target_weight = self.get_weight_simple_alpha_with_tc(weight_current=current_weight)

        # No alpha turning
        if alpha_scale == SET.ALPHA_SCALE[self.instrument]:
            return {'current_weight': current_weight, 'target_weight': target_weight, 'clean': False,
                    'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc, 'alpha scale': alpha_scale, 'kappa:': self.transaction_cost.kappa}
        else:
            if alpha_scale != SET.ALPHA_SCALE[self.instrument] and alpha_scale != 0:
                return {'current_weight': current_weight, 'target_weight': target_weight, 'clean': True,
                        'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc,
                        'alpha scale': alpha_scale, 'kappa:': self.transaction_cost.kappa}
            elif alpha_scale == 0:
                return {'current_weight': current_weight, 'target_weight': SET.POSITION_CLEAN, 'clean': True,
                        'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc,
                        'alpha scale': alpha_scale, 'kappa:': self.transaction_cost.kappa}


class MarketFxStrategy(FxStrategy):
    def __init__(self, trader, instrument, portfolio):
        FxStrategy.__init__(self, trader, instrument, portfolio)
        self.transaction_cost = MarketTransactionCostModel(SET.KAPPA[self.instrument], SET.SPREAD_HALF_LIFE)

    def init_historical_alpha(self, event):
        """
        Initilize system with 400 historical data at the beginning of startup
        :param event: Event contains 400 past observation
        :return: None
        """
        candles = event.candles
        bids = candles['closeBid']
        asks = candles['closeAsk']
        ret = candles['ret']
        for i in range(0, candles.shape[0]):
            self.alpha.get_simple_reversion_alpha(return_t=ret[i],
                                                  short_ema_pre=self.alpha.short_ema_t,
                                                  long_ema_pre=self.alpha.long_ema_t,
                                                  sigma_ema_pre=self.alpha.sigma_ema_t,
                                                  alpha_scale=self.alpha.alpha_scale)
            self.transaction_cost.update_transaction_cost(bid=bids[i], ask=asks[i])
        logger.info('[Init Historical Alpha] [%s] time %s bid %s ask %s' % (self.instrument, candles.index[-1],
                                                                            str(bids[-1]), str(asks[-1])))
        return {'time': candles.index[-1], 'bid': bids[-1], 'ask': asks[-1]}

    def get_target_weight(self, event, scale_weight):
        """
        Function is valid when after historical data initialized completed
        :param event: Event contains return of bid
        :return: updated weight
        """
        ret = event.ret
        alpha_scale = self.alpha.get_alpha_scale_with_weight(scale_weight)
        self.alpha.get_simple_reversion_alpha(return_t=ret,
                                              short_ema_pre=self.alpha.short_ema_t,
                                              long_ema_pre=self.alpha.long_ema_t,
                                              sigma_ema_pre=self.alpha.sigma_ema_t, alpha_scale=alpha_scale)
        # self.transaction_cost.update_transaction_cost(event.bid, event.ask)
        current_weight = self.portfolio.get_current_weight()
        target_weight = self.get_weight_simple_alpha_with_tc(weight_current=current_weight)

        # No alpha turning
        if alpha_scale == SET.ALPHA_SCALE[self.instrument]:
            return {'current_weight': current_weight, 'target_weight': target_weight, 'clean': False,
                    'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc, 'alpha scale': alpha_scale,'kappa': self.transaction_cost.kappa}
        else:
            if alpha_scale != SET.ALPHA_SCALE[self.instrument] and alpha_scale != 0:
                return {'current_weight': current_weight, 'target_weight': target_weight, 'clean': True,
                        'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc,
                        'alpha scale': alpha_scale, 'kappa': self.transaction_cost.kappa}
            elif alpha_scale == 0:
                return {'current_weight': current_weight, 'target_weight': SET.POSITION_CLEAN, 'clean': True,
                        'alpha': self.alpha.alpha_signal_t, 'tc': self.transaction_cost.tc,
                        'alpha scale': alpha_scale, 'kappa': self.transaction_cost.kappa}


class MarketDataHandler:
    def __init__(self, trade_handler, instrument, order_type):
        self.instrument = instrument
        self.order_type = order_type
        self.trade_handler = trade_handler

        self.market_ticks = deque(maxlen=2)
        self.last_bid = np.nan
        self.last_ask = np.nan
        self.last_ret = np.nan
        self.last_tick_date = datetime.utcnow().strftime('%Y%m%d')
        self.last_tick_time = datetime.utcnow().strftime('%H:%M:%S.%f')[:-3]

        self.lock = threading.Lock()
        self.last_sec = 0
        self.flag = True

    def update_tick(self, event):
        """
        Duka min
        :param event:
        :return:
        """
        self.lock.acquire()
        self.last_bid = event.bid
        self.last_ask = event.ask
        self.last_tick_date = event.date
        self.last_tick_time = event.time
        self.lock.release()

        self.update_spread()
        self.update_market_data()

    def update_market_data(self):
        """
        handler for market data deliver
        :return:
        """
        self.lock.acquire()
        date = self.last_tick_date
        t_time = self.last_tick_time
        bid = self.last_bid
        ask = self.last_ask
        self.lock.release()
        if not np.isnan(ask):
            self.market_ticks.append((bid, ask))
        if len(self.market_ticks) == 2:
            pre_mid = (self.market_ticks[0][0] + self.market_ticks[0][1]) / 2
            last_mid = (self.market_ticks[1][0] + self.market_ticks[1][1]) / 2
            self.last_ret = (last_mid - pre_mid) / pre_mid
            market_data_event = MarketDataEvent(self.instrument, date, t_time, bid, ask, self.last_ret, self.order_type)
            self.trade_handler.on_trade_event(market_data_event)
            self.trade_handler.portfolio[self.instrument][self.order_type].perf_monitor.interval_data_generate(
                market_data_event, self.order_type)

            # logger.info('[Tick Data] [%s] time %s bid %s ask %s return %s' % (
            #     self.instrument, str(t_time), str(bid), str(ask), str(self.last_ret)))

    def update_spread(self):
        if not np.isnan(self.last_ask):
            # spread_event = SpreadEvent(self.instrument, self.last_tick_date, self.last_tick_time, self.last_bid,
            #                            self.last_ask)
            self.trade_handler.fx_strategy[self.instrument][SET.OrderType.MARKET_ORDER].transaction_cost.update_transaction_cost(self.last_bid, self.last_ask)

    def initialize_market_data(self, bid, ask):
        self.market_ticks.append((bid, ask))


class SimMarketWrapper:
    def __init__(self, trade_handler, instrument, trade_wrapper):
        self.instrument = instrument
        self.trade_wrapper = trade_wrapper
        self.trade_handler = trade_handler

    def market_data_refresh(self, msg):
        self.trade_wrapper.set_current_price(bid=msg[2], ask=msg[3])
        self.trade_wrapper.set_time_slot(t_date=msg[0], t_time=msg[1])
        tick_event = TickEvent(self.instrument, date=msg[0], time=msg[1], bid=msg[2], ask=msg[3], bid_vol=0, ask_vol=0)

        for order_type in self.trade_handler.portfolio[self.instrument]:
            self.trade_handler.market_data_handler[self.instrument][order_type].update_tick(tick_event)


class DataFeeder:
    def __init__(self, market_wrapper, instrument, path, log_file):
        self.market_wrapper = market_wrapper
        self.instrument = instrument
        self.data_pool = None
        self.read_duka_log(path, log_file)

        instrument = self.instrument.lower().replace('/', '')

        tc = pd.read_csv('/Users/jerryhuang/Documents/Trading Data/4-TC/tc_' + instrument + '.csv', header=0)
        self.spread_by_hour = tc.ix[:, 0].tolist()

    def read_duka_log(self, path, log_file):
        self.data_pool = pd.read_csv(path + log_file + '.csv', header=0)
        self.data_pool.index = pd.to_datetime(self.data_pool['Time (UTC)'], format="%d.%m.%Y %H:%M:%S")
        self.data_pool = pm.fx_data_cleaning(self.data_pool)

    def data_process_duka(self):
        for j in range(0, len(self.data_pool)):
            s_time = self.data_pool.index[j].strftime('%H:%M:%S')
            s_date = self.data_pool.index[j].strftime('%d.%m.%Y')
            s_date = datetime.strptime(s_date, '%d.%m.%Y').strftime('%Y%m%d')
            hour = datetime.strptime(s_time, '%H:%M:%S').hour
            bid = self.data_pool['Close'][j]
            ask = bid + self.spread_by_hour[hour]
            self.market_wrapper.market_data_refresh((s_date, s_time, bid, ask))
            print(s_date, s_time)

        for order_type in self.market_wrapper.trade_handler.portfolio[self.market_wrapper.instrument]:
            self.market_wrapper.trade_handler.portfolio[self.market_wrapper.instrument][
                order_type].perf_monitor.save_file()

    def read_lmax_log(self, path, log_file):
        """
        initial data pool 
        :return: 
        """
        self.data_pool = pd.read_csv(path + log_file + '.csv', header=0)
        self.data_pool = self.data_pool[self.data_pool['event'] == 'Dummy_Tick']
        self.data_pool['time_index'] = self.data_pool['date'].astype(str) + '-' + self.data_pool['time']
        self.data_pool.index = pd.to_datetime(self.data_pool['time_index'].str[:-1], format='%Y%m%d-%H:%M:%S.%f')
        self.data_pool['bid'] = self.data_pool['bid'].astype(float)
        self.data_pool['ask'] = self.data_pool['ask'].astype(float)
        self.data_pool['spread'] = self.data_pool['ask'] - self.data_pool['bid']

    def data_process_lmax(self):
        for j in range(0, len(self.data_pool)):
            s_time = self.data_pool.index[j].strftime('%H:%M:%S.%f')
            s_date = self.data_pool.index[j].strftime('%Y%m%d')
            bid = self.data_pool['bid'][j]
            ask = self.data_pool['ask'][j]
            self.market_wrapper.market_data_refresh((s_date, s_time, bid, ask))
            # time.sleep(0.005)
            # print(s_date, s_time)

        for order_type in self.market_wrapper.trade_handler.portfolio[self.market_wrapper.instrument]:
            self.market_wrapper.trade_handler.portfolio[self.market_wrapper.instrument][order_type].perf_monitor.save_file()


class SimTradeWrapper:
    def __init__(self, instrument):
        self.instrument = instrument
        self.trade_handler = None
        self.ask = 0
        self.bid = 0
        self.date = None
        self.time = None

    def new_order_single(self, order):
        # logger.info('[New Order Request] [%s] Type %s %s' % (order.instrument, order.type, order.units))

        units = round((order.units / 10000), 1)

        if self.fill_simulator(units):
            self.on_execute_report('0', order, units)
            self.on_execute_report('F', order, units)
        else:
            self.on_execute_report('8', order, units)

    def fill_simulator(self, units):
        if units >= 0.1:
            return True
        else:
            return False

    def on_execute_report(self, execType, order, units):
        order_type = ''
        price = 0.0

        if execType == '0':
            t_date = self.date
            t_time = self.time

            if order.side == '1':
                units *= 10000
            else:
                units *= -10000
            if order.type == 'LIMIT_ORDER':
                price = order.price
                order_type = OrderType.LIMIT_ORDER
            elif order.type == 'MARKET_ORDER':
                price = 0.0
                order_type = OrderType.MARKET_ORDER

            event = OrderCreateEvent(t_date, t_time, self.instrument, int(units), price, order.side, 0,
                                     0, order_type)
            self.trade_handler.on_order_create(event)
            # logger.info('[New Order Confirmed] %s', str(units))

        elif execType == 'F':
            t_date = self.date
            t_time = self.time

            if order.type == 'LIMIT_ORDER':
                price = order.price
                order_type = OrderType.LIMIT_ORDER
            elif order.type == 'MARKET_ORDER':
                order_type = OrderType.MARKET_ORDER

            if order.side == '1':
                units *= 10000
                price = self.ask
            else:
                units *= -10000
                price = self.bid

            # logger.info('[Order Filled] %s', str(units))
            event = OrderFilledEvent(self.instrument, t_date, t_time, int(units), int(units), price, 0, order_type)
            self.trade_handler.on_order_filled(event)

        elif execType == '8':
            event = OrderCreateRejectEvent(self.instrument)
            self.trade_handler.on_order_create_reject(event)
            # logger.info('[Order Create Rejected]')

    def set_current_price(self, bid, ask):
        """
        no slippage assumpation
        :param bid: 
        :param ask: 
        :return: 
        """
        self.ask = ask
        self.bid = bid

    def set_time_slot(self, t_date, t_time):
        self.date = t_date
        self.time = t_time

    def set_handler(self, handler):
        self.trade_handler = handler


class Portfolio(object):
    def __init__(self, instrument, data_house, market_data_handler, order_type):
        self.instrument = instrument
        # data source handler
        self.data_house = data_house

        if self.instrument.count('jpy') == 1:
            self.precise = 3
        else:
            self.precise = 5

        # performance monitor handler
        self.perf_monitor = PerformanceMonitor(self, market_data_handler, order_type)

        # initial position is Zero
        self.current_position = 0
        self.current_weight = 0

        # order type indicator
        self.order_type = order_type

    def init_trading(self, net_asset_value, exchange_price, position):
        self.net_asset_value = net_asset_value
        self.exchange_price = exchange_price
        self.perf_monitor.last_current_position = position
        # load the current position
        # self.update_current_weight()

    def weight_to_dollar_position(self, weight, price):
        """
        Cal funding currency position by multiple weight
        Covert funding currency position to trading currency position
        :param weight:
        :return: trading currency position
        """
        return (weight * self.net_asset_value * self.exchange_price) / price

    def dollar_position_to_weight(self, price):
        """
        Convert trading currency position to funding currency position
        Convert funding currency position to weight
        :param
        :return: funding currency weight
        """
        return (self.current_position / self.exchange_price / self.net_asset_value) * price

    def get_current_weight(self):
        """
        Fetch existing quantity of units and convert into weight
        Prefer to fetch real current quantity of units than using static update
        especially for limit order which the order will not be executed every time
        :return: current weight
        """
        return self.current_weight

    def update_current_weight(self, price):
        """
        split udpate from get. update may happen at different chance
        :return:
        """
        self.current_position = self.perf_monitor.last_current_position
        self.current_weight = self.dollar_position_to_weight(price)

    def generate_trade_order(self, alpha_result, tick_event):
        """
        Calculate new position based on target weight
        if new position differ with the current position then generate order to fill it
        :param alpha_result: 
        :param tick_event: 
        :return: 
        """

        current_weight = alpha_result['current_weight']
        target_weight = alpha_result['target_weight']
        if current_weight != target_weight and target_weight != SET.POSITION_CLEAN:
            target_position = np.int(self.weight_to_dollar_position(target_weight, (tick_event.ask + tick_event.bid) / 2))
            trade_position = target_position - self.current_position

            # if remaining position is less then minimum requirement, clean in one go
            if alpha_result['clean'] and np.abs(target_position) < 1000:
                logger.info('target position is less then minimum requirement %s' % target_position)
                target_position = 0
                trade_position = target_position - self.current_position
            # logger.info(
            #     # '[Position] [%s] Type %s Trade Position:%s Target Position:%s Current Position:%s' % (self.instrument, str(self.order_type), str(trade_position),
            #     #                                                                  str(target_position),
            #     #                                                                  str(self.current_position)))
            # position has to be modified
            if trade_position != 0:
                units = np.abs(trade_position)
                # Sell
                if np.sign(trade_position) == -1:
                    if self.order_type == SET.OrderType.LIMIT_ORDER:
                        price = round((tick_event.ask - SET.X[self.instrument]), self.precise)
                        return LimitOrderEvent(self.instrument, units, '2', price)
                    else:
                        return MarketOrderEvent(self.instrument, units, '2')
                # Buy
                else:
                    if self.order_type == SET.OrderType.LIMIT_ORDER:
                        price = round((tick_event.bid + SET.X[self.instrument]), self.precise)
                        return LimitOrderEvent(self.instrument, units, '1', price)
                    else:
                        return MarketOrderEvent(self.instrument, units, '1')
            else:
                return None
        # sepcial case for position empty
        elif target_weight == SET.POSITION_CLEAN:
            units = np.abs(self.current_position)
            # short position, buy back
            if np.sign(self.current_position) == -1:
                if self.order_type == SET.OrderType.LIMIT_ORDER:
                    price = round((tick_event.bid + SET.X[self.instrument]), self.precise)
                    return LimitOrderEvent(self.instrument, units, '1', price)
                else:
                    return MarketOrderEvent(self.instrument, units, '1')
            # long position, sell out
            elif np.sign(self.current_position) == 1:
                if self.order_type == SET.OrderType.LIMIT_ORDER:
                    price = round((tick_event.ask - SET.X[self.instrument]), self.precise)
                    return LimitOrderEvent(self.instrument, units, '2', price)
                else:
                    return MarketOrderEvent(self.instrument, units, '2')
            # NO position at all, do nothing
            else:
                return None
        else:
            return None


class TradeHandler:
    def __init__(self, fix_wrapper, queues, t_time=None):

        # Event queue is to collebrating the time series between different tasks
        self.trade_queue = queues['TRADE']

        # Switch to control ON/OFF trading process
        self.active = True
        # Swtich to control ON/OFF trade
        self.halt = False

        # Handles of all kind of trading relevant instance
        self.fix_wrapper = fix_wrapper

        # Handles of individual instrument components
        self.fx_strategy = {}
        self.portfolio = {}
        self.market_data_handler = {}
        # store current unique order of per instrument
        self.orders = {}

        if t_time:
            self.event_calendar = EventCalendar(t_time)
        else:
            self.event_calendar = EventCalendar()

        for ins in SET.INSTRUMENT:
            # portfolio instance
            self.portfolio[ins] = {}
            self.fx_strategy[ins] = {}
            self.market_data_handler[ins] = {}

            for order_type in SET.INSTRUMENT_ORDER_TYPE[ins]:
                self.market_data_handler[ins][order_type] = MarketDataHandler(self, ins, order_type)
                self.portfolio[ins][order_type] = Portfolio(ins, self.fix_wrapper, self.market_data_handler[ins][order_type], order_type)
                self.portfolio[ins][order_type].init_trading(SET.INITIAL_NET_ASSET,
                                                             SET.EXCHANGE_RATE[ins],
                                                             SET.REMAIN_POSITION[ins][order_type])
                if order_type == SET.OrderType.LIMIT_ORDER:
                    self.fx_strategy[ins][order_type] = LimitFxStrategy(self, ins, self.portfolio[ins][order_type])
                else:
                    self.fx_strategy[ins][order_type] = MarketFxStrategy(self, ins, self.portfolio[ins][order_type])



            # order stats instance
            self.orders[ins] = {}

        self.fix_wrapper.set_handler(self)

    def on_trade_event(self, event):
        self.event_calendar.update_alpha_weight(event.instrument, event.date, event.time)
        alpha_weight = self.event_calendar.get_alpha_weight(event.instrument)

        alpha_result = self.fx_strategy[event.instrument][event.order_type].get_target_weight(event,
                                                                                              alpha_weight)
        # logger.info('time:%s, return:%s, TC:%s, current:%s, target:%s, kappa:%s', str(event.time), str(event.ret),
        #             str(alpha_result['tc']), str(alpha_result['current_weight']),
        #             str(alpha_result['target_weight']), str(alpha_result['kappa']))
        # logger.info(
        #     '[Alpha Weight] [%s] Type %s alpha weight-%s current weight-%s target weight-%s',
        #     event.instrument, str(event.order_type), str(alpha_weight), str(alpha_result['current_weight']),
        #     str(alpha_result['target_weight']))
        # self.portfolio[event.instrument][event.order_type].perf_monitor.interval_data_generate(event,
        #                                                                                        event.order_type,
        #                                                                                        alpha_result
        #                                                                                        )
        order_event = self.portfolio[event.instrument][event.order_type].generate_trade_order(
            alpha_result=alpha_result,
            tick_event=event)
        # Order Execution
        if order_event:
            # t3 = time.time()
            # logger.info('[Tick 2 Order] [%s] %s', event.instrument, str(t3 - event.t1))

            # close existing limit order which not been filled when new order raised
            if order_event.order_type in self.orders[order_event.instrument]:
                self.orders[order_event.instrument][order_event.order_type].order_saver = order_event
                try:
                    self.fix_wrapper.order_cancel_request(
                        self.orders[order_event.instrument][order_event.order_type])
                    self.orders[order_event.instrument][
                        order_event.order_type].state = OrderHandler.OrderStats.CANCELLING
                except Exception as e:
                    pass
            # or execute order straightaway
            else:
                # execute order
                try:
                    clOrderId = self.fix_wrapper.new_order_single(order_event)
                    # self.orders[event.instrument][event.order_type] = OrderHandler(event.instrument, clOrderId)
                    # self.orders[event.instrument][event.order_type].state = OrderHandler.OrderStats.CREATING
                except Exception as e:
                    logger.error('[Exception Create Order] [%s] Type %s %s' % (
                        order_event.instrument, order_event.order_type, e))

    def on_order_create(self, event):
        # if self.orders[event.instrument][event.order_type].state == OrderHandler.OrderStats.CREATING:
        #     self.orders[event.instrument][event.order_type].update(event.units, event.side, event.orderId)
        #     self.orders[event.instrument][event.order_type].state = OrderHandler.OrderStats.CREATED
        self.portfolio[event.instrument][event.order_type].perf_monitor.order_create(event, event.order_type)

    def on_order_filling(self, event):
        if event.order_type in self.orders[event.instrument]:
            self.orders[event.instrument][event.order_type].total_leaves_unit = event.total_leaves_units
            self.orders[event.instrument][event.order_type].state = OrderHandler.OrderStats.FILLING
            self.portfolio[event.instrument][event.order_type].perf_monitor.order_filling(event, event.order_type)

    def on_order_filled(self, event):
        # if event.order_type in self.orders[event.instrument]:
        self.portfolio[event.instrument][event.order_type].perf_monitor.order_filled(event, event.order_type)
            # self.orders[event.instrument].pop(event.order_type)

    def on_order_cancel(self, event):
        if event.order_type in self.orders[event.instrument]:
            try:
                clOrderId = self.fix_wrapper.new_order_single(
                    self.orders[event.instrument][event.order_type].order_saver)
                self.orders[event.instrument].pop(event.order_type)
                self.orders[event.instrument][event.order_type] = OrderHandler(event.instrument, clOrderId)
                self.orders[event.instrument][event.order_type].state = OrderHandler.OrderStats.CREATING
            except Exception as e:
                logger.error('[Exception Create Order] [%s] Type %s %s' % (
                    event.instrument, event.order_type, e))

    def on_order_create_reject(self, event):
        if SET.OrderType.LIMIT_ORDER in self.orders[event.instrument]:
            self.orders[event.instrument].pop(SET.OrderType.LIMIT_ORDER)
        elif SET.OrderType.MARKET_ORDER in self.orders[event.instrument]:
            self.orders[event.instrument].pop(SET.OrderType.MARKET_ORDER)

    def on_order_cancel_reject(self, event):
        for instrument in self.orders:
            for order_type in self.orders[instrument]:
                if self.orders[instrument][order_type].clOrderId == event.clOrderId:
                    self.orders[instrument].pop(order_type)
                    break


class PerformanceMonitor:
    def __init__(self, portfolior, market_data_handler, order_type):
        # Handler of portfolior
        self.portfolior = portfolior

        self.order_type = order_type

        # track every tick update using for slippage study during order execution
        self.last_target_position = 0
        self.last_current_position = 0

        self.market_data_handler = market_data_handler

        # self.timer_dp = Timer(3600, self.dump_log)
        # self.timer_dp.start()

        # allocate Dic for saving minutes return
        self.data_columns = ['date',
                             'time',
                             'instrument',
                             'event',
                             'order',
                             'bid',
                             'ask',
                             'bid vol',
                             'ask vol',
                             'return',
                             'current position',
                             'target position',
                             'traded position',
                             'target price',
                             'trade price']

        self.records = []

        # for filled bid/ask update
        self.current_filled = -1
        self.trade_size = 0

        # for transaction record
        # self.data_store = pd.DataFrame(columns=self.data_columns)

    def interval_data_generate(self, event, order_type):
        """
        fill when tick event trigger
        :param event: 
        :param order_type: 
        :param alpha_result: 
        :return: 
        """
        self.market_data_handler.lock.acquire()
        bid = self.market_data_handler.last_bid
        ask = self.market_data_handler.last_ask
        self.market_data_handler.lock.release()

        order_type = 'MARKET' if order_type == SET.OrderType.MARKET_ORDER else 'LIMIT'

        self.records.append((event.date,
                             event.time,
                             event.instrument,
                             'One_Min_Tick',
                             order_type,
                             bid,
                             ask,
                             0,
                             0,
                             self.market_data_handler.last_ret,
                             self.last_current_position,
                             0,
                             0,
                             0,
                             0))

        return self.last_current_position

    def order_create(self, event, order_type):
        """
        order accepted confirm created
        :param event:
        :return:
        """
        self.market_data_handler.lock.acquire()
        bid = self.market_data_handler.last_bid
        ask = self.market_data_handler.last_ask
        self.market_data_handler.lock.release()

        order_type = 'MARKET' if order_type == SET.OrderType.MARKET_ORDER else 'LIMIT'

        self.last_target_position = self.last_current_position + event.units
        self.records.append((event.date,
                             event.time,
                             event.instrument,
                             'Order_Create',
                             order_type,
                             bid,
                             ask,
                             0,
                             0,
                             self.market_data_handler.last_ret,
                             self.last_current_position,
                             self.last_current_position + event.units,
                             0,
                             event.price,
                             0))

    def order_filling(self, event, order_type):
        """
        fill when order be filling
        :param event:
        :return:
        """
        self.market_data_handler.lock.acquire()
        bid = self.market_data_handler.last_bid
        ask = self.market_data_handler.last_ask
        self.market_data_handler.lock.release()

        self.last_current_position += event.this_filled_units

        order_type = 'MARKET' if order_type == SET.OrderType.MARKET_ORDER else 'LIMIT'

        self.records.append((event.date,
                             event.time,
                             event.instrument,
                             'Order_Filling',
                             order_type,
                             bid,
                             ask,
                             0,
                             0,
                             self.market_data_handler.last_ret,
                             self.last_current_position,
                             self.last_target_position,
                             event.this_filled_units,
                             0,
                             event.price))

        self.portfolior.update_current_weight((bid + ask) / 2)

    def order_filled(self, event, order_type):
        """
        order filled event handler
        :param event:
        :return:
        """
        self.market_data_handler.lock.acquire()
        bid = self.market_data_handler.last_bid
        ask = self.market_data_handler.last_ask
        self.market_data_handler.lock.release()

        # update position
        self.last_current_position += event.this_filled_units

        order_type = 'MARKET' if order_type == SET.OrderType.MARKET_ORDER else 'LIMIT'

        self.records.append((event.date,
                             event.time,
                             event.instrument,
                             'Order_Filled',
                             order_type,
                             bid,
                             ask,
                             0,
                             0,
                             self.market_data_handler.last_ret,
                             self.last_current_position,
                             self.last_target_position,
                             event.this_filled_units,
                             0,
                             event.price))

        self.portfolior.update_current_weight((bid + ask) / 2)

    def save_file(self):
        """
        save data in configured interval
        :return:
        """
        # l = self.data_store.shape[0]

        if self.order_type == SET.OrderType.MARKET_ORDER:
            suffix = '.market'
        else:
            suffix = '.limit'
        instrument = self.portfolior.instrument
        instrument = instrument.replace('/', '_')

        # 4. sim log保存目录
        file_name = '/Users/jerryhuang/Documents/Trading Data/6-Simulate Log/' + instrument + suffix + '.csv'
        data_store = pd.DataFrame(self.records, columns=self.data_columns)
        data_store.to_csv(file_name, index=False, header=True, mode='a')
        # self.data_store.to_csv(file_name, index=False, header=True, mode='a')
        # del self.data_store
        # self.data_store = pd.DataFrame(columns=self.data_columns)
        logger.info('[Saved File.....]')


class Calender:
    MAX_LIFE = 25
    # decay duration
    ACTIVE = 0
    # default stats is deactive
    DEACTIVE = 1
    # pre action decay completed
    PRE_ACTION_EXPIRED = 2
    # during between PRE_ACTION_EXPIRED and re ACTIVE
    ACTIVE_HALT = 3
    # Post action decay completed
    POST_ACTION_EXPIRED = 4
    # full completed
    END = 5
    # internally using for indicating decay direction
    PRE_ACTION = 0
    POST_ACTION = 1

    def __init__(self, instrument, news, pre_action_time, post_action_time, t_time=None):
        self.instrument = instrument
        self.news = news
        self.stat = Calender.DEACTIVE
        self.pre_action_time = pre_action_time
        self.post_action_time = post_action_time
        self.flag = Calender.PRE_ACTION
        self.left_time = 0

        self.initial_decay(t_time)

    def decay(self):
        # positive decay- decrease
        if self.flag == Calender.PRE_ACTION:
            self.left_time += 1
            if self.left_time >= Calender.MAX_LIFE:
                self.stat = Calender.PRE_ACTION_EXPIRED
                self.flag = Calender.POST_ACTION
        # negative decay-increase
        elif self.flag == Calender.POST_ACTION:
            self.left_time -= 1
            if self.left_time <= 0:
                self.stat = Calender.POST_ACTION_EXPIRED

    def initial_decay(self, t_time=None):
        """
        :param t_time: format: '2017-05-08 22:00:00'
        :return: 
        """
        if t_time:
            utc_now = datetime.strptime(t_time, '%Y%m%d %H:%M:%S')
            current_utc_time = pd.to_datetime(utc_now)
            current_utc_time = current_utc_time.tz_localize('UTC')
        else:
            utc_now = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
            current_utc_time = pd.to_datetime(utc_now)
            current_utc_time = current_utc_time.tz_localize('UTC')
        time_elapse = (current_utc_time - self.pre_action_time).total_seconds()
        # current time before pre_action. Do nothing
        if time_elapse < 0:
            pass
        # current time beyond pre_action, but still processing decay
        elif time_elapse > 0 and int(time_elapse / 60) <= Calender.MAX_LIFE:
            self.stat = Calender.ACTIVE
            self.left_time = Calender.MAX_LIFE - int(time_elapse / 60)
            logger.info('%s %s PRE Activated with %s' % (self.instrument, self.news, self.left_time))
        # Pre action expired and still earlier than post action time
        elif int(time_elapse / 60) >= Calender.MAX_LIFE and current_utc_time < self.post_action_time:
            self.stat = Calender.ACTIVE_HALT
            self.left_time = Calender.MAX_LIFE
            self.flag = Calender.POST_ACTION
            logger.info('%s %s ACTIVE HALT' % (self.instrument, self.news))
        # current time after post_action but before expired
        elif current_utc_time >= self.post_action_time and int(
                ((current_utc_time - self.post_action_time).total_seconds() / 60)) < Calender.MAX_LIFE:
            self.left_time = Calender.MAX_LIFE - int(((current_utc_time - self.post_action_time).total_seconds() / 60))
            self.flag = Calender.POST_ACTION
            self.stat = Calender.ACTIVE
            logger.info('%s %s POST Activated with %s' % (self.instrument, self.news, self.left_time))
        # post action end
        elif current_utc_time >= self.post_action_time and int(
                ((current_utc_time - self.post_action_time).total_seconds() / 60)) >= Calender.MAX_LIFE:
            self.stat = Calender.END
            self.left_time = 0
            logger.info('%s %s POST EXPIRED' % (self.instrument, self.news))


class InstrumentCalender:
    """
    Calender manager is able to deal with multiple news parallel
    """

    def __init__(self, instrument):
        self.instrument = instrument
        self.calender_list = []
        self.alpha_weigh_list = []
        self.exp_decay_half_life = sf.get_exponential_weight(10)
        self.current_exp_weight = 1

    def update_alpha_weight(self):
        """
        Update multiple ongoing news
        :return: the restrict alpha weight will be return
        """
        for action_calender in self.calender_list:
            if action_calender.stat == Calender.ACTIVE:
                self.alpha_weigh_list[
                    self.calender_list.index(action_calender)] = self.exp_decay_half_life ** action_calender.left_time
                self.current_exp_weight = min(self.alpha_weigh_list)
            elif action_calender.stat == Calender.PRE_ACTION_EXPIRED:
                self.alpha_weigh_list[self.calender_list.index(action_calender)] = 0
                action_calender.stat = Calender.ACTIVE_HALT
                self.current_exp_weight = min(self.alpha_weigh_list)
            elif action_calender.stat == Calender.ACTIVE_HALT:
                self.alpha_weigh_list[self.calender_list.index(action_calender)] = 0
                self.current_exp_weight = min(self.alpha_weigh_list)
            elif action_calender.stat == Calender.POST_ACTION_EXPIRED:
                self.alpha_weigh_list[self.calender_list.index(action_calender)] = 1
                action_calender.stat = Calender.END
                self.current_exp_weight = min(self.alpha_weigh_list)

        return self.current_exp_weight


class EventCalendar:
    """
    One instance manage all instruments calender(InstrumentCalender) instance
    """
    # 0.5 hours in advance of event in seconds to take action
    NEWS_PRE_ACTION_RANGE = 1800
    # 2.5 hours beyond of event in seconds to restore
    NEWS_POST_ACTION_RANGE = 3600

    def __init__(self, t_day=None):
        # Thread.__init__(self)
        self.calenders = {}
        self.instrument_weight = {}
        self.reset = False

        for instrument in SET.INSTRUMENT:
            self.calenders[instrument] = InstrumentCalender(instrument)
            self.instrument_weight[instrument] = 1

        # for instrument in SET.CALENDER:
        #     news_list = SET.CALENDER[instrument]
        #     for news in news_list:
        #         news_date = pd.to_datetime(news)
        #         news_date = news_date.tz_localize('UTC')
        #         pre_action_date = news_date - pd.offsets.Second(EventCalendar.NEWS_PRE_ACTION_RANGE)
        #         post_action_date = news_date + pd.offsets.Second(EventCalendar.NEWS_POST_ACTION_RANGE)
        #         self.calenders[instrument].calender_list.append(
        #             Calender(instrument, news, pre_action_date, post_action_date, t_time))
        #         self.calenders[instrument].alpha_weigh_list.append(self.calenders[instrument].current_exp_weight)

        self.init_calendar(t_day)

    def init_calendar(self, t_day, t_time='21:20:00'):
        """

        :param t_day: One day before the actual date
        :param t_time: 
        :return: 
        """
        self.reset = False

        news_day = pd.to_datetime(t_day) + pd.offsets.Day(1)
        news_day = news_day.strftime('%Y%m%d')
        t_time = t_day + ' ' + t_time

        for instrument in SET.CALENDER:
            self.calenders[instrument].calender_list = []
            news_list = SET.CALENDER[instrument]
            for news in news_list:
                news_str = news_day + ' ' + news
                news_date = datetime.strptime(news_str, '%Y%m%d %H:%M:%S')
                news_date = pd.to_datetime(news_date).tz_localize('UTC')
                logger.info('[Date Initial] %s %s', instrument, news_date)
                pre_action_date = news_date - pd.offsets.Second(EventCalendar.NEWS_PRE_ACTION_RANGE)
                post_action_date = news_date + pd.offsets.Second(EventCalendar.NEWS_POST_ACTION_RANGE)
                self.calenders[instrument].calender_list.append(
                    Calender(instrument, news_str, pre_action_date, post_action_date, t_time))
                self.calenders[instrument].alpha_weigh_list.append(self.calenders[instrument].current_exp_weight)

    def update_alpha_weight(self, instrument=None, t_date=None, t_time=None):
        """
        apply market data to trigger time relative action
        call this method after trading weight update, it will cause one minute lag but avoid trading order generating delay.
        :param event:
        :return:
        """
        if np.logical_and(t_date is not None, t_time is not None):
            current_utc_time = datetime.strptime((t_date + ' ' + t_time), '%Y%m%d %H:%M:%S')
            current_utc_time = pd.to_datetime(current_utc_time).tz_localize('UTC')
            # current_utc_time = current_utc_time.tz_localize('UTC')
        else:
            current_utc_time = pd.to_datetime(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'))
            current_utc_time = current_utc_time.tz_localize('UTC')

        # Common News Event
        if instrument:
            instrument_calender = self.calenders[instrument]
            # action section
            for action_calender in instrument_calender.calender_list:
                pre_action_time = action_calender.pre_action_time
                post_action_time = action_calender.post_action_time
                if (
                            pre_action_time - current_utc_time).total_seconds() <= 0 and action_calender.stat == Calender.DEACTIVE:
                    action_calender.stat = Calender.ACTIVE
                    logger.info('%s %s PRE Activated' % (instrument, action_calender.news))
                # Calender life elapsed after activated
                elif (
                            current_utc_time - post_action_time).total_seconds() >= 0 and action_calender.stat == Calender.ACTIVE_HALT:
                    action_calender.stat = Calender.ACTIVE
                    logger.info('%s %s POST_ACTION Activated' % (instrument, action_calender.news))
                elif action_calender.stat == Calender.ACTIVE:
                    action_calender.decay()
                    logger.info(
                        '%s %s ACTION Decay: %s' % (
                            instrument, action_calender.news, str(action_calender.left_time)))

            news_alpha_weight = instrument_calender.update_alpha_weight()

            # Overnight Event
            overnight_alpha_weight = np.nan

            if current_utc_time.hour == SET.OVERNIGHT['OVERNIGHT_HOUR_PRE']:
                if current_utc_time.minute > SET.OVERNIGHT['CUT_OFF_MIN_PRE']:
                    overnight_alpha_weight = 0
                else:
                    overnight_alpha_weight = instrument_calender.exp_decay_half_life ** current_utc_time.minute
            elif current_utc_time.hour == SET.OVERNIGHT['OVERNIGHT_HOUR_POST']:
                self.reset = True
                if current_utc_time.minute < SET.OVERNIGHT['CUT_OFF_MIN_POST']:
                    overnight_alpha_weight = 0
                else:
                    overnight_alpha_weight = instrument_calender.exp_decay_half_life ** (
                        60 - current_utc_time.minute)

            if not np.isnan(overnight_alpha_weight):
                self.instrument_weight[instrument] = min(news_alpha_weight, overnight_alpha_weight)
            else:
                if self.reset:
                    self.init_calendar(t_date)
                self.instrument_weight[instrument] = news_alpha_weight

        else:
            for instrument in SET.INSTRUMENT:
                instrument_calender = self.calenders[instrument]
                # action section
                for action_calender in instrument_calender.calender_list:
                    pre_action_time = action_calender.pre_action_time
                    post_action_time = action_calender.post_action_time
                    if (
                                pre_action_time - current_utc_time).total_seconds() <= 0 and action_calender.stat == Calender.DEACTIVE:
                        action_calender.stat = Calender.ACTIVE
                        logger.info('%s %s PRE Activated' % (instrument, action_calender.news))
                    # Calender life elapsed after activated
                    elif (
                                current_utc_time - post_action_time).total_seconds() >= 0 and action_calender.stat == Calender.ACTIVE_HALT:
                        action_calender.stat = Calender.ACTIVE
                        logger.info('%s %s POST_ACTION Activated' % (instrument, action_calender.news))
                    elif action_calender.stat == Calender.ACTIVE:
                        action_calender.decay()
                        logger.info(
                            '%s %s ACTION Decay: %s' % (
                                instrument, action_calender.news, str(action_calender.left_time)))

                news_alpha_weight = instrument_calender.update_alpha_weight()

                # Overnight Event
                overnight_alpha_weight = np.nan

                if current_utc_time.hour == SET.OVERNIGHT['OVERNIGHT_HOUR_PRE']:
                    if current_utc_time.minute > SET.OVERNIGHT['CUT_OFF_MIN_PRE']:
                        overnight_alpha_weight = 0
                    else:
                        overnight_alpha_weight = instrument_calender.exp_decay_half_life ** current_utc_time.minute
                elif current_utc_time.hour == SET.OVERNIGHT['OVERNIGHT_HOUR_POST']:
                    if current_utc_time.minute < SET.OVERNIGHT['CUT_OFF_MIN_POST']:
                        overnight_alpha_weight = 0
                    else:
                        overnight_alpha_weight = instrument_calender.exp_decay_half_life ** (
                            60 - current_utc_time.minute)

                if not np.isnan(overnight_alpha_weight):
                    # logger.info('%s weight: %s' % (event.instrument, str(min(news_alpha_weight, overnight_alpha_weight))))
                    self.instrument_weight[instrument] = min(news_alpha_weight, overnight_alpha_weight)
                else:
                    # logger.info('%s weight: %s' % (event.instrument, str(news_alpha_weight)))
                    self.instrument_weight[instrument] = news_alpha_weight

    def get_alpha_weight(self, instrument):
        """
        feedback current weight with specific instrument
        :param instrument: 
        :return: 
        """
        return self.instrument_weight[instrument]


if __name__ == '__main__':
    l = ['AUD/USD', 'EUR/CHF', 'EUR/GBP', 'NZD/USD', 'USD/SGD']
    instrument = l[0]

    # lmax
    log_file_name = instrument.replace('/', '_')
    #Duka
    log_file_name = 'AUDUSD_UTC_1 Min_Bid_2017.07.30_2017.08.05'

    lmax_log_path = 'C:\\Users\\jquan\\Documents\\Trading Data\\5-Dukascopy\\'

    queues = {'TRADE': queue.Queue(), 'MARKET': queue.Queue()}

    sim_trade_wrapper = SimTradeWrapper(instrument)

    trader = TradeHandler(fix_wrapper=sim_trade_wrapper, queues=queues, t_time='20170730')

    sim_market_wrapper = SimMarketWrapper(trader, instrument, sim_trade_wrapper)

    data_feeder = DataFeeder(sim_market_wrapper, instrument, lmax_log_path, log_file_name)

    data_feeder.data_process_duka()
