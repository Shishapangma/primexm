import pandas as pd
import src.production.settings as SET
import src.production.config.path as PATH
from src.production.event import HistoriyDataEvent

import queue


class HistData:
    def __init__(self, event_queue):

        self.path = PATH.HIS_DATA
        self.event_queue = event_queue

    def get_hist_data(self):

        for instrument in SET.INSTRUMENT:

            instrument = instrument.replace('/', '_')
            data_lmax = pd.read_csv(self.path + instrument + '.csv', header=0)
            data_lmax = data_lmax[data_lmax['event'] == 'Dummy_Tick']
            data_lmax['time_index'] = data_lmax['date'].astype(str) + '-' + data_lmax['time']
            data_lmax.index = pd.to_datetime(data_lmax['time_index'].str[:-1], format='%Y%m%d-%H:%M:%S.%f')
            data_lmax['bid'] = data_lmax['bid'].astype(float)
            data_lmax['ask'] = data_lmax['ask'].astype(float)
            data_lmax = data_lmax.resample('1min').last().fillna(method='pad').shift(1)
            data_lmax['ret'] = data_lmax['bid'].pct_change()

            try:
                last = data_lmax[-1000:]
            # less than 1000
            except IndexError:
                last = data_lmax

            initial_event = HistoriyDataEvent(instrument.replace('_', '/'), last)
            self.event_queue.put(initial_event)


if __name__ == '__main__':
    queues = {'TRADE': queue.Queue(), 'MARKET': queue.Queue(), 'DUMMY_MARKET': queue.Queue()}
    h = HistData(queues['TRADE'])
    h.get_hist_data()