import pandas as pd
import src.production.config.path as PATH

from src.production.fx_logger import logger


class TickCrawler:
    def __init__(self, instrument, database):

        self.instrument = instrument
        self.database = database

        self.data_columns = ['date',
                             'time',
                             'instrument',
                             'event',
                             'bid',
                             'ask',
                             'bid vol',
                             'ask vol']
        self.records_local = []
        self.records_database = []

        import platform

        if platform.system() == 'Windows':
            self.path = PATH.TICK_WIN
        else:
            self.path = PATH.TICK_POSIX

        # timer for frequent detect queue length
        # self.timer = Timer(120, self.detect_queue_stat)
        # self.timer.start()

    def update_tick(self, event):
        """
        tick receive event handler
        :param bid:
        :param ask:
        :param tick_time:
        :return:
        """
        self.records_local.append((event.date,
                                   event.time,
                                   self.instrument,
                                   'Dummy_Tick',
                                   event.bid,
                                   event.ask,
                                   event.bid_vol,
                                   event.ask_vol))

        if len(self.records_local) > 1000:
            self.save_to_local()

    def save_to_local(self):
        """
        save data in configured interval
        :return:
        """
        try:
            instrument = self.instrument
            instrument = instrument.replace('/', '_')
            file_name = self.path + instrument + '.csv'
            data_store = pd.DataFrame(self.records_local, columns=self.data_columns)
            data_store.to_csv(file_name, index=False, header=True, mode='a')
            self.records_local.clear()
            # logger.info('[Tick Achieve] %s', instrument)
        except BaseException as e:
            logger.info('[Tick Output Exception] %s' %(e))

    def save_to_database(self):
        self.database.insert_tick(self.instrument, self.records_database)
        self.records_database.clear()