from queue import Empty
from threading import Thread
from src.database.mongo_wrapper import Mongo_Wrapper

from src.database.Tick.TickCrawler import TickCrawler
import src.production.settings as SET
from src.production.config.primexm_config import DEBUG

#TODO: optimize tick store

class DummyMarketStreamHandler(Thread):

    def __init__(self, queue, database=None):
        Thread.__init__(self)

        self.market_queue = queue

        if database:
            if DEBUG:
                    database = database + '_Demo'

            self.mongodb = Mongo_Wrapper(database)
        else:
            self.mongodb = None
        self.crawlers = {}

        for instrument in SET.DUMMY_INSTRUMENT:
            self.crawlers[instrument] = TickCrawler(instrument, self.mongodb)

    def run(self):

        while 1:
            try:
                event = self.market_queue.get(block=True)
                self.crawlers[event.instrument].update_tick(event)
            except Empty:
                pass