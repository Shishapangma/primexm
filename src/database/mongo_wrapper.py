import pymongo
import os
import datetime
import pandas as pd
import src.production.settings as SET


class Mongo_Wrapper:

    def __init__(self, database_name):
        # setup connection
        self.client = pymongo.MongoClient(SET.MONOGO_URI)
        # connect to database
        self.database = self.client[database_name]

    def insert_tick(self, instrument, records):
        """
        :param instrument: represent mongodb collection
        :param records:
        :return:
        """
        self.database[instrument].insert_many(records)

    def find_tick(self, instrument, start, end):
        """
        :param instrument: represent collection
        :param start: instance of datetime.datetime
        :param end: instance of datetime.datetime
        :return: instance of Pandas.DataFrame
        """
        docs = self.database[instrument].find({'date': {'$gte': start, '$lte': end}})
        ticks = pd.DataFrame(list(docs))
        ticks.index = pd.to_datetime(ticks['date'], format='%Y-%m-%d %H:%M:%S.%f')
        del ticks['_id']
        return ticks

    def close(self):
        self.client.close()


#
# client = pymongo.MongoClient(SET.MONOGO_URI)
# lmax_tick_database = client['LMAX_Tick']
#
# log_path = 'C:\\Users\\jquan\\Documents\\Trading Data\\7-LMAX Tick\\'
# folders = os.listdir(log_path)
#
# for folder in folders:
#     print(folder)
#     files = os.listdir(log_path + folder)
#     for log_file in files:
#         instrument = log_file[:-4].replace('_', '/')
#         tick = pd.read_csv(log_path + folder + "\\" + log_file, header=0)
#         tick = tick[tick['event'] == 'Dummy_Tick']
#         tick['date'] = tick['date'].astype(str)
#         tick['bid'] = tick['bid'].astype(float)
#         tick['ask'] = tick['ask'].astype(float)
#         tick['bid vol'] = tick['bid vol'].astype(float)
#         tick['ask vol'] = tick['ask vol'].astype(float)
#
#         tick_list = []
#         for i in range(0, len(tick)):
#             date_str = tick['date'].iloc[i] + ' ' + tick['time'].iloc[i]
#
#             tick_list.append({'date': datetime.datetime.strptime(date_str, '%Y%m%d %H:%M:%S.%f'),
#                               'price': [tick['bid'].iloc[i], tick['ask'].iloc[i]],
#                               'vol': [tick['bid vol'].iloc[i], tick['ask vol'].iloc[i]]})
#
#             if len(tick_list) >= 5000:
#                 lmax_tick_database[instrument].insert_many(tick_list)
#                 tick_list.clear()
#                 print(folder, instrument, 'DB insert complete', i)
#
#             elif i == len(tick) - 1:
#                 lmax_tick_database[instrument].insert_many(tick_list)
#                 tick_list.clear()
#                 print(folder, instrument, 'DB insert last batch complete', i, 'len of tick', len(tick))
#
# client.close()
#
# client = pymongo.MongoClient('mongodb://shishapangmatechnologies:Shishapangma2017@cluster0-shard-00-00-a9fqu.mongodb.net:27017,cluster0-shard-00-01-a9fqu.mongodb.net:27017,cluster0-shard-00-02-a9fqu.mongodb.net:27017/local?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin')
#
#
# # query
# #format year, month, day, hour, min, sec, microsec
#
# start = datetime.datetime(2017, 8, 22, 1, 0, 0, 0)
# end = datetime.datetime(2017, 8, 22, 7, 0, 0, 0)
#
# docs = client['LMAX_Tick']['AUD/USD'].find({'date': {'$gte': start, '$lte': end}})
# tick = pd.DataFrame(list(docs))
# del tick['_id']
# tick.index = pd.to_datetime(tick['date'], format='%Y-%m-%d %H:%M:%S.%f')
# tick['bid'] = tick['price'][0]



