from lib.broker_api_lib.oanda_rest import oandapy

class OandaExecuteHandler(object):
    def __init__(self, domain, access_token, account_id):
        self.domain = domain
        self.access_token = access_token
        self.account_id = account_id
        self.oanda = oandapy.API(environment=self.domain, access_token=self.access_token)

    def order_execute(self, event):
        # market order
        if event.order_type == 'market':
            response = self.oanda.create_order(self.account_id,
                                          instrument=event.instrument,
                                          units=event.units,
                                          side=event.side,
                                          type=event.order_type)
            return response

        # limit order
        elif event.order_type == 'limit':
            response = self.oanda.create_order(self.account_id,
                                               instrument=event.instrument,
                                               units=event.units,
                                               side=event.side,
                                               type=event.order_type,
                                               price=event.price,
                                               expiry=event.trade_expire)
            return response

    def order_close(self, instrument):
        exit_orders = self.oanda.get_orders(account_id=self.account_id, instrument=instrument)
        orders = exit_orders['orders']
        for order in orders:
            order_id = order['id']
            respose = self.oanda.close_order(account_id=self.account_id, order_id=order_id)
            # print(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'), ' Order Close ', respose)
