import requests
import json
import numpy as np
from datetime import datetime
from src.production.event import TickEvent, OrderFillingEvent, OrderFilledEvent, OrderCreateEvent, OrderExpiredEvent, OrderClientCancelEvent


class OandaStreamingOanda(object):
    def __init__(self, domain, access_token, account_id, instruments, event):
        self.domain = domain
        self.access_token = access_token
        self.account_id = account_id
        self.instruments = instruments
        self.event = event

    def connect_to_price_stream(self):
        instruments = ''
        for i in range(0, len(self.instruments)):
            if i < len(self.instruments) - 1:
                instruments = instruments + self.instruments[i]
                instruments += ','
            # last instrument in list
            else:
                instruments = instruments + self.instruments[i]

        try:
            s = requests.Session()
            url = "https://" + self.domain + "/v1/prices"
            headers = {'Authorization': 'Bearer ' + self.access_token}
            params = {'instruments': instruments, 'accountId': self.account_id}
            req = requests.Request('GET', url, headers=headers, params=params)
            pre = req.prepare()
            resp = s.send(pre, stream=True, verify=False)
            return resp

        except Exception as e:
            s.close()
            print("Caught exception when connecting to stream\n" + str(e))

    def tick_stream_to_queue(self):
        response = self.connect_to_price_stream()
        if response.status_code != 200:
            return
        for line in response.iter_lines(1):
            if line:
                decoded_line = line.decode('utf-8')
                try:
                    msg = json.loads(decoded_line)
                except Exception as e:
                    print("Caught exception when converting message into json\n" + str(e))
                    return
                if 'instrument' in msg or 'tick' in msg:
                    instrument = msg['tick']['instrument']
                    time = msg['tick']['time']
                    bid = msg['tick']['bid']
                    ask = msg['tick']['ask']
                    tick_event = TickEvent(instrument, time, bid, ask)
                    self.event.put(tick_event)

    def connect_to_events_stream(self):
        try:
            s = requests.Session()
            url = "https://" + self.domain + "/v1/events"
            headers = {'Authorization': 'Bearer ' + self.access_token}
            params = {'accountId': self.account_id}
            req = requests.Request('GET', url, headers=headers, params=params)
            pre = req.prepare()
            resp = s.send(pre, stream=True, verify=False)
            return resp

        except Exception as e:
            fo = open('../log/fx_log.txt', 'a')
            errors = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f') + 'OandaError at Get Weight:' + str(
                e) + '\n'
            print(errors)
            fo.write(errors)
            fo.close()
            raise e

    def event_stream_to_queue(self):
        response = self.connect_to_events_stream()

        if response.status_code != 200:
            return

        for line in response.iter_lines(1):

            if line:
                decoded_line = line.decode('utf-8')
                try:
                    msg = json.loads(decoded_line)

                    if 'transaction' in msg:
                        print(msg)
                        tran = msg['transaction']
                        if 'type' in tran:
                            # order filling
                            if tran['type'] == 'ORDER_FILLED':
                                instrument = tran['instrument']
                                time = tran['time']
                                if tran['side'] == 'buy':
                                    units = tran['units']
                                else:
                                    units = -1 * tran['units']
                                price = tran['price']
                                pl = tran['pl']
                                orderId = tran['orderId']
                                if 'tradeReduced' in tran:
                                    tranId = tran['tradeReduced']['id']
                                else:
                                    tranId = np.nan
                                order_filling_event = OrderFillingEvent(time, instrument, units, price, pl, tranId, orderId)
                                self.event.put(order_filling_event)
                            # order cancel
                            elif tran['type'] == 'ORDER_CANCEL':
                                if 'reason' in tran:
                                    # order filled
                                    if tran['reason'] == 'ORDER_FILLED':
                                        time = tran['time']
                                        filled_event = OrderFilledEvent(time, tran['orderId'])
                                        self.event.put(filled_event)
                                    # order cancelled by user
                                    elif tran['reason'] == 'CLIENT_REQUEST':
                                        time = tran['time']
                                        client_cancel_event = OrderClientCancelEvent(time, tran['orderId'])
                                        self.event.put(client_cancel_event)
                                    # order expired
                                    elif tran['reason'] == 'TIME_IN_FORCE_EXPIRED':
                                        time = tran['time']
                                        expired_event = OrderExpiredEvent(time, tran['orderId'])
                                        self.event.put(expired_event)
                            # limit order created
                            elif tran['type'] == 'LIMIT_ORDER_CREATE':
                                time = tran['time']
                                instrument = tran['instrument']
                                if tran['side'] == 'buy':
                                    units = tran['units']
                                else:
                                    units = -1 * tran['units']
                                price = tran['price']
                                side = tran['side']
                                orderId = tran['id']
                                order_create_event = OrderCreateEvent(time, instrument, units, price, side, orderId)
                                self.event.put(order_create_event)
                except Exception as e:
                    fo = open('../log/fx_log.txt', 'a')
                    errors = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f') + 'OandaError at Event Stream:' + str(
                        e) + '\n'
                    print(errors)
                    fo.write(errors)
                    fo.close()
                    raise e
