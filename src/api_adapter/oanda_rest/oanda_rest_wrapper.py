import time
import datetime
import pandas as pd

import src.production.config.oanda_config as OANDA_SET
import src.production.settings as SET
from src.production.event import *
from lib.broker_api_lib.oanda_rest import oandapy


class OandaRestWrapper(object):
    """
    Datahouse represents the instance to communicate with different Data Source
    a infinite loop to keep GET tick information
    """

    def __init__(self, queues):
        # set of trading instruments
        self.instruments = SET.INSTRUMENT

        # use for order relative events
        self.trade_queue = queues['TRADE']

        # using for positive data fetch
        self.oanda = oandapy.API(OANDA_SET.API_DOMAIN, OANDA_SET.OANDA_ACCESS_TOKEN)

    def get_history_prices(self):
        """
        Retrieve 400 observation every time Trader starting
        Data fetching is the time consuming process, in order to utilize existing multi thread capability, we construct
        an event named CandleEvent to wrap and deliver the historical data to the other section
        :return: None
        """
        # fetch data and fill event
        for instrument in SET.INSTRUMENT:
            d2 = datetime.datetime.utcnow()
            d1 = d2 - datetime.timedelta(days=1)
            instrument = instrument.replace('/', '_')
            response = self.oanda.get_history(instrument=instrument, start=d1.strftime('%Y-%m-%dT%H:%M:%SZ'),
                                              end=d2.strftime('%Y-%m-%dT%H:%M:%SZ'), granularity='M1',
                                              alignmentTimezone='Etc/GMT+0')
            candles = pd.DataFrame(columns=['time', 'closeBid', 'closeAsk', 'volume', 'ret'])
            for j in range(0, len(response['candles'])):
                candle = response['candles'][j]
                candle_time = candle['time']
                candle_closeBid = candle['closeBid']
                candle_closeAsk = candle['closeAsk']
                candle_volume = candle['volume']
                try:
                    candle_ret = (candle_closeBid - candles.loc[j - 1]['closeBid']) / candles.loc[j - 1]['closeBid']
                except KeyError:
                    candle_ret = 0
                candles.loc[j] = [candle_time, candle_closeBid, candle_closeAsk, candle_volume, candle_ret]
            candles.index = candles['time']
            candles = candles.drop('time', 1)
            initial_event = HistoriyDataEvent(instrument.replace('_', '/'), candles)

            self.trade_queue.put(initial_event)
        self.oanda = None


def get_history_price(instrument, start, end):
    """
    check_time format meets '%Y-%m-%dT%H:%M:%SZ'
    :param instrument: 
    :param time: 
    :return: 
    """
    oanda = oandapy.API(OANDA_SET.API_DOMAIN, OANDA_SET.OANDA_ACCESS_TOKEN)
    instrument = instrument.replace('/', '_')
    response = oanda.get_history(instrument=instrument, start=start, end=end, granularity='M1',
                                 alignmentTimezone='Etc/GMT+0')
    candles = pd.DataFrame(columns=['time', 'close'])

    for j in range(0, len(response['candles'])):
        candle = response['candles'][j]
        candle_time = candle['time']
        candle_close = candle['closeBid']
        candles.loc[j] = [candle_time, candle_close]

    candles.index = pd.to_datetime(candles['time'], format='%Y-%m-%dT%H:%M:%S.000000Z')
    candles = candles.drop('time', 1)
    return candles
