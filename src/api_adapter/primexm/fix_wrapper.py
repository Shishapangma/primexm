from threading import Thread

from lib.broker_api_lib.fix_primexm.pyfix.connection import ConnectionState
from lib.broker_api_lib.fix_primexm.pyfix.client_connection import FIXClient
from lib.broker_api_lib.fix_primexm.pyfix.engine import FIXEngine

import src.production.config.path as PATH


class FixWrapper(Thread):
    def __init__(self, event_queues, connect_host, connect_port, ssl, target_compId, sender_compId, username, password=None):
        Thread.__init__(self)
        #identifier
        self.name = ''

        # event queue
        self.event_queue = None

        # FIX related msg SeqNum
        self.clOrdID = 0
        self.MDReqID = 0

        # Broker dedicated trading info
        self.connect_host = connect_host
        self.connect_port = connect_port
        self.ssl = ssl
        self.target_compId = target_compId
        self.sender_compId = sender_compId
        self.username = username
        self.password = password

        # Thread loop controller
        self.active = True

        # Label of stream stats managing
        self.client_disconnected = False

    def run(self):
        """
        Rewrite Thread interface
        :return: None
        """
        # Create unique FIX Engine

        import platform

        if platform.system() == 'Windows':
            path = PATH.ENGINE_WIN
        else:
            path = PATH.ENGINE_POSIX

        self.engine = FIXEngine(path+self.name)

        # Create a FIX Client using the FIX 4.4 standard
        self.client = FIXClient(self.engine, 'lib.broker_api_lib.fix_primexm.pyfix.FIX44', self.target_compId,
                                            self.sender_compId, self.username, self.password)

        # Register listeners of Sockect stats changing
        self.client.addConnectionListener(self.on_connect, ConnectionState.CONNECTED)
        self.client.addConnectionListener(self.on_disconnect, ConnectionState.DISCONNECTED)

        # FIX Client Start
        self.client.start(self.connect_host, self.connect_port, self.ssl)

        # Thread stats, initial True
        active = True
        while active:
            self.engine.eventManager.waitForEventWithTimeout(10)
            active = self.active

    def stop(self):
        """
        Enable to stop thread manually
        :return:
        """
        if self.client_disconnected is False:
            self.client_disconnected = True
            self.client.stop()
            self.active = False

    def on_connect(self, connectionHandler):
        """
        Handler when FIXClient socket connection establishment completed
        :param connectionHandler:
        :return:
        """
        pass


    def on_disconnect(self, connection):
        """
        Handler when FIXClient socket disconnect
        :param connection:
        :return:
        """
        # self.client.removeConnectionListener(self.on_connect, ConnectionState.CONNECTED)
        # self.client.removeConnectionListener(self.on_disconnect, ConnectionState.DISCONNECTED)
        self.client_disconnected = True
        # self.stop()

    def on_login(self, connectionHandler, msg):
        """
        Handler for FIX Event LOGON Msg
        :param connectionHandler:
        :param msg:
        :return:
        """
        pass

    def logout(self):
        self.client.connection_handler.sendMsg(self.client.protocol.messages.Messages.logout())

