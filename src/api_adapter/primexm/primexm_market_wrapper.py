import src.production.settings as SET
from src.api_adapter.primexm.fix_wrapper import FixWrapper
from lib.broker_api_lib.fix_primexm.pyfix.message import MessageDirection, FIXMessage
from src.production.event import TickEvent
from src.production.fx_logger import logger
from lib.twilio_alarm.twilio_notifications.middleware import TwilioNotificationsMiddleware


class PrimeXmMarketWrapper(FixWrapper):
    def __init__(self, event_queue, dummy_event_queue, connect_host, connect_port, ssl, target_compId, sender_compId, username, password=None):

        FixWrapper.__init__(self, event_queue, connect_host, connect_port, ssl, target_compId, sender_compId, username, password)

        self.event_queue = event_queue
        self.dummy_event_queue = dummy_event_queue

        self.name = 'PrimeXM Market Stream.store'

        self.last_bid = {}
        self.last_ask = {}

        self.MDReqID_Instrument = {}

    def on_connect(self, connectionHandler):
        """
        Register interested FIX Msg handler, prepare for FIX related process after network connection setup
        :param connectionHandler:
        :return:
        """
        logger.info('[Market Streaming Connection Established] %s', connectionHandler.address())
        connectionHandler.addMessageHandler(self.on_login, MessageDirection.INBOUND,
                                            self.client.protocol.msgtype.LOGON)
        connectionHandler.addMessageHandler(self.on_mass_quote_update, MessageDirection.INBOUND,
                                            self.client.protocol.msgtype.MASSQUOTE)

    def on_login(self, connection_handler, msg):
        """
        Handler of FIX LOGON msg
        :param connectionHandler:
        :param msg:
        :return:
        """
        if self.client_disconnected:
            notifier = TwilioNotificationsMiddleware()
            notifier.process_exception(None, '[Market Streaming Logon Back] %s' % (connection_handler.address(),))
            logger.info('[Market Streaming Logon Back] %s', connection_handler.address())
            # Set disconnected label to False after Login done
            self.client_disconnected = False

        else:
            logger.info('[Market Streaming Client Logon] %s', connection_handler.address())

        self.request_market_data()

    def request_market_data(self):
        """
        Similar as self.request_market_data except instruments different
        :return: 
        """
        for instrument in SET.DUMMY_INSTRUMENT:
            self.last_ask[instrument] = 0
            self.last_bid[instrument] = 0

            self.MDReqID += 1
            self.MDReqID_Instrument[str(self.MDReqID)] = instrument

            codec = self.client.connection_handler.codec
            msg = FIXMessage(codec.protocol.msgtype.MARKETDATAREQUEST)
            msg.setField(codec.protocol.fixtags.MDReqID, str(self.MDReqID))
            msg.setField(codec.protocol.fixtags.SubscriptionRequestType, '1')
            msg.setField(codec.protocol.fixtags.MarketDepth, 1)
            msg.setField(codec.protocol.fixtags.NoRelatedSym, 1)
            # prod
            # msg.setField(codec.protocol.fixtags.Symbol, instrument.replace('/', ''))
            # demo
            msg.setField(codec.protocol.fixtags.Symbol, instrument)
            self.client.connection_handler.sendMsg(msg)

    def quote_ack(self, quote_id):
        codec = self.client.connection_handler.codec
        msg = FIXMessage(codec.protocol.msgtype.MASSQUOTEACKNOWLEDGEMENT)
        msg.setField(codec.protocol.fixtags.QuoteID, quote_id)
        self.client.connection_handler.sendMsg(msg)

    def on_mass_quote_update(self, connectionHandler, msg):
        """
        Handler of FIX MARKETDATASNAPSHOTFULLREFRESH
        :param connectionHandler:
        :param msg:
        :return:
        """

        codec = connectionHandler.codec

        t = msg.getField(codec.protocol.fixtags.SendingTime).split('-')
        t_date = t[0]
        t_time = t[1]

        _, gr = msg.getRepeatingGroup(codec.protocol.fixtags.NoQuoteSets)

        for group in gr:

            instrument = self.MDReqID_Instrument[group.tags[codec.protocol.fixtags.QuoteSetID]]

            try:
                bid = group.tags[codec.protocol.fixtags.BidSpotRate]
                self.last_bid[instrument] = bid
            except KeyError:
                bid = self.last_bid[instrument]
            try:
                ask = group.tags[codec.protocol.fixtags.OfferSpotRate]
                self.last_ask[instrument] = ask
            except KeyError:
                ask = self.last_ask[instrument]

            # logger.info('%s %s %s %s', t_time, instrument, bid, ask)

            tick_event = TickEvent(instrument=instrument, date=t_date, time=t_time, bid=float(bid), ask=float(ask),
                                   bid_vol=0.0, ask_vol=0.0)

            if instrument in SET.INSTRUMENT:
                self.event_queue.put(tick_event)

            self.dummy_event_queue.put(tick_event)

            # quote_id = int(msg.getField(codec.protocol.fixtags.QuoteID))
            # self.quote_ack(quote_id)

            try:
                quote_id = int(msg.getField(codec.protocol.fixtags.QuoteID))
                self.quote_ack(quote_id)
            except KeyError:
                pass
