from datetime import datetime
import pandas as pd
from src.api_adapter.primexm.fix_wrapper import FixWrapper
from lib.broker_api_lib.fix_primexm.pyfix.message import MessageDirection, FIXMessage
from src.production.fx_logger import logger
from src.production.event import *
from lib.twilio_alarm.twilio_notifications.middleware import TwilioNotificationsMiddleware

class PrimeXmTradeWrapper(FixWrapper):

    def __init__(self, event_queue, connect_host, connect_port, ssl, target_compId, sender_compId, username, password=None):

        FixWrapper.__init__(self, event_queue, connect_host, connect_port, ssl, target_compId, sender_compId, username, password)

        self.event_queue = event_queue

        self.name = 'PrimeXM Trade Stream.store'

        self.MDReqID_Instrument = {}

    def on_connect(self, connectionHandler):
        """
        Register interested FIX Msg handler, prepare for FIX related process after network connection setup
        :param connectionHandler:
        :return:
        """
        logger.info('[Trade Client Connection Established] %s', connectionHandler.address())
        connectionHandler.addMessageHandler(self.on_login, MessageDirection.INBOUND,
                                            self.client.protocol.msgtype.LOGON)
        connectionHandler.addMessageHandler(self.on_execute_report, MessageDirection.INBOUND,
                                            self.client.protocol.msgtype.EXECUTIONREPORT)

    def on_disconnect(self, connection):
        """
        do something when disconnected
        :param connection:
        :return:
        """
        FixWrapper.on_disconnect(self, connection)

    def set_handler(self, trader_handler):
        """

        :param trader_handler:
        :return:
        """
        self.trader_hanlder = trader_handler

    def on_login(self, connection_handler, msg):
        """
        Handler of FIX LOGON msg
        :param connectionHandler:
        :param msg:
        :return:
        """
        # For message purpose only
        if self.client_disconnected:
            notifier = TwilioNotificationsMiddleware()
            notifier.process_exception(None, '[Trade Client Logon Back] %s' % (connection_handler.address(),))
            logger.info('[Trade Client Logon Back] %s', connection_handler.address())
            # Set disconnected label to False after Login done
            self.client_disconnected = False
            # restore known order stats
            restore_event = Reconnection()
            self.event_queue.put(restore_event)
        else:
            logger.info('[Trade Client Logon] %s', connection_handler.address())

    def new_order_single(self, order):
        """
        Order Submit
        :param order:
        :return:
        """
        order.units = round(order.units / 1000, 0) * 1000

        if self.min_qty(order):
            self.clOrdID += 1
            clOrdID = int(datetime.utcnow().timestamp()) + self.clOrdID
            codec = self.client.connection_handler.codec
            msg = FIXMessage(codec.protocol.msgtype.NEWORDERSINGLE)

            msg.setField(codec.protocol.fixtags.ClOrdID, str(clOrdID))

            msg.setField(codec.protocol.fixtags.Account, 'shisha')

            # prod
            # msg.setField(codec.protocol.fixtags.Symbol, order.instrument.replace('/', ''))
            # demo
            msg.setField(codec.protocol.fixtags.Symbol, order.instrument)

            msg.setField(codec.protocol.fixtags.Side, order.side)

            msg.setField(codec.protocol.fixtags.OrderQty, order.units)

            if order.type == 'LIMIT_ORDER':
                msg.setField(codec.protocol.fixtags.OrdType, '2')
                msg.setField(codec.protocol.fixtags.Price, order.price)
            elif order.type == 'MARKET_ORDER':
                msg.setField(codec.protocol.fixtags.OrdType, '1')

            transact_time = datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f')[:-3]
            msg.setField(codec.protocol.fixtags.TransactTime, transact_time)

            # add tag 10000 ttl to 1s to increase market order fill rate
            msg.setField(codec.protocol.fixtags.ttl, 1500)

            self.client.connection_handler.sendMsg(msg)
            # logger.info('[New Order Request] [%s] Type %s ' % (order.instrument, order.type, ))

            return clOrdID
        else:
            # reject_event = OrderCreateRejectEvent(order.instrument)
            # self.event_queue.put(reject_event)
            logger.info('[Qty less than MinQty] [%s] %s' % (order.instrument, int(order.units)))
            return 0

    def min_qty(self, order):

        return True if abs(order.units) >= 10000 else False

    def order_cancel_request(self, order):
        """
        market order has no order cancel
        :param order:
        :return:
        """
        pass
        # codec = self.client.connection_handler.codec
        # msg = FIXMessage(codec.protocol.msgtype.ORDERCANCELREQUEST)
        # msg.setField(codec.protocol.fixtags.ClOrdID, order.clOrderId)
        # msg.setField(codec.protocol.fixtags.Account, 'shisha')
        #
        # transact_time = datetime.utcnow().strftime('%Y%m%d-%H:%M:%S.%f')[:-3]
        # msg.setField(codec.protocol.fixtags.TransactTime, transact_time)
        #
        # self.client.connection_handler.sendMsg(msg)

    def on_execute_report(self, connection_handler, msg):
        """
        Handle of FIXExecution Report EXECUTIONREPORT
        :param connectionHandler:
        :param msg:
        :return:
        """
        try:
            codec = connection_handler.codec

            # tag 150
            execType = msg.getField(codec.protocol.fixtags.ExecType)

            # tag 39
            order_status = msg.getField(codec.protocol.fixtags.OrdStatus)

            # tag 55
            instrument = msg.getField(codec.protocol.fixtags.Symbol)

            # prod
            # instrument = instrument[:3] + '/' + instrument[3:]

            # New Order
            if execType == '0':
                try:
                    clOrderId = msg.getField(codec.protocol.fixtags.ClOrdID)
                    orderId = msg.getField(codec.protocol.fixtags.OrderID)
                    t = msg.getField(codec.protocol.fixtags.TransactTime)
                    date = pd.to_datetime(t).strftime('%Y%m%d')
                    t_time = pd.to_datetime(t).strftime('%H:%M:%S.%f')[:-3]
                    side = msg.getField(codec.protocol.fixtags.Side)
                    units = int(float(msg.getField(codec.protocol.fixtags.OrderQty)))
                    units = units * -1 if side == '2' else units
                    price = ''
                    order_type = msg.getField(codec.protocol.fixtags.OrdType)
                    if order_type == '1':
                        price = 0.0
                        order_type = OrderType.MARKET_ORDER
                    elif order_type == '2':
                        price = float(msg.getField(codec.protocol.fixtags.Price))
                        order_type = OrderType.LIMIT_ORDER

                    event = OrderCreateEvent(date, t_time, instrument, units, price, side, int(clOrderId),
                                             orderId,
                                             order_type)
                    self.event_queue.put(event)
                    # logger.info('[New Order Confirmed] [%s] Type %s ' % (instrument, str(order_type),))
                except Exception as e:
                    logger.error('[New Order Execution Report] %s' % e)

            # Trade
            elif execType == 'F':
                try:
                    orderId = msg.getField(codec.protocol.fixtags.OrderID)
                    side = msg.getField(codec.protocol.fixtags.Side)
                    order_units = int(float(msg.getField(codec.protocol.fixtags.OrderQty)))
                    total_filled_units = int(float(msg.getField(codec.protocol.fixtags.CumQty)))
                    this_filled_units = int(float(msg.getField(codec.protocol.fixtags.LastQty)))
                    total_leaves_units = int(float(msg.getField(codec.protocol.fixtags.LeavesQty)))

                    if side == '2':
                        this_filled_units *= -1
                        total_filled_units *= -1
                        order_units *= -1
                        total_leaves_units *= -1

                    # last fill price
                    price = float(msg.getField(codec.protocol.fixtags.LastPx))
                    t = msg.getField(codec.protocol.fixtags.TransactTime)
                    date = pd.to_datetime(t).strftime('%Y%m%d')
                    t_time = pd.to_datetime(t).strftime('%H:%M:%S.%f')[:-3]
                    side = msg.getField(codec.protocol.fixtags.Side)

                    order_type = msg.getField(codec.protocol.fixtags.OrdType)
                    order_type = OrderType.MARKET_ORDER if order_type == '1' else OrderType.LIMIT_ORDER

                    # full filled
                    if order_status == '2':
                        # logger.info('[Order Filled] [%s] Type %s ' % (instrument, str(order_type),))
                        event = OrderFilledEvent(instrument, date, t_time, this_filled_units, total_filled_units,
                                                 price, orderId, order_type)
                        self.event_queue.put(event)

                    # partially fill
                    elif order_status == '1':
                        # logger.info('[Order Filling] [%s] Type %s ' % (instrument, str(order_type),))
                        event = OrderFillingEvent(instrument, date, t_time, this_filled_units,total_leaves_units,
                                                  price, orderId, order_type)
                        self.event_queue.put(event)
                except Exception as e:
                    logger.error('[Order Fill Execution Report] %s' % e)

            # Cancelled
            elif execType == '4':
                try:
                    # tag 60
                    t = msg.getField(codec.protocol.fixtags.TransactTime)
                    date = pd.to_datetime(t).strftime('%Y%m%d')
                    t_time = pd.to_datetime(t).strftime('%H:%M:%S.%f')[:-3]

                    # tag 54
                    side = msg.getField(codec.protocol.fixtags.Side)

                    # tag 38
                    target_units = int(float(msg.getField(codec.protocol.fixtags.OrderQty)))

                    # tag 14
                    total_filled_units = int(float(msg.getField(codec.protocol.fixtags.CumQty)))

                    # tag 40
                    order_type = msg.getField(codec.protocol.fixtags.OrdType)
                    order_type = OrderType.MARKET_ORDER if order_type == '1' else OrderType.LIMIT_ORDER

                    cancel_event = OrderCancelEvent(instrument, date, t_time, order_type, (target_units-total_filled_units))
                    self.event_queue.put(cancel_event)
                    # logger.info('[Order Cancel Confirmed] [%s] Type %s' % (instrument, str(order_type),))
                except Exception as e:
                    logger.error('[Order Cancel Execution Report] %s' % e)

                    # for test purpose
                    # UTA can't cover this section sufficiently
                    from lib.twilio_alarm.twilio_notifications.middleware import TwilioNotificationsMiddleware
                    notifier = TwilioNotificationsMiddleware()
                    notifier.process_exception(None, '[Order Cancel Error in Trade_Wrapper] %s' % (e))

            # Create reject
            elif execType == '8':
                reject_event = OrderCreateRejectEvent(instrument)
                self.event_queue.put(reject_event)
                logger.info('[Order Create Rejected] [%s] %s' % (instrument, msg))

            # Order Status
            elif execType == 'I':

                if order_status == '0':
                    clOrderId = msg.getField(codec.protocol.fixtags.ClOrdID)
                    orderId = msg.getField(codec.protocol.fixtags.OrderID)
                    t = msg.getField(codec.protocol.fixtags.TransactTime)
                    date = pd.to_datetime(t).strftime('%Y%m%d')
                    t_time = pd.to_datetime(t).strftime('%H:%M:%S.%f')[:-3]
                    side = msg.getField(codec.protocol.fixtags.Side)
                    units = int(float(msg.getField(codec.protocol.fixtags.OrderQty)))
                    units = units * -1 if side == '2' else units
                    price = ''
                    order_type = msg.getField(codec.protocol.fixtags.OrdType)
                    if order_type == '1':
                        price = 0.0
                        order_type = OrderType.MARKET_ORDER
                    elif order_type == '2':
                        price = msg.getField(codec.protocol.fixtags.Price)
                        order_type = OrderType.LIMIT_ORDER

                    event = OrderCreateEvent(date, t_time, instrument, units, float(price), side, int(clOrderId),
                                             orderId,
                                             order_type)
                    self.event_queue.put(event)
                    logger.info('[Order Status Response][New Order Confirmed] [%s] Type %s -%s' % (
                        instrument, str(order_type), msg))

                # Partially filled
                elif order_status == '1':
                    orderId = msg.getField(codec.protocol.fixtags.OrderID)
                    side = msg.getField(codec.protocol.fixtags.Side)
                    order_units = int(float(msg.getField(codec.protocol.fixtags.OrderQty)))
                    total_filled_units = int(float(msg.getField(codec.protocol.fixtags.CumQty)))
                    this_filled_units = int(float(msg.getField(codec.protocol.fixtags.LastQty)))
                    total_leaves_units = int(float(msg.getField(codec.protocol.fixtags.LeavesQty)))

                    if side == '2':
                        this_filled_units *= -1
                        total_filled_units *= -1
                        order_units *= -1
                        total_leaves_units *= -1

                    price = float(msg.getField(codec.protocol.fixtags.LastPx))
                    t = msg.getField(codec.protocol.fixtags.TransactTime)
                    date = pd.to_datetime(t).strftime('%Y%m%d')
                    t_time = pd.to_datetime(t).strftime('%H:%M:%S.%f')[:-3]
                    order_type = msg.getField(codec.protocol.fixtags.OrdType)
                    order_type = OrderType.MARKET_ORDER if order_type == '1' else OrderType.LIMIT_ORDER
                    event = OrderFillingEvent(instrument, date, t_time, this_filled_units, total_leaves_units,
                                              price, orderId, order_type)
                    self.event_queue.put(event)
                    logger.info(
                        '[Order Status Response][Order Filling] [%s] Type %s %s' % (instrument, str(order_type), msg))

                # Filled
                elif order_status == '2':
                    orderId = msg.getField(codec.protocol.fixtags.OrderID)
                    side = msg.getField(codec.protocol.fixtags.Side)
                    order_units = int(float(msg.getField(codec.protocol.fixtags.OrderQty)))
                    total_filled_units = int(float(msg.getField(codec.protocol.fixtags.CumQty)))
                    this_filled_units = int(float(msg.getField(codec.protocol.fixtags.LastQty)))
                    total_leaves_units = int(float(msg.getField(codec.protocol.fixtags.LeavesQty)))

                    if side == '2':
                        this_filled_units *= -1
                        total_filled_units *= -1
                        order_units *= -1
                        total_leaves_units *= -1

                    price = float(msg.getField(codec.protocol.fixtags.LastPx))
                    t = msg.getField(codec.protocol.fixtags.TransactTime)
                    date = pd.to_datetime(t).strftime('%Y%m%d')
                    t_time = pd.to_datetime(t).strftime('%H:%M:%S.%f')[:-3]
                    order_type = msg.getField(codec.protocol.fixtags.OrdType)
                    order_type = OrderType.MARKET_ORDER if order_type == '1' else OrderType.LIMIT_ORDER
                    event = OrderFilledEvent(instrument, date, t_time, this_filled_units, total_filled_units,
                                             price, orderId, order_type)
                    self.event_queue.put(event)
                    logger.info(
                        '[Order Status Response][Order Filled] [%s] Type %s %s' % (instrument, str(order_type), msg))

                # Canceled
                elif order_status == '4':
                    clOrderId = msg.getField(codec.protocol.fixtags.OrigClOrdID)
                    orderID = msg.getField(codec.protocol.fixtags.OrderID)
                    t = msg.getField(codec.protocol.fixtags.TransactTime)
                    date = pd.to_datetime(t).strftime('%Y%m%d')
                    t_time = pd.to_datetime(t).strftime('%H:%M:%S.%f')[:-3]
                    side = msg.getField(codec.protocol.fixtags.Side)

                    order_type = msg.getField(codec.protocol.fixtags.OrdType)
                    order_type = OrderType.MARKET_ORDER if order_type == '1' else OrderType.LIMIT_ORDER

                    cancel_event = OrderCancelEvent(instrument, date, t_time, int(clOrderId), order_type)
                    self.event_queue.put(cancel_event)
                    logger.info(
                        '[Order Status Response][Order Cancelled] [%s] Type %s %s' % (instrument, str(order_type), msg))

                # Status Request Reject due to unknown order
                elif order_status == '8':
                    reject_event = OrderCreateRejectEvent(instrument)
                    self.event_queue.put(reject_event)
                    logger.info(
                        '[Order Status Response][Unknown Order] [%s] %s' % (instrument, msg))

        except Exception as e:
            logger.error('[Exception Execution Report] %s' % e)

    def order_status_request(self, order):
        """
        Message sent by the client to request the state of an order. Note that orders that were rejected for whatever reason will be reported as an unknown order
        :param order:
        :return:
        """
        # codec = self.client.connection_handler.codec
        # msg = FIXMessage(codec.protocol.msgtype.ORDERSTATUSREQUEST)
        # msg.setField(codec.protocol.fixtags.ClOrdID, str(order.clOrderId))
        # self.client.connection_handler.sendMsg(msg)
        pass