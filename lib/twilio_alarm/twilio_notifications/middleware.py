from __future__ import unicode_literals

from twilio.rest import Client
from django.core.exceptions import MiddlewareNotUsed
import logging
import lib.twilio_alarm.config.administrators as SET
from datetime import datetime

logger = logging.getLogger(__name__)

NOT_CONFIGURED_MESSAGE = """Cannot initialize Twilio notification
middleware. Required enviroment variables TWILIO_ACCOUNT_SID, or
TWILIO_AUTH_TOKEN or TWILIO_NUMBER missing"""


def load_twilio_config():

    twilio_account_sid = SET.twilio_account_sid
    twilio_auth_token = SET.twilio_auth_token
    twilio_number = SET.twilio_number

    if not all([twilio_account_sid, twilio_auth_token, twilio_number]):
        logger.error(NOT_CONFIGURED_MESSAGE)
        raise MiddlewareNotUsed

    return (twilio_number, twilio_account_sid, twilio_auth_token)


class MessageClient(object):
    def __init__(self):
        (twilio_number, twilio_account_sid,
         twilio_auth_token) = load_twilio_config()

        self.twilio_number = twilio_number
        self.twilio_client = Client(twilio_account_sid,
                                              twilio_auth_token)

    def send_message(self, body, to):
        self.twilio_client.messages.create(body=body, to=to,
                                           from_=self.twilio_number,
                                           # media_url=['https://demo.twilio.com/owl.png'])
                                           )

class TwilioNotificationsMiddleware(object):
    def __init__(self):
        self.client = MessageClient()

    def process_exception(self, request, exception):
        d = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        message_to_send = d + ':' + exception

        for number in SET.admin_phone_number:
            self.client.send_message(message_to_send, number)

        logger.info('[Administrators Notified with SMS]')

        return None
