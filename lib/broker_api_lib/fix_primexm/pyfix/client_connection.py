import socket
import ssl
import time
from lib.broker_api_lib.fix_primexm.pyfix.journaler import DuplicateSeqNoError
from lib.broker_api_lib.fix_primexm.pyfix.connection import FIXEndPoint, ConnectionState, FIXConnectionHandler
from lib.broker_api_lib.fix_primexm.pyfix.event import TimerEventRegistration
from lib.twilio_alarm.twilio_notifications.middleware import TwilioNotificationsMiddleware
from src.production.fx_logger import logger


class FIXClientConnectionHandler(FIXConnectionHandler):
    def __init__(self, engine, protocol, targetCompId, senderCompId, username, password, sock=None, addr=None, observer=None,
                 targetSubId=None, senderSubId=None, heartbeatTimeout=30):
        FIXConnectionHandler.__init__(self, engine, protocol, sock, addr, observer)

        self.targetCompId = targetCompId
        self.senderCompId = senderCompId
        self.username = username
        self.password = password
        self.targetSubId = targetSubId
        self.senderSubId = senderSubId
        self.heartbeatPeriod = float(heartbeatTimeout)

        # we need to send a login request when first create client
        self.session = self.engine.getOrCreateSessionFromCompIds(self.targetCompId, self.senderCompId)

        if self.session is None:
            raise RuntimeError("FIXClientConnectionHandler.__init__--Failed to create client session")

        # First time of logon
        self.sendMsg(protocol.messages.Messages.logon(self.username, self.password))

    def handleSessionMessage(self, msg):
        """
        Session relative management
        :param msg:
        :return:
        """
        protocol = self.codec.protocol
        responses = []

        recvSeqNo = msg[protocol.fixtags.MsgSeqNum]

        msgType = msg[protocol.fixtags.MsgType]
        targetCompId = msg[protocol.fixtags.TargetCompID]
        senderCompId = msg[protocol.fixtags.SenderCompID]

        if msgType == protocol.msgtype.LOGON:
            # Logged In again
            if self.connectionState == ConnectionState.LOGGED_IN:
                logger.warning(
                    "FIXClientConnectionHandler.handleSessionMessage--Client session already logged in - ignoring login request")
            # first time of connection setup
            else:
                try:
                    self.connectionState = ConnectionState.LOGGED_IN
                    self.heartbeatPeriod = float(msg[protocol.fixtags.HeartBtInt])
                    self.registerLoggedIn()
                except DuplicateSeqNoError:
                    logger.error(
                        "FIXClientConnectionHandler.handleSessionMessage--Failed to process login request with duplicate seq no")
                    self.disconnect()
                    return
        elif self.connectionState == ConnectionState.LOGGED_IN:
            # compids are reversed here
            if not self.session.validateCompIds(senderCompId, targetCompId):
                logger.error("Received message with unexpected comp ids")
                self.disconnect()
                return

            # if msgType == protocol.msgtype.HEARTBEAT:
            #     logger.info('[Receiving Heartbeat] %s', self.addr)

            elif msgType == protocol.msgtype.LOGOUT:
                self.connectionState = ConnectionState.LOGGED_OUT
                logger.info('[Receiving Log Out] %s', self.addr)
                self.registerLoggedOut()
                self.handle_close()

            elif msgType == protocol.msgtype.TESTREQUEST:
                logger.warning('[Receiving Heartbeat TestRequest] %s', self.addr)
                responses.append(protocol.messages.Messages.heartbeat())

            elif msgType == protocol.msgtype.RESENDREQUEST:
                logger.warning('[Receiving Resend Request] %s', self.addr)
                responses.extend(self._handleResendRequest(msg))

            elif msgType == protocol.msgtype.SEQUENCERESET:
                # we can treat GapFill and SequenceReset in the same way
                # in both cases we will just reset the seq number to the
                # NewSeqNo received in the message
                newSeqNo = msg[protocol.fixtags.NewSeqNo]
                if msg[protocol.fixtags.GapFillFlag] == "Y":
                    logger.info("Received SequenceReset(GapFill) filling gap from %s to %s" % (recvSeqNo, newSeqNo))
                self.session.setRecvSeqNo(int(newSeqNo) - 1)
                recvSeqNo = newSeqNo
        else:
            logger.warning("Can't process message, counterparty is not logged in msgType: %s" % str(msgType))
        return (recvSeqNo, responses)


class FIXClient(FIXEndPoint):
    def __init__(self, engine, protocol, targetCompId, senderCompId, username, password, targetSubId=None, senderSubId=None,
                 heartbeatTimeout=30):
        self.targetCompId = targetCompId
        self.senderCompId = senderCompId
        self.username = username
        self.password = password
        self.targetSubId = targetSubId
        self.senderSubId = senderSubId
        self.heartbeatTimeout = heartbeatTimeout
        self.connection_handler = None

        FIXEndPoint.__init__(self, engine, protocol)

    def tryConnecting(self, type, closure):
        try:
            if self.ssl:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket = ssl.wrap_socket(s)
            else:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            logger.info("[Attempting Connection]" + self.host + ":" + str(self.port))
            self.socket.connect((self.host, self.port))
            if self.connectionRetryTimer is not None:
                self.engine.eventManager.unregisterHandler(self.connectionRetryTimer)
                self.connectionRetryTimer = None
            self.connected()
        except socket.error as why:
            logger.error("%s, Connection failed, trying again in 5s", why)
            if self.connectionRetryTimer is None:
                self.connectionRetryTimer = TimerEventRegistration(self.tryConnecting, 5.0)
                self.engine.eventManager.registerHandler(self.connectionRetryTimer)

    def start(self, host, port, ssl):
        self.host = host
        self.port = port
        self.ssl = ssl
        self.connections = []
        self.connectionRetryTimer = None
        self.tryConnecting(None, None)

    def connected(self):
        self.addr = (self.host, self.port)
        logger.info("[Network Connected] %s", self.addr)
        self.connection_handler = FIXClientConnectionHandler(self.engine, self.protocol, self.targetCompId,
                                                             self.senderCompId, self.username, self.password, self.socket,
                                                             self.addr, self,
                                                             self.targetSubId, self.senderSubId, self.heartbeatTimeout)
        self.connections.append(self.connection_handler)
        for handler in filter(lambda x: x[1] == ConnectionState.CONNECTED, self.connectionHandlers):
            handler[0](self.connection_handler)

    def notifyDisconnect(self, connection):
        FIXEndPoint.notifyDisconnect(self, connection)

        notifier = TwilioNotificationsMiddleware()
        notifier.process_exception(None, '[Connection Loss] %s' % repr(self.addr))

        time.sleep(10)
        self.tryConnecting(None, None)

    def stop(self):

        for connection in self.connections:
            connection.disconnect()
        self.socket.close()

