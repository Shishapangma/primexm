import importlib
import sys
from lib.broker_api_lib.fix_primexm.pyfix.codec import Codec
from lib.broker_api_lib.fix_primexm.pyfix.journaler import DuplicateSeqNoError
from lib.broker_api_lib.fix_primexm.pyfix.message import FIXMessage, MessageDirection

from lib.broker_api_lib.fix_primexm.pyfix.session import *
from enum import Enum
from lib.broker_api_lib.fix_primexm.pyfix.event import FileDescriptorEventRegistration, EventType, TimerEventRegistration, \
    AutoTimerEventRegistration
from src.production.fx_logger import logger


class ConnectionState(Enum):
    UNKNOWN = 0
    DISCONNECTED = 1
    CONNECTED = 2
    LOGGED_IN = 3
    LOGGED_OUT = 4


class FIXException(Exception):
    class FIXExceptionReason(Enum):
        NOT_CONNECTED = 0
        DECODE_ERROR = 1
        ENCODE_ERROR = 2

    def __init__(self, reason, description=None):
        super(Exception, self).__init__(description)
        self.reason = reason


class SessionWarning(Exception):
    pass


class SessionError(Exception):
    pass


class FIXConnectionHandler(object):
    def __init__(self, engine, protocol, sock=None, addr=None, observer=None):
        self.codec = Codec(protocol)
        self.engine = engine
        self.connectionState = ConnectionState.CONNECTED
        self.session = None
        self.addr = addr
        self.observer = observer
        self.msgBuffer = b''
        self.heartbeatPeriod = 30.0
        self.msgHandlers = []
        self.sock = sock
        self.heartbeatTimerRegistration = None
        self.expectedHeartbeatRegistration = None
        self.autoHeartbeatTimerRegistration = None
        self.autoExpectedHeartbeatRegistration = None
        self.socketEvent = FileDescriptorEventRegistration(self.handle_read, sock, EventType.READ)
        self.engine.eventManager.registerHandler(self.socketEvent)

    def address(self):
        return self.addr

    def disconnect(self):
        self.handle_close()


    def _notifyMessageObservers(self, msg, direction, persistMessage=False):
        if persistMessage is True:
            self.engine.journaller.persistMsg(msg, self.session, direction)
        for handler in filter(lambda x: (x[1] is None or x[1] == direction) and (x[2] is None or x[2] == msg.msgType),
                              self.msgHandlers):
            handler[0](self, msg)

    def addMessageHandler(self, handler, direction=None, msgType=None):
        self.msgHandlers.append((handler, direction, msgType))

    def removeMessageHandler(self, handler, direction=None, msgType=None):
        remove = filter(lambda x: x[0] == handler and
                                  (x[1] == direction or direction is None) and
                                  (x[2] == msgType or msgType is None), self.msgHandlers)
        for h in remove:
            self.msgHandlers.remove(h)

    def _sendHeartbeat(self):
        """
        positive heart beat required
        :return:
        """
        msg = self.codec.protocol.messages.Messages.heartbeat()
        # logger.info('[Sending Heartbeat] %s', self.addr)
        self.sendMsg(msg)

    def _expectedHeartbeat(self, type, closure):
        """
        called only when expected heartbeat period expired
        :param type:
        :param closure:
        :return:
        """
        # logger.warning('[Expecting Heartbeat] %s', self.addr)
        self.sendMsg(self.codec.protocol.messages.Messages.test_request())

    def registerLoggedIn(self):
        """
        logon relative 2 procedure. heartbeat period trigger and expectedHeartbeat
        :return:
        """
        # hear beat
        # self.heartbeatTimerRegistration = TimerEventRegistration(lambda type, closure: self._sendHeartbeat(),
        #                                                          self.heartbeatPeriod)
        # self.engine.eventManager.registerHandler(self.heartbeatTimerRegistration)
        # register timeout for 10% more than we expect
        self.expectedHeartbeatRegistration = TimerEventRegistration(self._expectedHeartbeat,
                                                                    self.heartbeatPeriod * 1.10)
        self.engine.eventManager.registerHandler(self.expectedHeartbeatRegistration)
        # new heart beat strategy to replace initial one using thread
        self.autoHeartbeatTimerRegistration = AutoTimerEventRegistration(self._sendHeartbeat, self.heartbeatPeriod)
        self.autoHeartbeatTimerRegistration.start()

    def registerLoggedOut(self):
        if self.heartbeatTimerRegistration is not None:
            self.engine.eventManager.unregisterHandler(self.heartbeatTimerRegistration)
            self.heartbeatTimerRegistration = None
        if self.expectedHeartbeatRegistration is not None:
            self.engine.eventManager.unregisterHandler(self.expectedHeartbeatRegistration)
            self.expectedHeartbeatRegistration = None

    def _handleResendRequest(self, msg):
        protocol = self.codec.protocol
        responses = []

        beginSeqNo = msg[protocol.fixtags.BeginSeqNo]
        endSeqNo = msg[protocol.fixtags.EndSeqNo]
        if int(endSeqNo) == 0:
            endSeqNo = sys.maxsize
        replayMsgs = self.engine.journaller.recoverMsgs(self.session, MessageDirection.OUTBOUND, beginSeqNo, endSeqNo)
        gapFillBegin = int(beginSeqNo)
        gapFillEnd = int(beginSeqNo)
        for replayMsg in replayMsgs:
            msgSeqNum = int(replayMsg[protocol.fixtags.MsgSeqNum])
            if replayMsg[protocol.fixtags.MsgType] in protocol.msgtype.sessionMessageTypes:
                gapFillEnd = msgSeqNum + 1
            else:
                if self.engine.shouldResendMessage(self.session, replayMsg):
                    if gapFillBegin < gapFillEnd:
                        # we need to send a gap fill message
                        gapFillMsg = FIXMessage(protocol.msgtype.SEQUENCERESET)
                        gapFillMsg.setField(protocol.fixtags.GapFillFlag, 'Y')
                        gapFillMsg.setField(protocol.fixtags.MsgSeqNum, gapFillBegin)
                        gapFillMsg.setField(protocol.fixtags.NewSeqNo, str(gapFillEnd))
                        responses.append(gapFillMsg)

                    # and then resent the replayMsg
                    replayMsg.removeField(protocol.fixtags.BeginString)
                    replayMsg.removeField(protocol.fixtags.BodyLength)
                    replayMsg.removeField(protocol.fixtags.SendingTime)
                    replayMsg.removeField(protocol.fixtags.SenderCompID)
                    replayMsg.removeField(protocol.fixtags.TargetCompID)
                    replayMsg.removeField(protocol.fixtags.CheckSum)
                    replayMsg.setField(protocol.fixtags.PossDupFlag, "Y")
                    responses.append(replayMsg)

                    gapFillBegin = msgSeqNum + 1
                else:
                    gapFillEnd = msgSeqNum + 1
                    responses.append(replayMsg)

        if gapFillBegin < gapFillEnd:
            # we need to send a gap fill message
            gapFillMsg = FIXMessage(protocol.msgtype.SEQUENCERESET)
            gapFillMsg.setField(protocol.fixtags.GapFillFlag, 'Y')
            gapFillMsg.setField(protocol.fixtags.MsgSeqNum, gapFillBegin)
            gapFillMsg.setField(protocol.fixtags.NewSeqNo, str(gapFillEnd))
            responses.append(gapFillMsg)

        return responses

    def handle_read(self, type, closure):
        """
        Read buffer from file descriptor when select event called back
        :param type:
        :param closure:
        :return:
        """
        try:
            msg = self.sock.recv(8192)
            if msg:
                self.msgBuffer = self.msgBuffer + msg
                (decodedMsg, parsedLength) = self.codec.decode(self.msgBuffer)

                # if decodedMsg.msgType != self.codec.protocol.msgtype.MASSQUOTE:
                #     logger.info('[Server Msg Read] %s' % (self.msgBuffer))

                self.msgBuffer = self.msgBuffer[parsedLength:]
                while decodedMsg is not None and self.connectionState != ConnectionState.DISCONNECTED:
                    self.processMessage(decodedMsg)
                    (decodedMsg, parsedLength) = self.codec.decode(self.msgBuffer)
                    self.msgBuffer = self.msgBuffer[parsedLength:]
                if self.expectedHeartbeatRegistration is not None:
                    self.expectedHeartbeatRegistration.reset()
            else:
                logger.critical("Client Has Been Closed By Server %s %s:%s" % (msg, str(self.addr[0]), str(self.addr[1])))
                self.disconnect()
        except ConnectionError as why:
            logger.critical("Client Has Been Closed WHY-%s %s:%s" % (why, str(self.addr[0]), str(self.addr[1])))
            self.disconnect()
        except TimeoutError as why:
            logger.critical("TimeoutError WHY-%s %s:%s" % (why, str(self.addr[0]), str(self.addr[1])))
            self.disconnect()
        except Exception as why:
            logger.critical("Unexpected Error WHY-%s %s:%s" % (why, str(self.addr[0]), str(self.addr[1])))
            self.disconnect()

    def handleSessionMessage(self, msg):
        """
        virtual function, will be overwritten by sub-class
        dedicately for session relative message dealing
        :param msg:
        :return:
        """
        return -1

    def processMessage(self, decodedMsg):
        """
        SeqNum audit and notify handlers
        :param decodedMsg:
        :return:
        """
        protocol = self.codec.protocol
        beginString = decodedMsg[protocol.fixtags.BeginString]
        if beginString != protocol.beginstring:
            logger.critical(
                "FIXConnectionHandler.processMessage--FIX BeginString is incorrect (expected: %s received: %s)",
                (protocol.beginstring, beginString))
            self.disconnect()
            return

        msgType = decodedMsg[protocol.fixtags.MsgType]

        try:
            responses = []
            # Dedicatedly process session relative msg
            if msgType in protocol.msgtype.sessionMessageTypes:
                (recvSeqNo, responses) = self.handleSessionMessage(decodedMsg)
            else:
                recvSeqNo = decodedMsg[protocol.fixtags.MsgSeqNum]

            # validate the seq number to ensure response is not out-of-order
            (seqNoState, lastKnownSeqNo) = self.session.validateRecvSeqNo(recvSeqNo)

            if seqNoState is False:
                # JH: Only process SeqNo check for after logged
                # it is due to an unsolved reload message issue when reconnect
                if self.connectionState == ConnectionState.LOGGED_IN:
                    # We should send a resend request, seq is out-of-order
                    responses.append(protocol.messages.Messages.resend_request(lastKnownSeqNo, 0))
                    # we still need to notify if we are processing Logon message
                    if msgType == protocol.msgtype.LOGON:
                        self._notifyMessageObservers(decodedMsg, MessageDirection.INBOUND, False)
            else:
                # if everything is normal, increase SeqNo and notify handler
                self.session.setRecvSeqNo(recvSeqNo)
                if decodedMsg.msgType != self.codec.protocol.msgtype.MARKETDATASNAPSHOTFULLREFRESH:
                    self._notifyMessageObservers(decodedMsg, MessageDirection.INBOUND, False)
                else:
                    self._notifyMessageObservers(decodedMsg, MessageDirection.INBOUND)

            # process few cases required to feedback to server
            # it is coded in message type
            for m in responses:
                self.sendMsg(m)

        except SessionWarning as sw:
            logger.warning(sw)
        except SessionError as se:
            logger.error(se)
            self.disconnect()
        except DuplicateSeqNoError:
            try:
                if decodedMsg[protocol.fixtags.PossDupFlag] == "Y":
                    logger.critical(
                        "FIXConnectionHandler.processMessage--Received duplicate message with PossDupFlag set")
            except KeyError:
                pass
            finally:
                logger.critical(
                    "FIXConnectionHandler.processMessage--Failed to process message with duplicate seq no (MsgSeqNum: %s) (and no PossDupFlag='Y') - disconnecting" % (
                        recvSeqNo,))
                self.disconnect()

    def handle_close(self):
        if self.connectionState != ConnectionState.DISCONNECTED:
            logger.critical("Client Disconnected From Peer %s:%s" % (str(self.addr[0]), str(self.addr[1])))
            self.registerLoggedOut()
            self.sock.close()
            self.connectionState = ConnectionState.DISCONNECTED
            self.msgHandlers.clear()
            self.engine.eventManager.unregisterHandler(self.socketEvent)
            if self.autoHeartbeatTimerRegistration:
                self.autoHeartbeatTimerRegistration.set_deactive()
            if self.observer is not None:
                self.observer.notifyDisconnect(self)

    def sendMsg(self, msg):
        if self.connectionState != ConnectionState.CONNECTED and self.connectionState != ConnectionState.LOGGED_IN:
            raise FIXException(FIXException.FIXExceptionReason.NOT_CONNECTED, 'FIX Not Connected')
        encodedMsg = self.codec.encode(msg, self.session).encode('utf-8')

        # if msg.msgType != self.codec.protocol.msgtype.MASSQUOTEACKNOWLEDGEMENT:
        #     logger.info('[Client Msg Send] %s' %encodedMsg)

        try:
            self.sock.send(encodedMsg)
        except ConnectionError as why:
            logger.critical("Client Has Been Closed WHY-%s %s:%s" % (why, str(self.addr[0]), str(self.addr[1])))
            self.disconnect()
        # JH
        # if self.heartbeatTimerRegistration is not None:
        #     self.heartbeatTimerRegistration.reset()
        decodedMsg, junk = self.codec.decode(encodedMsg)

        try:
            self._notifyMessageObservers(decodedMsg, MessageDirection.OUTBOUND, False)
        except DuplicateSeqNoError:
            logger.critical("We have sent a message with a duplicate seq no, failed to persist it (MsgSeqNum: %s)" % (
                decodedMsg[self.codec.protocol.fixtags.MsgSeqNum]))


class FIXEndPoint(object):
    def __init__(self, engine, protocol):
        self.engine = engine
        self.protocol = importlib.import_module(protocol)

        self.connections = []
        self.connectionHandlers = []

    def writable(self):
        return True

    def start(self, host, port, ssl):
        pass

    def stop(self):
        pass

    def addConnectionListener(self, handler, filter):
        self.connectionHandlers.append((handler, filter))

    def removeConnectionListener(self, handler, filter):
        for s in self.connectionHandlers:
            if s == (handler, filter):
                self.connectionHandlers.remove(s)

    def notifyDisconnect(self, connection):
        self.connections.remove(connection)
        for handler in filter(lambda x: x[1] == ConnectionState.DISCONNECTED, self.connectionHandlers):
            handler[0](connection)
        # Added by JH
        self.resetSession()

    # Added by JH
    def resetSession(self):
        self.engine.resetSession()
