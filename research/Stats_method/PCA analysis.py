# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt

p = pd.DataFrame()

files = os.listdir(r'C:\Research\data\Dukascopy\2017usd')
for d in range(0,len(files)):
    data_duka = pd.read_csv('C:\\Research\\data\\Dukascopy\\2017usd\\' + files[d], header=0)
    data_duka.index = pd.to_datetime(data_duka['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
    data_duka = pm.fx_data_cleaning(data_duka)
    name = files[d][0:6]
    p[name] = data_duka['Close']

ret = p.pct_change()


corr_p = ret.cov()
evals, evecs = np.linalg.eig(corr_p)

evecs = pd.DataFrame(evecs)
pcas = pd.DataFrame(np.dot(ret, evecs.ix[:,0:7]),index = p.index)
pcas_ret = pcas


# pcas.plot()
a = pd.DataFrame()
for i in range(1,10):
    a[i]=pcas_ret.corrwith(pcas_ret.shift(i)).values
a.plot(kind='bar')

ret.corrwith(ret.shift(1))

# ret1 = pd.DataFrame(ret['AUDUSD'] - ret['NZDUSD'])
# ret1.corrwith(ret1.shift(1))
#
retx = pd.DataFrame(ret['EURUSD']*-1+ret['GBPUSD'])
# ret1.corrwith(ret1.shift(1))
#
# ret1 = pd.DataFrame(ret['USDCAD']*-0.8-ret['NZDUSD']*0.53)
# ret1.corrwith(ret1.shift(1))
all = pd.DataFrame()
alpha_all = pd.DataFrame()
for i in range(0,8):
    retx = pcas_ret[pcas_ret.columns[i]]
    alpha =(pd.ewma(retx, halflife=200) - pd.ewma(retx, halflife=20)) / pd.ewmstd(retx, halflife=200)
    alpha_all = pd.concat([alpha_all,alpha],axis=1)
    str_ret = (alpha.shift(1) * retx)
    all = pd.concat([all,str_ret],axis=1)
    print(pm.PerformanceStatistics(str_ret, 60 * 24 * 300, 0))
    print(pd.DataFrame(alpha).corrwith(pd.DataFrame(alpha).shift(1)))

#########################################################################################################
alpha_all_s = alpha_all.ix[:,3:6]
vec_s = evecs.ix[:,3:6]

alpha_new = pd.DataFrame(alpha_all_s.values.dot(vec_s.T.values),index = alpha_all_s.index, columns = ret.columns)

d = 5
symbols = ['audusd', 'eurusd', 'gbpusd', 'nzdusd', 'usdcad', 'usdchf', 'usdjpy','usdsgd']
hour_skip_ao = ['21:00:00', '22:00:00']
hour_skip_oa = ['22:00:00', '23:00:00']
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_' + symbols[d] + '.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()

strategy1 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy1.get_strategy_weight_for_sample(order_type='mkt', signal=alpha_new[alpha_new.columns[d]], estimated_tc=TC_by_hour,
                                        ask_low=p[p.columns[d]], bid_high=p[p.columns[d]], mid_price=p[p.columns[d]], tick_incremental=0.0000,
                                        commission=0.13, nav=1, returns=pd.Series(p[p.columns[d]].pct_change().values, p[p.columns[d]].index),
                                        alpha_type='custom', hour_select_ao=hour_skip_ao, hour_select_oa=hour_skip_oa)

pm.PerformanceStatistics(strategy1.net_return, 60 * 24 * 300, 0)
strategy1.net_return.cumsum().plot()

strategy1.net_return.to_clipboard()