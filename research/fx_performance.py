import pandas as pd
import numpy as np


class PerformanceCalc(object):
    def __init__(self, weight, returns):
        self._weight = weight
        self._returns = returns
        self._gross_ret = np.nan
        self._turnover = np.nan
        self._transaction_costs = np.nan
        self._net_ret = np.nan
        self._gross_ret_daily = np.nan
        self._net_ret_daily = np.nan
        self._sharpe_ratio_gross = np.nan
        self._sharpe_ratio_net = np.nan
        self.output = pd.DataFrame()

    def get_performance_stats(self, tc):
        w = self._weight.values
        ret = self._returns.values
        self._gross_ret = w[0:-1] * ret[1:]
        self._turnover = np.abs(np.diff(w, axis=0))
        self._transaction_costs = self._turnover * tc
        self._net_ret = self._gross_ret - self._transaction_costs
        self._net_ret_daily = (
        pd.DataFrame(self._net_ret, index=self._returns.index[1:], columns=self._returns.columns).add(
            1)).cumprod().resample('D').pct_change().values
        self._gross_ret_daily = (
        pd.DataFrame(self._gross_ret, index=self._returns.index[1:], columns=self._returns.columns).add(
            1)).cumprod().resample('D').pct_change().values
        self._sharpe_ratio_gross = np.nanmean(self._gross_ret_daily, axis=0) / np.nanstd(self._gross_ret_daily,
                                                                                         axis=0) * np.sqrt(300)
        self._sharpe_ratio_net = np.nanmean(self._net_ret_daily, axis=0) / np.nanstd(self._net_ret_daily,
                                                                                     axis=0) * np.sqrt(300)

    def show_performance_stats(self):
        print('gross return: ' + str(np.nansum(self._gross_ret_daily)))
        print('gross return per 2 leg turnover: ' + str(
                (np.nansum(self._gross_ret_daily) / np.nansum(self._turnover) * 20000)))
        print('net return: ' + str(np.nansum(self._net_ret_daily)))
        print('trans cost: ' + str(np.nansum(self._transaction_costs)))
        print('turnover daily: ' + str(np.nansum(self._turnover) / len(self._turnover) * 1440))
        print('net return annual: ' + str(np.nanmean(self._net_ret_daily) * 300))
        print('annual volatility: ' + str(np.nanstd(self._net_ret_daily) * np.sqrt(300)))
        print('gross Sharpe ratio: ' + str(self._sharpe_ratio_gross[0]))
        print('net Sharpe ratio: ' + str(self._sharpe_ratio_net[0]))

    def store_performance_stats(self):
        self.output[0] = [np.nansum(self._gross_ret_daily)]
        self.output[1] = [np.nansum(self._gross_ret_daily) / np.nansum(self._turnover) * 20000]
        self.output[2] = [np.nansum(self._net_ret_daily)]
        self.output[3] = [np.nansum(self._transaction_costs)]
        self.output[4] = [np.nansum(self._turnover) / len(self._turnover) * 1440]
        self.output[5] = [np.nanmean(self._net_ret_daily) * 300]
        self.output[6] = [np.nanstd(self._net_ret_daily) * np.sqrt(300)]
        self.output[7] = self._sharpe_ratio_gross
        self.output[8] = self._sharpe_ratio_net


class StrategyWeight(object):
    def __init__(self):
        self._alpha = pd.DataFrame()
        self._w = pd.DataFrame()
        self._risk = pd.DataFrame()

    def strategy_alpha_ret_mean_reversion(self, ret, hl_l, hl_s):
        self._alpha = pd.DataFrame(
                (pd.ewma(ret, halflife=hl_l) - pd.ewma(ret, halflife=hl_s)) / pd.ewmstd(ret, halflife=hl_l))
        self._alpha[self._alpha.isnull()] = 0

    def strategy_risk_forecast(self, ret, hl, lambda_risk):
        self._risk = pd.ewmvar(ret, halflife=hl) * lambda_risk

    def get_weight_simple_alpha_with_tc(self, alpha, tc, kappa):

        self._w = np.zeros(np.shape(alpha))

        for i in range(1, len(self._w)):
            if alpha.ix[i, :].values > (self._w[i - 1, :] + tc * kappa):
                self._w[i, :] = alpha.ix[i, :] - tc * kappa
            elif alpha.ix[i, :].values < (self._w[i - 1, :] - tc * kappa):
                self._w[i, :] = alpha.ix[i, :] + tc * kappa
            else:
                self._w[i, :] = self._w[i - 1, :]

        self._w = pd.DataFrame(self._w, index=alpha.index, columns=alpha.columns)

    def get_weight_simple_alpha_with_tc_risk(self, alpha, risk, tc, kappa):

        self._w = np.zeros(np.shape(alpha))

        for i in range(1, len(self._w)):
            if alpha.ix[i, :].values > ((self._w[i - 1, :] + tc * kappa) * risk.ix[i, :].values):
                self._w[i, :] = (alpha.ix[i, :] - tc * kappa) / (risk.ix[i, :])
            elif alpha.ix[i, :].values < ((self._w[i - 1, :] - tc * kappa) * risk.ix[i, :].values):
                self._w[i, :] = (alpha.ix[i, :] + tc * kappa) / (risk.ix[i, :])
            else:
                self._w[i, :] = self._w[i - 1, :]

        self._w = pd.DataFrame(self._w, index=alpha.index, columns=alpha.columns)
