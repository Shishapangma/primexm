# test code
# from research.strategy_wrapper_research_jh import *
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt

start = '2017-08-20 21:00:00'
end = '2017-08-21 21:00:00'
folder = '21-Aug-2017'

duka_path = 'C:\\Users\\jquan\\Documents\\Trading Data\\5-Dukascopy\\'
lmax_tick_path = 'C:\\Users\\jquan\\Documents\\Trading Data\\7-LMAX Tick\\'
ge_tick_path = '/Users/jerryhuang/Documents/Trading Data/10-PrimeXM Tick/'
duka_skip_path = '/Users/jerryhuang/Documents/Trading Data/6-Simulate Log/'
product_path = 'C:\\Users\\jquan\\Documents\\Trading Data\\1-Trading Log\\'
lmax_demo_path = 'C:\\Users\\jquan\Documents\\Trading Data\\8-LMAX Demo\\'

d = 4
hour_skip_ao = ['21:00:00', '22:00:00']
# hour_skip_ao = []
hour_skip_oa = ['22:00:00', '23:00:00']
hour_skip_oa = ['22:00:00', '23:00:00']

symbols = ['audusd', 'eurchf', 'eurgbp', 'nzdusd', 'usdsgd']
multi = [0.25, 0.5, 0.3, 0.25, 0.75]
tc = pd.read_csv('C:\\Users\\jquan\Documents\\Trading Data\\4-TC\\tc_' + symbols[d] + '.csv', header=0)
TC_by_hour = (tc.iloc[:, 0] * 10000).values.tolist()

#################################################################################################################
# Dukascopy
files = os.listdir(duka_path)

try:
    files.remove('.DS_Store')
except ValueError:
    pass

data_duka = pd.read_csv(duka_path + files[d], header=0)
data_duka.index = pd.to_datetime(data_duka['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
data_duka = pm.fx_data_cleaning(data_duka)
p = data_duka['Close']
strategy_duka = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy_duka.get_strategy_weight_for_sample(order_type='mkt',
                                        signal=p.pct_change(),
                                        estimated_tc=TC_by_hour,
                                        ask_low=p, bid_high=p,
                                        mid_price=p, bid_price=p, ask_price=p,
                                        tick_incremental=pd.Series(0.0000, index=p.index),
                                        commission=0.09, nav=1,
                                        returns=pd.Series(p.pct_change().values, p.index),
                                        alpha_type='simple',
                                        tc_type='hour',
                                        hour_select_ao=hour_skip_ao,
                                        hour_select_oa=hour_skip_oa,
                                        scale_factor=10)

(strategy_duka.net_return['2010-01-01 23:00:00':]).cumsum().plot()
print(pm.PerformanceStatistics(strategy_duka.net_return, 300 * 60 * 24, 0))
strategy_duka.turnover.mean()
strategy_duka.net_return['2017-06-05':].resample('1D').sum().to_clipboard()
strategy_duka.net_return.resample('1D').sum().to_clipboard()

######################################################################################################
# LMAX Tick with true spread
# symbol = symbols[d].upper()[:3] + '_' + symbols[d].upper()[3:]
# data_lmax = pd.read_csv(lmax_tick_path + folder + '/' + symbol + '.csv', header=0)
# data_lmax = data_lmax[data_lmax['event'] == 'Dummy_Tick']
# data_lmax['time_index'] = data_lmax['date'].astype(str) + '-' + data_lmax['time']
# data_lmax.index = pd.to_datetime(data_lmax['time_index'].str[:-1], format='%Y%m%d-%H:%M:%S.%f')
# data_lmax['bid'] = data_lmax['bid'].astype(float)
# data_lmax['ask'] = data_lmax['ask'].astype(float)
# data_lmax = data_lmax.resample('1min').last().fillna(method='pad').shift(1)
# p_spread = data_lmax['ask'] - data_lmax['bid']
# p_lmax = data_lmax['bid']

# data_limit1 = data_lmax[data_lmax['event'] == 'Dummy_Tick']
# data_limit1.time = data_limit1.date.astype(str) + '-' + data_limit1.time
# data_limit1.index = pd.to_datetime(data_limit1.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
# data_limit1[['ask','bid']] = data_limit1[['ask','bid']].astype(float)
# bid_ask = data_limit1[['ask','bid']].resample('1S').last().fillna(method='pad').shift(1)
#
# p = data_limit1['bid'].resample('60S').last().fillna(method='pad').shift(1)
# ask = bid_ask['ask'].resample('60S').last().fillna(method='pad').shift(1)
# bid = bid_ask['bid'].resample('60S').last().fillna(method='pad').shift(1)
# pp_spread = ask - bid


# strategy_lmax_true_spread = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
# strategy_lmax_true_spread.get_strategy_weight_for_sample(order_type='mkt',
#                                                          signal=p_lmax.pct_change(),
#                                                          estimated_tc=TC_by_hour,
#                                                          ask_low=p_lmax,
#                                                          bid_high=p_lmax,
#                                                          mid_price=p_lmax,
#                                                          tick_incremental=0.0000,
#                                                          commission=0.09,
#                                                          nav=1,
#                                                          returns=pd.Series(p_lmax.pct_change().values, p_lmax.index),
#                                                          alpha_type='simple',
#                                                          hour_select_ao=hour_skip_ao,
#                                                          hour_select_oa=hour_skip_oa,
#                                                          scale_factor=10,
#                                                          spread=p_spread)

######################################################################################################
# GE Raw Tick
# symbol = symbols[d].upper()[:3] + '_' + symbols[d].upper()[3:]
# data_ge = pd.read_csv(ge_tick_path + folder +'/' + symbol + '.csv',header=0)
# data_ge = data_ge[data_ge['event'] == 'Dummy_Tick']
# data_ge['time_index'] = data_ge['date'].astype(str) + '-' + data_ge['time']
# data_ge.index = pd.to_datetime(data_ge['time_index'].str[:-1], format='%Y%m%d-%H:%M:%S.%f')
# data_ge = data_ge.resample('1min', how='last').fillna(method='pad').shift(1)
# data_ge = pm.fx_data_cleaning(data_ge)
# p_ge = data_ge['bid'].astype(float) * 0.5 + data_ge['ask'].astype(float) * 0.5
# strategy_ge_raw = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
# strategy_ge_raw.get_strategy_weight_for_sample(order_type='mkt', signal=p_ge.pct_change(), estimated_tc=TC_by_hour,
#                                         ask_low=p_ge, bid_high=p_ge, mid_price=p_ge, tick_incremental=0.0000,
#                                         commission=0.09, nav=1, returns=pd.Series(p_ge.pct_change().values, p_ge.index),
#                                         alpha_type='simple', hour_select_ao=hour_skip_ao, hour_select_oa=hour_skip_oa, scale_factor=10,spread = p_spread)

######################################################################################################
# Production
path = product_path + folder + "\\"
files = os.listdir(path)
try:
    files.remove('.DS_Store')
except ValueError:
    pass
data_ori = pd.read_csv(path + files[d], header=0)
data_limit = data_ori[
    np.logical_or(np.logical_or(data_ori['event'] == 'Order_Create', data_ori['event'] == 'Order_Filled'),
                  data_ori['event'] == 'One_Min_Tick')]
data_limit.time = data_limit.date.astype(str) + '-' + data_limit.time
data_limit.index = pd.to_datetime(data_limit.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
data_limit[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
    data_limit[
        ['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']].astype(
        float)
data_limit['mid'] = 0.5 * data_limit['bid'] + 0.5 * data_limit['ask']
data_limit['holding_return'] = (data_limit['mid'] - data_limit['mid'].shift(1)) * data_limit['current position'].shift(
    1)
data_limit['slippage'] = (data_limit['mid'] - data_limit['trade price']) * data_limit[
    'traded position'] - 0.09 / 10000 * np.abs(data_limit['traded position']) * data_limit['trade price']
data_limit['actual_return'] = data_limit['holding_return'] + data_limit['slippage']
data_limit['target position'].where(data_limit['target position'] != 0, None, inplace=True)
data_limit['target position'].fillna(method='ffill', inplace=True)
data_limit['theory_return'] = (data_limit['mid'] - data_limit['mid'].shift(1)) * data_limit['target position'].shift(
    1) - 0.09 / 10000 * np.abs(data_limit['target position'].diff()) * data_limit['mid']

######################################################################################################
# LMAX Simulate
# files = os.listdir(duka_skip_path)
# try:
#     files.remove('.DS_Store')
# except ValueError:
#     pass
# data_ori = pd.read_csv(duka_skip_path + files[d], header=0)
# data_sim = data_ori[
#     np.logical_or(np.logical_or(data_ori['event'] == 'Order_Create', data_ori['event'] == 'Order_Filled'),
#                   data_ori['event'] == 'One_Min_Tick')]
# data_sim.time = data_sim.date.astype(str) + '-' + data_sim.time
# data_sim.index = pd.to_datetime(data_sim.time.str[:-3], format='%Y%m%d-%H:%M:%S.%f')
# data_sim[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
#     data_sim[
#         ['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']].astype(
#         float)
# data_sim['mid'] = 0.5 * data_sim['bid'] + 0.5 * data_sim['ask']
# data_sim['holding_return'] = (data_sim['mid'] - data_sim['mid'].shift(1)) * data_sim['current position'].shift(
#     1)
# data_sim['slippage'] = (data_sim['mid'] - data_sim['trade price']) * data_sim[
#     'traded position'] - 0.09 / 10000 * np.abs(data_sim['traded position']) * data_sim['trade price']
# data_sim['actual_return'] = data_sim['holding_return'] + data_sim['slippage']
# data_sim['target position'].where(data_sim['target position'] != 0, None, inplace=True)
# data_sim['target position'].fillna(method='ffill', inplace=True)
# data_sim['theory_return'] = (data_sim['mid'] - data_sim['mid'].shift(1)) * data_sim['target position'].shift(
#     1) - 0.09 / 10000 * np.abs(data_sim['target position'].diff()) * data_sim['mid']

######################################################################################################
# LMAX Demo
# path = lmax_demo_path + folder + '/'
# files = os.listdir(path)
# try:
#     files.remove('.DS_Store')
# except ValueError:
#     pass
# data_ori = pd.read_csv(path + files[d], header=0)
# data_lmax_dmeo = data_ori[
#     np.logical_or(np.logical_or(data_ori['event'] == 'Order_Create', data_ori['event'] == 'Order_Filled'),
#                   data_ori['event'] == 'One_Min_Tick')]
# data_lmax_dmeo.time = data_lmax_dmeo.date.astype(str) + '-' + data_lmax_dmeo.time
# data_lmax_dmeo.index = pd.to_datetime(data_lmax_dmeo.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
# data_lmax_dmeo[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
#     data_lmax_dmeo[
#         ['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']].astype(
#         float)
# data_lmax_dmeo['mid'] = 0.5 * data_lmax_dmeo['bid'] + 0.5 * data_lmax_dmeo['ask']
# data_lmax_dmeo['holding_return'] = (data_lmax_dmeo['mid'] - data_lmax_dmeo['mid'].shift(1)) * data_lmax_dmeo['current position'].shift(
#     1)
# data_lmax_dmeo['slippage'] = (data_lmax_dmeo['mid'] - data_lmax_dmeo['trade price']) * data_lmax_dmeo[
#     'traded position'] - 0.09 / 10000 * np.abs(data_lmax_dmeo['traded position']) * data_lmax_dmeo['trade price']
# data_lmax_dmeo['actual_return'] = data_lmax_dmeo['holding_return'] + data_lmax_dmeo['slippage']
# data_lmax_dmeo['target position'].where(data_lmax_dmeo['target position'] != 0, None, inplace=True)
# data_lmax_dmeo['target position'].fillna(method='ffill', inplace=True)
# data_lmax_dmeo['theory_return'] = (data_lmax_dmeo['mid'] - data_lmax_dmeo['mid'].shift(1)) * data_lmax_dmeo['target position'].shift(
#     1) - 0.09 / 10000 * np.abs(data_lmax_dmeo['target position'].diff()) * data_lmax_dmeo['mid']
######################################################################################################

# symbol = symbols[d]
# multic = multi[d] * 150000
# multic = 1.5e5

# plt.plot(strategy_duka.net_return[start:end].cumsum() * multic, label='Duka')
# print('Duka:', strategy_duka.net_return['net_return'][start:end].cumsum().iloc[-1] * multic)

# plt.plot(strategy_lmax_true_spread.net_return[start:end].cumsum() * multic, label='Lmax Tick Present Spread')
# print('lmax tick present spread:',
#       strategy_lmax_true_spread.net_return['net_return'][start:end].cumsum().iloc[-1] * multic)

# plt.plot(strategy_lmax_fake_spread.net_return[start:end].cumsum() * multic, label='Lmax Tick Hour Spread')
# print('lmax tick hour spread:',
#       strategy_lmax_fake_spread.net_return['net_return'][start:end].cumsum().iloc[-1] * multic)

# plt.plot(strategy_ge_raw.net_return[start:end].cumsum() * multic, label='GE')
# print('ge:', strategy_ge_raw.net_return['net_return'][start:end].cumsum().iloc[-1] * multic)
#
# plt.plot(data_limit['actual_return'][start:end].cumsum(), label='Production')
# print('Production:', data_limit['actual_return'][start:end].cumsum().iloc[-1])

# plt.plot(data_sim['actual_return'][start:end].cumsum(), label='LMAX Sim')
# print('LMAX Sim:', data_sim['actual_return'][start:end].cumsum().iloc[-1])
#
# plt.plot(data_lmax_dmeo['actual_return'][start:end].cumsum(), label='LMAX Demo')

f = plt.figure()
symbol = symbols[d]
f.suptitle(symbol)
multic = multi[d] * 150000
date = ['2017-07-23', '2017-07-24', '2017-07-25', '2017-07-26', '2017-07-27', '2017-07-28']
output = pd.DataFrame(index=date, columns=['duka', 'duka_skip', 'live'])
for i in range(0, 5):
    start = date[i] + ' 21:00:00'
    end = date[i + 1] + ' 21:00:00'
    ax = f.add_subplot(2, 3, i + 1)
    ax.plot(strategy_duka.net_return[start:end].cumsum() * multic, label='Research_backtest')
    ax.plot(strategy2.net_return[start:end].cumsum() * multic, label='tick lmax')
    ax.plot(data_limit['actual_return'][start:end].cumsum(), label='live')
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05), ncol=3)
    output['duka'][date[i + 1]] = strategy_duka.net_return[start:end].cumsum()[-1:].values[0][0] * multic
    output['duka_skip'][date[i + 1]] = strategy2.net_return[start:end].cumsum()[-1:].values[0][0] * multic
    output['live'][date[i + 1]] = data_limit['actual_return'][start:end].cumsum()[-1:].values[0]

output.to_clipboard()
