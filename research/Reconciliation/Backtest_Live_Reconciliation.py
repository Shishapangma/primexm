# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt

d = 1
# hour_skip_ao = ['7:00:00','8:00:00','21:00:00', '22:00:00']
# hour_skip_oa = ['8:00:00','9:00:00','21:00:00', '22:00:00']

hour_skip_ao = ['21:00:00', '22:00:00']
hour_skip_oa = ['22:00:00', '23:00:00']

symbols = ['audusd', 'eurchf', 'eurgbp', 'nzdusd', 'usdsgd']
multi = [0.25, 0.9, 0.3, 0.25, 0.75]
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_lmax\tc_' + symbols[d] + '.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()

###############################################################################################################################
# duka - no skip hour
files = os.listdir(r'C:\Research\data\Dukascopy\2017w\\')
data_duka = pd.read_csv('C:\\Research\\data\\Dukascopy\\2017w\\' + files[d], header=0)
data_duka.index = pd.to_datetime(data_duka['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
data_duka = pm.fx_data_cleaning(data_duka)
p = data_duka['Close']
strategy = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy.get_strategy_weight_for_sample(order_type='mkt',
                                        signal=p.pct_change(),
                                        estimated_tc=TC_by_hour,
                                        ask_low=p, bid_high=p,
                                        mid_price=p, bid_price=p, ask_price=p,
                                        tick_incremental=pd.Series(0.0000, index=p.index),
                                        commission=0.09, nav=1,
                                        returns=pd.Series(p.pct_change().values, p.index),
                                        alpha_type='simple',
                                        tc_type='hour',
                                        hour_select_ao=hour_skip_ao,
                                        hour_select_oa=hour_skip_oa,
                                        scale_factor=10)

(strategy.net_return['2010-01-01 23:00:00':]).cumsum().plot()
print(pm.PerformanceStatistics(strategy.net_return, 300 * 60 * 24, 0))
strategy.turnover.mean()
strategy.net_return['2017-06-05':].resample('1D').sum().to_clipboard()
strategy.net_return.resample('1D').sum().to_clipboard()
######################################################################################################
# duka - skip hour
# hour_skip_ao = ['00:00:00','18:00:00','19:00:00','20:00:00', '21:00:00', '22:00:00']
# strategy1 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
# strategy1.get_strategy_weight_for_sample(order_type='mkt',
#                                          signal=p.pct_change(),
#                                          estimated_tc=TC_by_hour,
#                                          ask_low=p, bid_high=p, mid_price=p, bid_price=p, ask_price=p,
#                                          tick_incremental=pd.Series(0.0000, index=p.index),
#                                          commission=0.09, nav=1,
#                                          returns=pd.Series(p.pct_change().values, p.index),
#                                          alpha_type='simple',
#                                          tc_type='hour',
#                                          hour_select_ao=hour_skip_ao,
#                                          hour_select_oa=hour_skip_oa,
#                                          scale_factor=10)
######################################################################################################
# Live - LMAX
path = r'C:\Research\data\log_live_archive\historical data\29-Jul-2017\combine\\'
files = os.listdir(path)
data_ori = pd.read_csv(path + files[d], header=0)
data_limit = data_ori[np.logical_or(np.logical_or(data_ori['event'] == 'Order_Create',
                                                  data_ori['event'] == 'Order_Filled'),
                                    data_ori['event'] == 'One_Min_Tick')]
data_limit.time = data_limit.date.astype(str) + '-' + data_limit.time
data_limit.index = pd.to_datetime(data_limit.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
data_limit[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
    data_limit[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']].astype(float)
data_limit['mid'] = 0.5 * data_limit['bid'] + 0.5 * data_limit['ask']
data_limit['holding_return'] = (data_limit['mid'] - data_limit['mid'].shift(1))\
                               * data_limit['current position'].shift(1)
data_limit['slippage'] = (data_limit['mid'] - data_limit['trade price']) * data_limit['traded position']\
                         - 0.09 / 10000 * np.abs(data_limit['traded position']) * data_limit['trade price']
data_limit['actual_return'] = data_limit['holding_return'] + data_limit['slippage']
data_limit['target position'].where(data_limit['target position'] != 0, None, inplace=True)
data_limit['target position'].fillna(method='ffill', inplace=True)
data_limit['theory_return'] = (data_limit['mid'] - data_limit['mid'].shift(1)) * data_limit['target position'].shift(1)\
                              - 0.09 / 10000 * np.abs(data_limit['target position'].diff()) * data_limit['mid']

######################################################################################################
# LMAX data
path = r'C:\Research\data\log_live_archive\historical data\29-Jul-2017\tick\combine\\'
files = os.listdir(path)
data_ori = pd.read_csv(path + files[d], header=0)
data_limit1 = data_ori[data_ori['event'] == 'Dummy_Tick']
data_limit1.time = data_limit1.date.astype(str) + '-' + data_limit1.time
data_limit1.index = pd.to_datetime(data_limit1.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
data_limit1[['ask','bid']] = data_limit1[['ask','bid']].astype(float)
bid_ask = data_limit1[['ask','bid']].resample('1S').last().fillna(method='pad').shift(1)

p = data_limit1['bid'].resample('60S').last().fillna(method='pad').shift(1)
ask = bid_ask['ask'].resample('60S').last().fillna(method='pad').shift(1)
bid = bid_ask['bid'].resample('60S').last().fillna(method='pad').shift(1)
# p= data_limit['bid'][data_limit['event']=='One_Min_Tick']
strategy2 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy2.get_strategy_weight_for_sample(order_type='mkt',
                                         signal=p.pct_change(),
                                         estimated_tc=TC_by_hour,
                                         ask_low=p, bid_high=p, mid_price=p, bid_price=bid, ask_price=ask,
                                         tick_incremental=pd.Series(0.0000, index=p.index),
                                         commission=0.09, nav=1,
                                         returns=pd.Series(p.pct_change().values, p.index),
                                         alpha_type='simple',
                                         tc_type='hour',
                                         hour_select_ao=hour_skip_ao,
                                         hour_select_oa=hour_skip_oa,
                                         scale_factor=10)
######################################################################################################
f = plt.figure()
symbol = symbols[d]
f.suptitle(symbol)
multic = multi[d] * 150000
date = ['2017-07-23', '2017-07-24', '2017-07-25', '2017-07-26', '2017-07-27', '2017-07-28']
output = pd.DataFrame(index=date, columns=['duka', 'duka_skip', 'live'])
for i in range(0, 5):
    start = date[i] + ' 21:00:00'
    end = date[i + 1] + ' 21:00:00'
    ax = f.add_subplot(2, 3, i + 1)
    ax.plot(strategy.net_return[start:end].cumsum() * multic, label='Research_backtest')
    ax.plot(strategy2.net_return[start:end].cumsum() * multic, label='tick lmax')
    ax.plot(data_limit['actual_return'][start:end].cumsum(), label='live')
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05), ncol=3)
    output['duka'][date[i + 1]] = strategy.net_return[start:end].cumsum()[-1:].values[0][0] * multic
    output['duka_skip'][date[i + 1]] = strategy2.net_return[start:end].cumsum()[-1:].values[0][0] * multic
    output['live'][date[i + 1]] = data_limit['actual_return'][start:end].cumsum()[-1:].values[0]

output.to_clipboard()


