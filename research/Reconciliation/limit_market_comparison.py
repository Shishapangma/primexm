# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.fx_performance as fx
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt

d=5
# symbols = ['audcad','audusd','eurchf','usdsgd']
# multi = [0.3, 1, 0.5]
################################################################################################################################
path = 'C:\\Research\\data\\log market\\'
# path = r'C:\Research\data\log_live_archive\lmax demo\market order/'
files = os.listdir(path)
data_ori = pd.read_csv(path + files[d], header=0)
data = data_ori[np.logical_and(data_ori['event'] != 'One_Sec_Tick', np.logical_and(data_ori['event'] != 'event', data_ori['event'] != 'Tick'))]
data.index = pd.to_datetime(data.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
data[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price', 'tc','alpha','alpha scale']] = \
    data[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price', 'tc','alpha','alpha scale']].astype(float)
data['tc'].where(data['tc']!=0,np.nan, inplace = True)
data['tc'].fillna(method = 'ffill', inplace=True)
data['mid'] = 0.5 * data['bid'] + 0.5 * data['ask']
data['holding_return'] = (data['mid'] - data['mid'].shift(1)) * data['current position'].shift(1)
data['slippage'] = (data['mid'] - data['trade price']) * data['traded position'] - 0.12/10000*np.abs(data['traded position'])
data['actual_return'] = data['holding_return'] + data['slippage']
data['target position'].where(data['target position'] != 0, None, inplace=True)
data['target position'].fillna(method='ffill', inplace=True)
data['theory_return'] = (data['mid'] - data['mid'].shift(1)) * data['target position'].shift(1)

################################################################################################################################
path = 'C:\\Research\\data\\log limit\\'
# path = r'C:\Research\data\log_live_archive\lmax demo\limit order/'
files = os.listdir(path)
data_ori = pd.read_csv(path + files[d], header=0)
data_limit = data_ori[np.logical_and(data_ori['event'] != 'event', data_ori['event'] != 'Tick')]
data_limit.index = pd.to_datetime(data_limit.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
data_limit[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price', 'tc','alpha','alpha scale']] = \
    data_limit[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price', 'tc','alpha','alpha scale']].astype(float)
data_limit['tc'].where(data_limit['tc']!=0,np.nan, inplace = True)
data_limit['tc'].fillna(method = 'ffill', inplace=True)
data_limit['mid'] = 0.5 * data_limit['bid'] + 0.5 * data_limit['ask']
data_limit['holding_return'] = (data_limit['mid'] - data_limit['mid'].shift(1)) * data_limit['current position'].shift(1)
data_limit['slippage'] = (data_limit['mid'] - data_limit['trade price']) * data_limit['traded position'] - 0.12/10000*np.abs(data_limit['traded position'])
data_limit['actual_return'] = data_limit['holding_return'] + data_limit['slippage']
data_limit['target position'].where(data_limit['target position'] != 0, None, inplace=True)
data_limit['target position'].fillna(method='ffill', inplace=True)
data_limit['theory_return'] = (data_limit['mid'] - data_limit['mid'].shift(1)) * data_limit['target position'].shift(1)

######################################################################################################

start = '2017-03-18 12:37:00'
end = '2017-03-21 20:45:00'
f,ax = plt.subplots()
# plt.suptitle(symbols[d])
ax.plot(data['actual_return'][start:end].cumsum())
ax.plot(data_limit['actual_return'][start:end].cumsum())
