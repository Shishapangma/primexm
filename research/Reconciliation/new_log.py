# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.fx_performance as fx
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt
import research.plot_functions.plot_functions as pf

import warnings
warnings.filterwarnings('ignore')
path = r'C:\Research\data\log_live_archive\historical data\26-Jul-2017\combine\\'
# path = 'C:\\Research\\data\\log\\'
all_actual_return = pd.DataFrame()
for d in range(4,5):
    files = os.listdir(path)
    data_ori = pd.read_csv(path + files[d], header=0)
    data = data_ori[np.logical_and(data_ori['event'] != 'One_Sec_Tick', np.logical_and(data_ori['event'] != 'event', data_ori['event'] != 'Tick'))]
    data.time = data.date.astype(str) + '-'+ data.time
    data.index = pd.to_datetime(data.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
    data[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
        data[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']].astype(float)
    del data['return']
    data = data['2017-06-06 00:00:00':]
    data['mid'] = 0.5 * data['bid'] + 0.5 * data['ask']
    data['holding_return'] = (data['mid'] - data['mid'].shift(1)) * data['current position'].shift(1)
    data['slippage'] = (data['mid'] - data['trade price']) * data['traded position']
    # data['trading_return'] = (data['mid'] - data['mid'].shift(1)) * data['traded position'].shift(1) / rate[d]
    data['actual_return'] = data['holding_return'] + data['slippage']
    data['target position'].where(data['target position'] != 0, None, inplace=True)
    data['target position'].fillna(method='ffill', inplace=True)
    data['target position'].where(data['current position'] != 0, 0, inplace=True)
    data['theory_return'] = (data['mid'] - data['mid'].shift(1)) * data['target position'].shift(1)

    f, (ax1, ax2, ax3) = plt.subplots(3,figsize=(22,10))
    f.suptitle(files[d][0:7], fontsize=14)
    bid = data['bid']
    ask = data['ask']
    mid = data['mid']
    trade_price = data['trade price']
    trade_price.where(trade_price !=0, np.nan, inplace = True)
    trade_price.where(data['event'] =='Order_Filled', np.nan, inplace = True)
    x = data.index
    trade_position = data['traded position']
    current_position = data['current position']
    target_position = data['target position']
    system_PnL = data['actual_return']
    theory_return = data['theory_return']
    actual_return = data['actual_return']
    trading_return = data['actual_return']
    holding_return = data['holding_return']
    slippage = data['slippage']

    pf.market_trade_monitor_plot(ax1, bid, ask, mid, trade_price, trade_position, x)
    pf.position_monitor_plot(ax2, target_position, current_position, trade_position, x)
    pf.peformance_monitor_plot(ax3, system_PnL, theory_return, actual_return, trading_return, holding_return, slippage)
    # summary stats
    avg_mid_price_sell = data[np.logical_and(data['trade price'].notnull(),data['traded position']<0)]['mid'].mean()
    avg_trade_price_sell = data[np.logical_and(data['trade price'].notnull(),data['traded position']<0)]['trade price'].mean()
    avg_mid_price_buy = data[np.logical_and(data['trade price'].notnull(),data['traded position']>0)]['mid'].mean()
    avg_trade_price_buy = data[np.logical_and(data['trade price'].notnull(),data['traded position']>0)]['trade price'].mean()
    avg_bid = data['bid'][data['target position'].notnull()].astype(float).mean()
    avg_ask = data['ask'][data['target position'].notnull()].astype(float).mean()

    sell_trade = data[np.logical_and(data['trade price'].notnull(),data['traded position']<0)][['mid','trade price','traded position']]
    execution_slippage_filled_sell_orders = (sell_trade['trade price'] - sell_trade['mid'])*sell_trade['traded position'].abs()
    buy_trade = data[np.logical_and(data['trade price'].notnull(),data['traded position']>0)][['mid','trade price','traded position']]
    execution_slippage_filled_buy_orders = (buy_trade['mid'] - buy_trade['trade price'])*buy_trade['traded position'].abs()
    # execution slippage

    print('average spread: ' + str(avg_ask - avg_bid))
    print('average spread by price: ' + str((avg_ask - avg_bid)/avg_mid_price_sell))
    print('average mid price for sell: ' + str(avg_mid_price_sell))
    print('average trade price for sell: ' + str(avg_trade_price_sell))
    print('average slippage for sell: ' + str(avg_trade_price_sell-avg_mid_price_sell))
    print('average mid price for buy: ' + str(avg_mid_price_buy))
    print('average trade price for buy: ' + str(avg_trade_price_buy))
    print('average slippage for buy: ' + str(avg_mid_price_buy-avg_trade_price_buy))
    print('sell slippage as percentage of spread: ' + str((avg_trade_price_sell-avg_mid_price_sell)/(avg_ask - avg_bid)))
    print('buy slippage as percentage of spread: ' + str((avg_mid_price_buy-avg_trade_price_buy)/(avg_ask - avg_bid)))
    print('total execution slippage for sell: ' + str(execution_slippage_filled_sell_orders.sum()))
    print('total execution slippage for buy: ' + str(execution_slippage_filled_buy_orders.sum()))
    print('total turnover: ' + str(data['traded position'].abs().sum()))

    # execution time
    order = data[np.logical_or(data['event']=='Order_Create',data['event']=='Order_Filled')]
    order['trade quantity'] = order['target position'] - order['current position']
    order['post_event'] = order.event.shift(-1)
    order['time'] = pd.to_datetime(order.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
    order['post_time'] = order.time.shift(-1)
    order['post_bid'] = order.bid.shift(-1)
    order['post_ask'] = order.ask.shift(-1)
    order['post_mid'] = order.mid.shift(-1)
    order['fill_status'] = np.nan
    order['fill_status'].where(order['post_event']==order['event'], 'filled', inplace = True)
    order['fill_status'].where(order['post_event']!=order['event'], 'canceled', inplace = True)
    order['time_fill'] = order.post_time-order.time
    canceled_order = order[order['fill_status']=='canceled']
    canceled_order_buy = canceled_order[canceled_order['trade quantity']>0]
    canceled_order_sell = canceled_order[canceled_order['trade quantity']<0]

    filled_order = order[np.logical_and(order['fill_status']=='filled',order['event']=='Order_Create')]
    filled_order_buy = filled_order[filled_order['trade quantity']>0]
    filled_order_sell = filled_order[filled_order['trade quantity']<0]

    time_slippage_canceled_sell_orders = (canceled_order_sell['post_mid']-canceled_order_sell['mid'])*canceled_order_sell['trade quantity'].abs()
    time_slippage_canceled_buy_orders = (canceled_order_buy['mid']-canceled_order_buy['post_mid'])*canceled_order_buy['trade quantity'].abs()

    time_slippage_filled_sell_orders = (filled_order_sell['post_mid']-filled_order_sell['mid'])*filled_order_sell['trade quantity'].abs()
    time_slippage_filled_buy_orders = (filled_order_buy['mid']-filled_order_buy['post_mid'])*filled_order_buy['trade quantity'].abs()

    print('total number of orders: ' + str(len(order[order['event'] == 'Order_Create'])))
    print('total canceled orders: ' + str(len(canceled_order)))
    print('canceled order percentage: ' + str(len(canceled_order)/len(order[order['event'] == 'Order_Create'])))
    print('average fill time: ' + str(filled_order.time_fill.mean()))
    print('total slippage for canceled sell orders: ' + str(time_slippage_canceled_sell_orders.sum()))
    print('total slippage for canceled buy orders: ' + str(time_slippage_canceled_buy_orders.sum()))
    print('total slippage for filled sell orders: ' + str(time_slippage_filled_sell_orders.sum()))
    print('total slippage for filled buy orders: ' + str(time_slippage_filled_buy_orders.sum()))

    # reconcile theory and actual returns
    total_cancel_time_slippage = time_slippage_canceled_sell_orders.sum() + time_slippage_canceled_buy_orders.sum()
    total_filled_time_slippage = + time_slippage_filled_sell_orders.sum() + time_slippage_filled_buy_orders.sum()
    total_execution_slippage = execution_slippage_filled_buy_orders.sum() + execution_slippage_filled_sell_orders.sum()
    total_slippage = total_cancel_time_slippage + total_filled_time_slippage + total_execution_slippage

    print('total_cancel_time_slippage: ' + str(total_cancel_time_slippage))
    print('total_filled_time_slippage: ' + str(total_filled_time_slippage))
    print('total_execution_slippage: ' + str(total_execution_slippage))
    print('total_slippage: ' + str(total_slippage))
    # print('total_trading return: ' + str(data['trading_return'].sum()))
    print('total difference between actual and theory (total slippage): ' + str(total_slippage))
    print('total difference between actual and theory (actual - theory): ' + str((data['actual_return'].sum()-data['theory_return'].sum())))
    print('total slippage percentage: ' + str(((data['actual_return'].sum()-data['theory_return'].sum()))/(data['traded position'].abs().sum()*avg_bid)))
    print('total return: ' + str((data['actual_return'].sum())))

    actual_return_resample = actual_return.resample('s').sum()
    actual_return_resample.where(actual_return_resample.notnull(),0,inplace = True)
    if all_actual_return.empty:
        all_actual_return = pd.DataFrame(actual_return_resample)
    else:
        all_actual_return = pd.merge(all_actual_return, pd.DataFrame(actual_return_resample), how='left', left_index=True, right_index=True)






