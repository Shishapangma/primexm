import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



path = 'C:\\Research\\data\\log_live_archive\\historical data\\18-Mar-2017\\'
files = os.listdir(path)
path_oanda = 'C:\\Research\\data\\Oanda_execution_report\\'
files_oanda = os.listdir(path_oanda)
symbol = ['AUD/USD','EUR/CHF']
date = ['2017-03-12', '2017-03-13','2017-03-14','2017-03-15','2017-03-16','2017-03-17','2017-03-18']

all = pd.DataFrame()

for oanda_file in range(0, len(files_oanda)):
    for log_number in range(0,len(symbol)):
        start_period = date[oanda_file] +' 21:00:00'
        end_period =  date[oanda_file+1] +' 21:00:00'
        oanda_symbol = symbol[log_number]

        data_ori = pd.read_csv(path + files[log_number], header=0)
        data = data_ori[np.logical_and(data_ori['event'] != 'event', data_ori['event'] != 'Tick')]
        data.index = pd.to_datetime(data.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
        data[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
            data[
                ['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']].astype(
                float)
        del data['return']
        data = data[start_period:end_period]
        data['mid'] = 0.5 * data['bid'] + 0.5 * data['ask']
        data['holding_return'] = (data['mid'] - data['mid'].shift(1)) * data['current position'].shift(1)
        data['slippage'] = (data['mid'] - data['trade price']) * data['traded position']
        # data['trading_return'] = (data['mid'] - data['mid'].shift(1)) * data['traded position'].shift(1) / rate[d]
        data['actual_return'] = data['holding_return'] + data['slippage']

        print(data['actual_return'].sum())
        oanda_data = pd.read_csv(path_oanda + files_oanda[oanda_file], header=0)
        oanda_data_single = oanda_data[oanda_data['Symbol'] == oanda_symbol]
        PnL = oanda_data_single[oanda_data_single['Side']=='Sell']['Amount'].sum()-oanda_data_single[oanda_data_single['Side']=='Buy']['Amount'].sum()
        print(PnL)
        all = pd.concat([all,pd.DataFrame([oanda_symbol, data['actual_return'].sum(), PnL], index = ['symbol','log','oanda'], columns =[start_period]).T])
all.to_clipboard()

