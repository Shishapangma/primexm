import pandas as pd
import numpy as np

a = pd.read_html(r'C:\Users\Owner\AppData\Roaming\MetaQuotes\Terminal\3212703ED955F10C7534BE8497B221F4\DetailedStatement1_8.htm', skiprows =2, header = 0)

a = a[0]
a = a[np.logical_or(a['Type']=='buy',a['Type']=='sell')]
del a['S / L']
del a['T / P']
del a['Commission']
del a['Taxes']
del a['Swap']
del a['Ticket']
a['Size'] = a['Size'].astype('float')
a['Profit'] = [x.strip().replace(' ', '') for x in a['Profit']]
a['Profit'] = a['Profit'].astype(float)

spread = pd.DataFrame([['audjpy', 'audcad', 'eurgbp', 'eurusd', 'usdjpy', 'audusd', 'audnzd', 'nzdusd'],[2.1/2, 2.4/2, 1.4/2, 1.3/2, 1.6/2, 1.5/2, 3.8/2, 1.6/2]]).T
spread.columns = ['pair', 'spread']
a = pd.merge(a,spread, how='left', left_on='Item', right_on='pair')
a['rebate'] = a['spread']*a['Size']*10
a['adj_profit'] = a['Profit'] + a['rebate']
a.index = pd.to_datetime(a['Open Time'])
a = a.sort_index(axis=0)
a['Profit'][a['Item']=='audcad'].cumsum().plot()

