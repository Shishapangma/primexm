# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.fx_performance as fx
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt
import research.plot_functions.plot_functions as pf

import warnings

warnings.filterwarnings('ignore')
path = 'C:\\Research\\data\\log\\'
all_actual_return = pd.DataFrame()
for d in range(2,3):
    files = os.listdir(path)
    rate = [1, 1, 1, 1, 1, 1]
    data_ori = pd.read_csv(path + files[d], header=0)
    data = data_ori[np.logical_and(data_ori['event'] != 'One_Sec_Tick',
                                   np.logical_and(data_ori['event'] != 'event', data_ori['event'] != 'One_Sec_Tick'))]
    data.index = pd.to_datetime(data.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
    data = data.sort_index()
    data[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
        data[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price',
              'trade price']].astype(float)
    del data['return']
    data = data['2017-03-16 23:00:00':]
    data['mid'] = np.round(0.5 * data['bid'] + 0.5 * data['ask'], 5)
    data['target position'].where(data['target position'] != 0, None, inplace=True)
    data['target position'].fillna(method='ffill', inplace=True)
    data['current position'].where(data['current position'] != 0, None, inplace=True)
    data['current position'].fillna(method='ffill', inplace=True)
    data['holding_return'] = (data['mid'] - data['mid'].shift(1)) * data['current position'].shift(1) / rate[d]
    data['slippage'] = (data['mid'] - data['trade price']) * data['traded position'] / rate[d]
    # data['trading_return'] = (data['mid'] - data['mid'].shift(1)) * data['traded position'].shift(1) / rate[d]
    data['actual_return'] = data['holding_return'] + data['slippage']
    data['theory_return'] = (data['mid'] - data['mid'].shift(1)) * data['target position'].shift(1) / rate[d]

    OneMinute = data[data['event'] == 'One_Min_Tick']
    OneMinute['bid_high'] = np.nan
    OneMinute['ask_low'] = np.nan
    for j in range(0, len(OneMinute)):
        OneMinute['bid_high'][j] = data[OneMinute.index[j]:OneMinute.index[j + 1]]['bid'].max()
        OneMinute['ask_low'][j] = data[OneMinute.index[j]:OneMinute.index[j + 1]]['ask'].min()

    ret = OneMinute.mid.pct_change()
    alpha = AlphaSignal()
    transaction_cost = TransactionCostModel(kappa=2)
    One_fx_Strategy_limit = FxStrategy(alpha, transaction_cost)
    w_all_limit = [0]
    fill = [0]
    tick_incremental = -0.00005
    for j in range(1, len(OneMinute)):
        alpha_scale = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[j].hour, minute=ret.index[j].minute)
        alpha.get_simple_reversion_alpha(return_t=ret.ix[j], short_ema_pre=alpha.short_ema_t,
                                         long_ema_pre=alpha.long_ema_t,
                                         sigma_ema_pre=alpha.sigma_ema_t, alpha_scale=alpha_scale)
        transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=0, tc=[0], price_t=OneMinute.mid.ix[j],
                                                                       commission=0)
        One_fx_Strategy_limit.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy_limit.target_weight_t)

        One_fx_Strategy_limit.get_fill_simulated_weight_tick(weight_target=OneMinute['target position'][j],
                                                             weight_current=w_all_limit[-1],
                                                             buy_price_t=OneMinute.mid[j] + tick_incremental,
                                                             sell_price_t=OneMinute.mid[j] - tick_incremental,
                                                             ask_low_t1=OneMinute['ask_low'].ix[j],
                                                             bid_high_t1=OneMinute['bid_high'].ix[j])
        w_all_limit.append(One_fx_Strategy_limit.target_weight_t)
        fill.append(One_fx_Strategy_limit.filled)

w_all_limit = pd.DataFrame(w_all_limit, index=OneMinute.index, columns=['Close'])

w_all_limit.to_clipboard()