# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.fx_performance as fx
import research.PerformanceMeasures as pm
all = pd.DataFrame()
TC = [1.1, 1.3, 0.5, 0.8, 0.4]
for d in range(1,2):
    tick_incremental = 0.000
    files = os.listdir(r'C:\Research\data\MT4_download_dec_2016')
    data = pd.read_csv('C:\\Research\\data\\MT4_download_dec_2016\\' + files[d], header=0)

    data.columns = [['date','time','open','high','low','close','volume']]

    data.index = pd.to_datetime(data['date'] +' ' + data['time'], format="%Y.%m.%d %H:%M")
    p = data['close']
    p = p[np.logical_and(np.logical_or(p.index.weekday != 4, p.index.hour < 21),
            np.logical_and(p.index.weekday != 5,
            np.logical_or(p.index.weekday != 6, p.index.hour >= 21)))]
    p = p[~np.logical_and(p.index.hour == 0, p.index.minute < 3)]
    ret = p.pct_change()
    hr = p.index.hour

    w_all = [0]
    alpha_sig = [0]
    tc_all = [0]
    alpha = AlphaSignal()
    transaction_cost = TransactionCostModel(kappa=2/10*3)
    One_fx_Strategy = FxStrategy(alpha, transaction_cost)

    for i in range(1, len(ret)):
        alpha_scale = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour,
                                                                 minute=ret.index[i].minute,
                                                                 scale_factor=3,
                                                                 overnight_hour_pre=21+2,
                                                                 overnight_hour_post=0,
                                                                 decay_half_life_pre=10,
                                                                 decay_half_life_post=10,
                                                                 cut_off_minute_pre=45,
                                                                 cut_off_minute_post=15)
        alpha.get_simple_reversion_alpha(return_t=ret.ix[i],
                                         short_ema_pre=alpha.short_ema_t,
                                         long_ema_pre=alpha.long_ema_t,
                                         sigma_ema_pre=alpha.sigma_ema_t,
                                         short_half_life=20,
                                         long_half_life=200,
                                         long_half_life_risk=200,
                                         alpha_winsorize=3,
                                         alpha_scale=alpha_scale)
        transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=ret.index[i].hour, TC_by_hour=TC[d],  price_t=p.ix[i], commission=1)
        One_fx_Strategy.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy.target_weight_t)
        w_all.append(One_fx_Strategy.target_weight_t)
        alpha_sig.append(alpha.alpha_signal_t)
        tc_all.append(transaction_cost.tc)

    w = pd.DataFrame(w_all, index=ret.index, columns=['Close'])
    alpha_sig = pd.DataFrame(alpha_sig, index=ret.index, columns=['Close'])
    ret = pd.DataFrame(ret, index=ret.index)
    tc = pd.DataFrame(tc_all, index=ret.index)
    strategy_perf = fx.PerformanceCalc(w, ret)
    strategy_perf.get_performance_stats((tc[1:].values)/10000)
    strategy_perf.show_performance_stats()
    strategy_perf.store_performance_stats()

    pd.DataFrame(strategy_perf._net_ret,index=ret[1:].index).cumsum().plot()
    all = pd.concat([all, pd.DataFrame(strategy_perf._net_ret,index=ret[1:].index)], axis=1)

# pd.DataFrame(strategy_perf._net_ret,index=ret[1:].index).groupby(by=ret[1:].index.hour).sum().plot(kind='bar')

all.columns = ['audcad','audnzd','audusd','eurchf','eurusd']

pd.DataFrame(strategy_perf._net_ret,index=ret[1:].index)['2017-02-12':].cumsum().plot()

w['2017-02-15 22:00:00':'2017-02-17 22:00:00'].plot()