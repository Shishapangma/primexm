# test code
from Research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import Research.fx_performance as fx
import Research.PerformanceMeasures as pm

#USDJPY
tc = {0: 0.69, 1: 0.58, 2: 0.54, 3: 0.54, 4: 0.54, 5: 0.54, 6: 0.57, 7: 0.55, 8: 0.48,
      9: 0.56, 10: 0.48, 11: 0.51, 12: 0.49, 13: 0.49, 14: 0.47, 15: 0.49, 16: 0.46, 17: 0.46, 18: 0.48, 19: 0.48, 20: 0.49, 21: 0.68, 22: 1.98,
      23: 0.98}

d = 3
files = os.listdir(r'C:\Research\data\log')
data = pd.read_csv('C:\\Research\\data\\log\\' + files[d], header=0)
data.columns = ['time', 'bid', 'ask', 'current weight', 'target weight',
                      'bid return', 'test current weight', 'test target weight', 'standard TC']
data.index = pd.to_datetime(data['time'])

p = data['bid']
p = p[np.logical_and(np.logical_or(p.index.weekday != 4, p.index.hour < 21),
        np.logical_and(p.index.weekday != 5,
        np.logical_or(p.index.weekday != 6, p.index.hour >= 21)))]
ret = p.pct_change()
hr = p.index.hour

w_all = []
alpha_sig = []
tc_all = []
alpha = AlphaSignal()
transaction_cost = TransactionCostModel(kappa=2)
One_fx_Strategy = FxStrategy(alpha, transaction_cost)

for i in range(0, len(ret)):
    alpha.get_simple_reversion_alpha(return_t=ret.ix[i],
                                     short_ema_pre=alpha.short_ema_t,
                                     long_ema_pre=alpha.long_ema_t,
                                     sigma_ema_pre=alpha.sigma_ema_t,
                                     short_half_life=20,
                                     long_half_life=200,
                                     long_half_life_risk=200,
                                     alpha_winsorize=10,
                                     alpha_scale=10)
    transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=hr[i], TC_by_hour=tc,  price_t=p.ix[i], commission=0)
    One_fx_Strategy.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy.target_weight_t)
    w_all.append(One_fx_Strategy.target_weight_t)
    alpha_sig.append(alpha.alpha_signal_t)
    tc_all.append(transaction_cost.tc)

w = pd.DataFrame(data['target weight'].values.astype(float), index=ret.index, columns=['Close'])
alpha_sig = pd.DataFrame(alpha_sig, index=ret.index, columns=['Close'])
ret = pd.DataFrame(ret, index=ret.index)
tc_actual = pd.DataFrame(tc_all, index=ret.index)
tc_demo = pd.DataFrame((data['ask'] - data['bid'])/data['bid']/2)
strategy_perf = fx.PerformanceCalc(w, ret)
strategy_perf.get_performance_stats(tc_actual[1:].values/10000)
strategy_perf.show_performance_stats()
strategy_perf.store_performance_stats()


a = pd.DataFrame(strategy_perf._net_ret,index=ret[1:].index).cumsum()
b = pd.DataFrame(strategy_perf._net_ret,index=ret[1:].index).cumsum()
pd.concat([a,b],axis=1).plot()
pd.DataFrame(strategy_perf._net_ret,index=ret[1:].index).groupby(by=ret[1:].index.hour).sum().plot(kind='bar')

