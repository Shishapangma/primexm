import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf
d = 8
path = r'C:\Users\Owner\Google Drive\GE Tick\26-28-Jul-2017\\'
files = os.listdir(path)
data_ori = pd.read_csv(path + files[d], header=0)
data = data_ori[data_ori['event'] == 'Dummy_Tick']
data.time = data.date.astype(str) + '-' + data.time
data.index = pd.to_datetime(data.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
data[['bid', 'ask']] = data[['bid', 'ask']].astype(float)
data['spread'] = data['ask'] - data['bid']
#
data['spread'].where(data['spread'] > data['spread'].quantile(0.01), None, inplace = True)
data['spread'].where(data['spread'] < data['spread'].quantile(0.99), None, inplace = True)
# (data['spread'].groupby(data.index.hour).mean()*10000).plot(kind='bar')
#
gp = (data['spread'].groupby(by=[data.index.date,data.index.hour]).mean())
gp.plot(kind='bar')

###########################################################################################
# LMAX data
d= 4
path = r'C:\Research\data\log_live_archive\historical data\29-Jul-2017\tick\combine\\'
files = os.listdir(path)
data_ori = pd.read_csv(path + files[d], header=0)
data = data_ori[data_ori['event'] == 'Dummy_Tick']
data.time = data.date.astype(str) + '-' + data.time
data.index = pd.to_datetime(data.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
data[['bid', 'ask']] = data[['bid', 'ask']].astype(float)
data['spread'] = data['ask'] - data['bid']

data['spread'].where(data['spread'] > data['spread'].quantile(0.01), None, inplace = True)
data['spread'].where(data['spread'] < data['spread'].quantile(0.99), None, inplace = True)
(data['spread'].groupby(data.index.hour).mean()*10000).plot(kind='bar')

lmax = (data['spread'].groupby(data.index.hour).mean())

a = pd.concat([gp, lmax],axis=1)
a.columns = [['gp','lmax']]
a.to_clipboard()