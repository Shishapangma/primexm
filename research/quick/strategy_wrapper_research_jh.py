import pandas as pd
import numpy as np
import production.stat_functions as sf
import math
import datetime


class AlphaSignal(object):
    def __init__(self):
        self.alpha_signal_t = 0
        self.short_ema_t = 0
        self.long_ema_t = 0
        self.sigma_ema_t = 0
        self.long_ema_t_con = 0
        self.short_ema_t_con = 0
        self.sigma_ema_t_con = 0
        self.sigma_ema_mean_pre_t = 0
        self.sigma_ema_long_pre_t = 0
        self.sigma_ema_short_pre_t = 0

    def get_simple_reversion_alpha(self, return_t, short_ema_pre, long_ema_pre, sigma_ema_pre, short_half_life=20,
                                   long_half_life=200, long_half_life_risk=200, alpha_winsorize=10, alpha_scale=10):

        self.short_ema_t = return_t * (
            1 - sf.get_exponential_weight(short_half_life)) + short_ema_pre * sf.get_exponential_weight(short_half_life)

        self.long_ema_t = return_t * (
            1 - sf.get_exponential_weight(long_half_life)) + long_ema_pre * sf.get_exponential_weight(long_half_life)

        self.sigma_ema_t = np.sqrt(sf.get_exponential_weight(long_half_life_risk) * (
            sigma_ema_pre ** 2 + (1 - sf.get_exponential_weight(long_half_life_risk)) * (
                (return_t - long_ema_pre) ** 2)))

        self.alpha_signal_t = (self.long_ema_t - self.short_ema_t) / self.sigma_ema_t * alpha_scale

        if math.isnan(self.alpha_signal_t):
            self.alpha_signal_t = 0
            self.short_ema_t = 0
            self.long_ema_t = 0
            self.sigma_ema_t = 0

        if self.alpha_signal_t > alpha_winsorize:
            self.alpha_signal_t = alpha_winsorize

        if self.alpha_signal_t < -alpha_winsorize:
            self.alpha_signal_t = -alpha_winsorize

        return self.alpha_signal_t, self.short_ema_t, self.long_ema_t, self.sigma_ema_t

    def get_reversion_alpha_combined(self, return_t, conditional_alpha_t, short_ema_pre_ret, long_ema_pre_ret,
                                     sigma_ema_pre_ret, short_ema_pre_con, long_ema_pre_con, sigma_ema_pre_con,
                                     short_half_life=20, long_half_life=200, long_half_life_risk=200,
                                     short_half_life_condition=20, long_half_life_condition=200,
                                     alpha_winsorize1=10, alpha_scale1=10, alpha_winsorize2=3, alpha_scale2=3):

        self.short_ema_t = return_t * (
        1 - sf.get_exponential_weight(short_half_life)) + short_ema_pre_ret * sf.get_exponential_weight(
                short_half_life)

        self.long_ema_t = return_t * (
        1 - sf.get_exponential_weight(long_half_life)) + long_ema_pre_ret * sf.get_exponential_weight(
                long_half_life)

        self.sigma_ema_t = np.sqrt(sf.get_exponential_weight(long_half_life_risk) * (
            sigma_ema_pre_ret ** 2 + (1 - sf.get_exponential_weight(long_half_life_risk)) * (
                (return_t - long_ema_pre_ret) ** 2)))

        self.short_ema_t_con = conditional_alpha_t * (
        1 - sf.get_exponential_weight(short_half_life_condition)) + short_ema_pre_con * sf.get_exponential_weight(
                short_half_life_condition)

        self.long_ema_t_con = conditional_alpha_t * (
        1 - sf.get_exponential_weight(long_half_life_condition)) + long_ema_pre_con * sf.get_exponential_weight(
                long_half_life_condition)

        self.sigma_ema_t_con = np.sqrt(sf.get_exponential_weight(long_half_life_risk) * (
            sigma_ema_pre_con ** 2 + (1 - sf.get_exponential_weight(long_half_life_risk)) * (
                (conditional_alpha_t - long_ema_pre_con) ** 2)))

        alpha1 = (self.long_ema_t - self.short_ema_t) / self.sigma_ema_t * alpha_scale1
        alpha2 = (self.short_ema_t_con - self.long_ema_t_con) / self.sigma_ema_t_con * alpha_scale2

        if alpha1 > alpha_winsorize1:
            alpha1 = alpha_winsorize1

        if alpha1 < -alpha_winsorize1:
            alpha1 = -alpha_winsorize1

        if alpha2 > alpha_winsorize1:
            alpha2 = alpha_winsorize1

        if alpha2 < -alpha_winsorize2:
            alpha2 = -alpha_winsorize2

        self.alpha_signal_t = (alpha1 + alpha2) / 1.2

        if math.isnan(self.alpha_signal_t):
            self.alpha_signal_t = 0
            self.short_ema_t = 0
            self.long_ema_t = 0
            self.sigma_ema_t = 0
            self.long_ema_t_con = 0
            self.short_ema_t_con = 0
            self.sigma_ema_t_con = 0

        return self.alpha_signal_t, self.short_ema_t, self.long_ema_t, self.sigma_ema_t, self.long_ema_t_con, \
               self.short_ema_t_con, self.sigma_ema_t_con

    def get_zscore_reversion_alpha(self, return_t, short_ema_pre, long_ema_pre, sigma_ema_pre, sigma_ema_mean_pre,
                                   sigma_ema_long_pre, sigma_ema_short_pre, short_half_life=20, long_half_life=200,
                                   short_half_life_sigma=10, long_half_life_sigma=400, long_half_life_risk=40,
                                   alpha_winsorize=5,
                                   alpha_scale=2):

        self.short_ema_t = return_t * (
            1 - sf.get_exponential_weight(short_half_life)) + short_ema_pre * sf.get_exponential_weight(short_half_life)

        self.long_ema_t = return_t * (
            1 - sf.get_exponential_weight(long_half_life)) + long_ema_pre * sf.get_exponential_weight(long_half_life)

        self.sigma_ema_short_pre_t = return_t * (
            1 - sf.get_exponential_weight(short_half_life_sigma)) + sigma_ema_short_pre * sf.get_exponential_weight(
            short_half_life_sigma)

        self.sigma_ema_long_pre_t = return_t * (
            1 - sf.get_exponential_weight(long_half_life_sigma)) + sigma_ema_long_pre * sf.get_exponential_weight(
            long_half_life_sigma)

        self.sigma_ema_mean_pre_t = (self.sigma_ema_long_pre_t - self.sigma_ema_short_pre_t) * (
            1 - sf.get_exponential_weight(long_half_life_risk)) + sigma_ema_mean_pre * sf.get_exponential_weight(
            long_half_life_risk)

        self.sigma_ema_t = np.sqrt(sf.get_exponential_weight(long_half_life_risk) * (
            sigma_ema_pre ** 2 + (1 - sf.get_exponential_weight(long_half_life_risk)) * (
                ((self.sigma_ema_long_pre_t - self.sigma_ema_short_pre_t) - sigma_ema_mean_pre) ** 2)))

        self.alpha_signal_t = (self.long_ema_t - self.short_ema_t) / self.sigma_ema_t * alpha_scale

        if math.isnan(self.alpha_signal_t):
            self.alpha_signal_t = 0
            self.short_ema_t = 0
            self.long_ema_t = 0
            self.sigma_ema_t = 0
            self.sigma_ema_mean_pre_t = 0
            self.sigma_ema_long_pre_t = 0
            self.sigma_ema_short_pre_t = 0

        if self.alpha_signal_t > alpha_winsorize:
            self.alpha_signal_t = alpha_winsorize

        if self.alpha_signal_t < -alpha_winsorize:
            self.alpha_signal_t = -alpha_winsorize

        return self.alpha_signal_t, self.short_ema_t, self.long_ema_t, self.sigma_ema_t, self.sigma_ema_mean_pre_t, self.sigma_ema_long_pre_t, self.sigma_ema_short_pre_t

    def get_reversion_alpha_conditional(self, return_t, conditional_alpha_t, short_ema_pre_ret, long_ema_pre_ret,
                                        sigma_ema_pre_ret,
                                        short_ema_pre_con, long_ema_pre_con, sigma_ema_pre_con,
                                        short_half_life=20, long_half_life=200, long_half_life_risk=200,
                                        short_half_life_condition=20, long_half_life_condition=200,
                                        alpha_winsorize=5, alpha_scale=30):

        self.short_ema_t = return_t * (
            1 - sf.get_exponential_weight(short_half_life)) + short_ema_pre_ret * sf.get_exponential_weight(
                short_half_life)

        self.long_ema_t = return_t * (
            1 - sf.get_exponential_weight(long_half_life)) + long_ema_pre_ret * sf.get_exponential_weight(
                long_half_life)

        self.sigma_ema_t = np.sqrt(sf.get_exponential_weight(long_half_life_risk) * (
            sigma_ema_pre_ret ** 2 + (1 - sf.get_exponential_weight(long_half_life_risk)) * (
                (return_t - long_ema_pre_ret) ** 2)))

        self.short_ema_t_con = conditional_alpha_t * (
            1 - sf.get_exponential_weight(short_half_life_condition)) + short_ema_pre_con * sf.get_exponential_weight(
                short_half_life_condition)

        self.long_ema_t_con = conditional_alpha_t * (
            1 - sf.get_exponential_weight(long_half_life_condition)) + long_ema_pre_con * sf.get_exponential_weight(
                long_half_life_condition)

        self.sigma_ema_t_con = np.sqrt(sf.get_exponential_weight(long_half_life_risk) * (
            sigma_ema_pre_con ** 2 + (1 - sf.get_exponential_weight(long_half_life_risk)) * (
                (conditional_alpha_t - long_ema_pre_con) ** 2)))

        self.alpha_signal_t = (self.long_ema_t - self.short_ema_t) / self.sigma_ema_t * \
                              (0.5 + 0.5 * np.tanh((
                                                       self.long_ema_t_con - self.short_ema_t_con) / self.sigma_ema_t_con)) * alpha_scale

        if math.isnan(self.alpha_signal_t):
            self.alpha_signal_t = 0
            self.short_ema_t = 0
            self.long_ema_t = 0
            self.sigma_ema_t = 0
            self.long_ema_t_con = 0
            self.short_ema_t_con = 0
            self.sigma_ema_t_con = 0

        if self.alpha_signal_t > alpha_winsorize:
            self.alpha_signal_t = alpha_winsorize

        if self.alpha_signal_t < -alpha_winsorize:
            self.alpha_signal_t = -alpha_winsorize

        return self.alpha_signal_t, self.short_ema_t, self.long_ema_t, self.sigma_ema_t, self.long_ema_t_con, \
               self.short_ema_t_con, self.sigma_ema_t_con

    def get_alpha_scale_decay_for_overnight_hour(self, hour, hour_select=False, scale_factor=10):
        alpha_scale = scale_factor
        if hour in hour_select:
            alpha_scale = 0
        return alpha_scale

    def get_alpha_scale_decay_for_overnight_hour1(self, time_index, scale_factor=10, hour_select_ao=False,
                                                  hour_select_oa=False, decay_half_life_pre=10, decay_half_life_post=10,
                                                  minute_start_decay=30, minute_post_full=30, cut_off_minute_pre=5,
                                                  cut_off_minute_post=5):
        alpha_scale = scale_factor

        if time_index.month in [4, 5, 6, 7, 8, 9]:
            hour_select = hour_select_ao
        else:
            hour_select = hour_select_oa

        hour_select_pre = []
        for i in range(0, len(hour_select)):
            hour_select_time = datetime.datetime.strptime(hour_select[i], '%H:%M:%S').time()
            # if time_index.hour == 23:
            #     hour_select_pre_time = (datetime.datetime.combine(time_index.date(), hour_select_time) + datetime.timedelta(hours=24) - datetime.timedelta(minutes=minute_start_decay))
            # else:
            #     hour_select_pre_time = (datetime.datetime.combine(time_index.date(), hour_select_time) - datetime.timedelta(minutes=minute_start_decay))
            #
            hour_select_pre_time = (datetime.datetime.combine(time_index.date(), hour_select_time) - datetime.timedelta(minutes=minute_start_decay))
            hour_select_pre.append(hour_select_pre_time)

        hour_select_pre_finish = []
        for i in range(0, len(hour_select)):
            hour_select_time = datetime.datetime.strptime(hour_select[i], '%H:%M:%S').time()
            # if time_index.hour == 23:
            #     hour_select_pre_finish_time = (datetime.datetime.combine(time_index.date(), hour_select_time) + datetime.timedelta(hours=24) - datetime.timedelta(minutes=cut_off_minute_pre))
            # else:
            hour_select_pre_finish_time = (datetime.datetime.combine(time_index.date(), hour_select_time) - datetime.timedelta(minutes=cut_off_minute_pre))
            hour_select_pre_finish.append(hour_select_pre_finish_time)

        hour_select_post_begin = []
        for i in range(0, len(hour_select)):
            hour_select_time = datetime.datetime.strptime(hour_select[i], '%H:%M:%S').time()
            hour_select_post_begin_time = (datetime.datetime.combine(time_index.date(), hour_select_time) + datetime.timedelta(minutes=cut_off_minute_post+60))
            hour_select_post_begin.append(hour_select_post_begin_time)

        hour_select_post = []
        for i in range(0, len(hour_select)):
            hour_select_time = datetime.datetime.strptime(hour_select[i], '%H:%M:%S').time()
            hour_select_post_time = (datetime.datetime.combine(time_index.date(), hour_select_time) + datetime.timedelta(minutes=minute_post_full+60))
            hour_select_post.append(hour_select_post_time)

        for i in range(0, len(hour_select)):
            if np.logical_and(time_index>hour_select_pre[i],time_index<hour_select_pre_finish[i]):
                minute_pass = (time_index - hour_select_pre[i]).total_seconds()/60
                alpha_scale = sf.get_exponential_weight(decay_half_life_pre) ** minute_pass * scale_factor

        for i in range(0, len(hour_select)):
            if np.logical_and(time_index>hour_select_post_begin[i],time_index<hour_select_post[i]):
                minute_pass = (time_index - hour_select_post_begin[i]).total_seconds()/60
                alpha_scale = (1 - (sf.get_exponential_weight(decay_half_life_post) ** minute_pass)) * scale_factor

        return alpha_scale

    def get_alpha_scale_decay_for_overnight_hour2(self, time_index, scale_factor=10, hour_select_ao=False,
                                                  hour_select_oa=False, cut_off_minute_pre=5, cut_off_minute_post=5):
        alpha_scale = scale_factor
        if time_index.month in [4, 5, 6, 7, 8, 9]:
            hour_select = hour_select_ao
        else:
            hour_select = hour_select_oa

        hour_select_begin = []
        for i in range(0, len(hour_select)):
            hour_select_time = datetime.datetime.strptime(hour_select[i], '%H:%M:%S').time()
            # if time_index.hour == 23:
            #     hour_select_begin_time = (datetime.datetime.combine(time_index.date(), hour_select_time) + datetime.timedelta(hours=24) - datetime.timedelta(minutes=cut_off_minute_pre))
            # else:
            hour_select_begin_time = (datetime.datetime.combine(time_index.date(), hour_select_time) - datetime.timedelta(minutes=cut_off_minute_pre))
            hour_select_begin.append(hour_select_begin_time)

        hour_select_finish = []
        for i in range(0, len(hour_select)):
            hour_select_time = datetime.datetime.strptime(hour_select[i], '%H:%M:%S').time()
            hour_select_finish_time = (datetime.datetime.combine(time_index.date(), hour_select_time) + datetime.timedelta(minutes=60+cut_off_minute_post))
            hour_select_finish.append(hour_select_finish_time)

        for i in range(0, len(hour_select)):
            if np.logical_and(time_index>hour_select_begin[i],time_index<hour_select_finish[i]):
                alpha_scale = 0

        return alpha_scale

class TransactionCostModel(object):
    def __init__(self, kappa):
        self.tc = np.nan
        self.kappa = kappa

    def get_simple_linear_transaction_cost_market_based(self, bid_pre_avg, ask_pre_avg, commission):
        self.tc = ((ask_pre_avg - bid_pre_avg) / (bid_pre_avg + ask_pre_avg) + commission) * 10000
        return self.tc

    def get_simple_linear_transaction_cost_hour_based(self, hour, tc, price_t, commission, spread):
        if spread != -100:
            self.tc = ((spread / 2) / price_t + commission/10000) * 10000
        else:
            self.tc = (tc[hour] / 2) / price_t + commission
        return self.tc


class FillSimulator(object):
    def __init__(self):
        self.is_rebalance = False
        self.trade_quantity = 0
        self.side = 0
        self.order_price = 0
        self.current_position_t = 0
        self.cancelled = 0

    def get_fill_simulated_position(self, isrebalance, target_position_t, target_position_tm1, current_position_tm1,
                                    tick, ask_t, bid_t):

        self.is_rebalance = isrebalance
        if self.is_rebalance:
            self.trade_quantity = target_position_t - current_position_tm1
            self.side = np.sign(self.trade_quantity)
            self.order_price = np.round((ask_t + bid_t) / 2, 5) - self.side * tick
            if current_position_tm1 != target_position_tm1:
                self.cancelled += 1

        elif np.logical_and(~self.is_rebalance, target_position_t != current_position_tm1):
            if np.logical_and(self.side == 1, self.order_price >= ask_t):
                self.current_position_t = current_position_tm1 + self.trade_quantity
                self.cancelled = 0
            elif np.logical_and(self.side == -1, self.order_price <= bid_t):
                self.current_position_t = current_position_tm1 + self.trade_quantity
                self.cancelled = 0
            else:
                self.current_position_t = current_position_tm1

        else:
            self.current_position_t = current_position_tm1


class FxStrategy(object):
    def __init__(self, alpha_model, transaction_cost_model):

        # Load objects
        self.alpha = alpha_model
        self.transaction_cost = transaction_cost_model
        self.target_weight_t = 0
        self.buy_price_stop = 0
        self.sell_price_stop = 0
        self.lock = False
        self.filled = 0
        self.ismktorder = 0

    def get_weight_simple_alpha_with_tc(self, weight_current):

        if self.alpha.alpha_signal_t > (weight_current + self.transaction_cost.tc * self.transaction_cost.kappa):
            self.target_weight_t = self.alpha.alpha_signal_t - self.transaction_cost.tc * self.transaction_cost.kappa
        elif self.alpha.alpha_signal_t < (weight_current - self.transaction_cost.tc * self.transaction_cost.kappa):
            self.target_weight_t = self.alpha.alpha_signal_t + self.transaction_cost.tc * self.transaction_cost.kappa
        else:
            self.target_weight_t = weight_current

        return self.target_weight_t

    def get_fill_simulated_weight_mkt_order(self, weight_target, weight_current, buy_price_t, sell_price_t, ask_low_t1,
                                            bid_high_t1):

        if np.logical_and(weight_target - weight_current > 0, buy_price_t >= ask_low_t1):
            self.target_weight_t = weight_target
            self.filled = 1
            self.ismktorder = 0
        elif np.logical_and(weight_target - weight_current < 0, sell_price_t <= bid_high_t1):
            self.target_weight_t = weight_target
            self.filled = 1
            self.ismktorder = 0
        elif self.target_weight_t == weight_current:
            self.filled = 2
            self.ismktorder = 0
        else:
            self.target_weight_t = weight_target
            self.filled = 0
            self.ismktorder = 1

        return self.target_weight_t, self.filled

    def get_fill_simulated_weight_tick(self, weight_target, weight_current, buy_price_t, sell_price_t, ask_low_t1,
                                       bid_high_t1):

        if np.logical_and(weight_target - weight_current > 0, buy_price_t >= ask_low_t1):
            self.target_weight_t = weight_target
            self.filled = 1
        elif np.logical_and(weight_target - weight_current < 0, sell_price_t <= bid_high_t1):
            self.target_weight_t = weight_target
            self.filled = 1
        elif self.target_weight_t == weight_current:
            self.filled = 2
        else:
            self.target_weight_t = weight_current
            self.filled = 0

        return self.target_weight_t, self.filled

    def get_stop_loss_weight(self, weight_current, price_current, weight_threshold=0.5, stop_loss_pip=100):

        if np.logical_and(self.target_weight_t > 0, weight_current <= 0):
            status = 'first_long'
        elif np.logical_and(self.target_weight_t > 0, weight_current > 0):
            status = 'long'

        elif np.logical_and(self.target_weight_t < 0, weight_current >= 0):
            status = 'first_short'

        elif np.logical_and(self.target_weight_t < 0, weight_current < 0):
            status = 'short'

        else:
            status = 'unknown'

        if status == 'first_long':
            self.buy_price_stop = price_current - stop_loss_pip / 10000
        if status == 'first_short':
            self.sell_price_stop = price_current + stop_loss_pip / 10000

        if np.logical_or(self.lock, np.logical_and(status == 'long', price_current < self.buy_price_stop)):
            print('STOP LONG!!!')
            self.lock = True

        if np.logical_or(self.lock, np.logical_and(status == 'short', price_current > self.sell_price_stop)):
            print('STOP SHORT!!!')
            self.lock = True

        if np.abs(self.target_weight_t) < weight_threshold:
            self.lock = False

        if self.lock:
            self.target_weight_t = 0
        else:
            self.target_weight_t = self.target_weight_t

        # print('Lock: ' + str(self.lock) + ' ' + status + ' buy stop: ' + str(self.buy_price_stop) + ' sell stop: ' + str(
        #     self.sell_price_stop) + ' current price: ' + str(price_current) + ' w: ' + str(self.target_weight_t))

        return self.target_weight_t, self.buy_price_stop, self.sell_price_stop, self.lock


class CreateStrategy(object):
    def __init__(self, fx_strategy):

        self.fx_strategy = fx_strategy
        self.wgt = [0]
        self.alpha_scale = [0]
        self.transaction_cost = [0]
        self.gross_return = pd.DataFrame()
        self.net_return = pd.DataFrame()
        self.slippage = pd.DataFrame()
        self.turnover = pd.DataFrame()

    def get_strategy_weight_for_sample(self, order_type, signal, estimated_tc, ask_low, bid_high, mid_price,
                                       tick_incremental, commission, nav, returns, alpha_type, scale_factor=10, spread=None,hour_select=False,
                                       hour_select_ao=False, hour_select_oa=False,
                                       signal2=False, conditional_signal=False):

        for i in range(1, len(signal) - 1):
            # alpha_scale = self.fx_strategy.alpha.get_alpha_scale_decay_for_overnight_hour(hour=signal.index[i].hour,
            #                                                                               hour_select=hour_select,
            #                                                                               scale_factor=1)

            alpha_scale = self.fx_strategy.alpha.get_alpha_scale_decay_for_overnight_hour1(time_index=signal.index[i],
                                                                                           scale_factor=1, hour_select_ao=hour_select_ao,
                                                                                           hour_select_oa=hour_select_oa)

            if alpha_type == 'simple':
                self.fx_strategy.alpha.get_simple_reversion_alpha(return_t=signal.ix[i],
                                                                  short_ema_pre=self.fx_strategy.alpha.short_ema_t,
                                                                  long_ema_pre=self.fx_strategy.alpha.long_ema_t,
                                                                  sigma_ema_pre=self.fx_strategy.alpha.sigma_ema_t,
                                                                  alpha_scale=scale_factor * alpha_scale)
            if alpha_type == 'reversionTrend':
                self.fx_strategy.alpha.get_reversion_alpha_combined(return_t=signal.ix[i],
                                                                    conditional_alpha_t=conditional_signal.ix[i],
                                                                    short_ema_pre_ret=self.fx_strategy.alpha.short_ema_t,
                                                                    long_ema_pre_ret=self.fx_strategy.alpha.long_ema_t,
                                                                    sigma_ema_pre_ret=self.fx_strategy.alpha.sigma_ema_t,
                                                                    short_ema_pre_con=self.fx_strategy.alpha.short_ema_t_con,
                                                                    long_ema_pre_con=self.fx_strategy.alpha.long_ema_t_con,
                                                                    sigma_ema_pre_con=self.fx_strategy.alpha.sigma_ema_t_con,
                                                                    alpha_winsorize1=scale_factor * alpha_scale,
                                                                    alpha_scale1=scale_factor * alpha_scale,
                                                                    alpha_winsorize2=3 * alpha_scale,
                                                                    alpha_scale2=3 * alpha_scale)

            elif alpha_type == 'score':
                self.fx_strategy.alpha.get_zscore_reversion_alpha(return_t=signal.ix[i],
                                                                  short_ema_pre=self.fx_strategy.alpha.short_ema_t,
                                                                  long_ema_pre=self.fx_strategy.alpha.long_ema_t,
                                                                  sigma_ema_pre=self.fx_strategy.alpha.sigma_ema_t,
                                                                  sigma_ema_mean_pre=self.fx_strategy.alpha.sigma_ema_mean_pre_t,
                                                                  sigma_ema_long_pre=self.fx_strategy.alpha.sigma_ema_long_pre_t,
                                                                  sigma_ema_short_pre=self.fx_strategy.alpha.sigma_ema_short_pre_t,
                                                                  alpha_scale=2)
            elif alpha_type == 'conditional':
                self.fx_strategy.alpha.get_reversion_alpha_conditional(return_t=signal.ix[i],
                                                                       conditional_alpha_t=conditional_signal.ix[i],
                                                                       short_ema_pre_ret=self.fx_strategy.alpha.short_ema_t,
                                                                       long_ema_pre_ret=self.fx_strategy.alpha.long_ema_t,
                                                                       sigma_ema_pre_ret=self.fx_strategy.alpha.sigma_ema_t,
                                                                       short_ema_pre_con=self.fx_strategy.alpha.short_ema_t_con,
                                                                       long_ema_pre_con=self.fx_strategy.alpha.long_ema_t_con,
                                                                       sigma_ema_pre_con=self.fx_strategy.alpha.sigma_ema_t_con,
                                                                       alpha_scale=alpha_scale * 2)
            elif alpha_type == 'custom':
                self.fx_strategy.alpha.alpha_signal_t = signal.ix[i] * scale_factor * alpha_scale

            if spread is None:
                self.fx_strategy.transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=signal.index[i].hour,
                                                                                                tc=estimated_tc,
                                                                                                price_t=mid_price.ix[i],
                                                                                                commission=commission,
                                                                                                spread=-100)
            else:
                self.fx_strategy.transaction_cost.get_simple_linear_transaction_cost_hour_based(
                    hour=signal.index[i].hour,
                    tc=estimated_tc,
                    price_t=mid_price.ix[i],
                    commission=commission,
                    spread=spread.ix[i])
            self.fx_strategy.get_weight_simple_alpha_with_tc(weight_current=self.fx_strategy.target_weight_t)

            if order_type == 'lmt':
                self.fx_strategy.get_fill_simulated_weight_tick(weight_target=self.fx_strategy.target_weight_t,
                                                                weight_current=self.wgt[-1],
                                                                buy_price_t=mid_price.ix[i] + tick_incremental,
                                                                sell_price_t=mid_price.ix[i] - tick_incremental,
                                                                ask_low_t1=ask_low.ix[i + 1],
                                                                bid_high_t1=bid_high.ix[i + 1])

            self.fx_strategy.target_weight_t = self.fx_strategy.alpha.get_alpha_scale_decay_for_overnight_hour2(time_index=signal.index[i],
                hour_select_ao=hour_select_ao, hour_select_oa=hour_select_oa,
                scale_factor=self.fx_strategy.target_weight_t)

            # self.fx_strategy.get_stop_loss_weight(weight_current=self.wgt[-1], price_current=mid_price.ix[i])

            self.wgt.append(self.fx_strategy.target_weight_t)
            self.alpha_scale.append(alpha_scale)
            self.transaction_cost.append(self.fx_strategy.transaction_cost.tc)

        self.wgt.append(self.fx_strategy.target_weight_t)
        self.alpha_scale.append(alpha_scale)
        self.transaction_cost.append(self.fx_strategy.transaction_cost.tc)
        self.wgt = pd.DataFrame(self.wgt, index=signal.index, columns=['position']) * nav
        self.transaction_cost = pd.DataFrame(self.transaction_cost, index=signal.index, columns=['transaction_cost'])
        self.alpha_scale = pd.DataFrame(self.alpha_scale, index=signal.index, columns=['alpha_scale'])

        self.gross_return = self.wgt.shift(1).multiply(pd.DataFrame(returns, columns=['position']))
        self.gross_return.columns = ['gross_return']
        self.turnover = pd.DataFrame(self.wgt.diff().abs().values, index=self.wgt.index, columns=['turnover'])
        self.slippage = pd.DataFrame(
                np.multiply(self.turnover.values, (self.transaction_cost.values / 10000 + tick_incremental)),
                index=self.wgt.index, columns=['slippage'])
        self.net_return = pd.DataFrame(self.gross_return.values - self.slippage.values, index=self.wgt.index,
                                       columns=['net_return'])
