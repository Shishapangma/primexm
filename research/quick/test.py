# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt
import warnings

warnings.filterwarnings('ignore')

hour_skip_ao = ['21:00:00', '22:00:00']
hour_skip_oa = ['21:00:00', '22:00:00']
venue = ['gp']
path = '/Users/jerryhuang/Trading/data/log_live/'
fig_path = '/Users/jerryhuang/Trading/data/log_live/pdf'
uta_path = '/Users/jerryhuang/Trading/data/uta/'
date = ['2018-07-08', '2018-07-09', '2018-07-10', '2018-07-11', '2018-07-12', '2018-07-13']

for i in range(0, len(venue)):

    if venue[i] == 'gp':
        symbols = ['AUD_USD', 'EUR_CHF', 'EUR_GBP', 'EUR_USD', 'GBP_USD', 'USD_SGD']

        multi = [0.25, 0.5, 0.3, 0.25, 0.25, 0.75]
        rate = [0.77, 0.77, 0.58, 0.77, 0.77, 1.04]
        commission = 0.06
        nav= 2.7e6
        hour_skip_ao_live_all = {'AUD_USD' : ['1:30:00','7:00:00','21:00:00', '22:00:00'],
                                 'EUR_CHF' : ['7:00:00','21:00:00', '22:00:00'],
                                 'EUR_GBP' : ['7:00:00','8:00:00','21:00:00', '22:00:00'],
                                 'EUR_USD' : ['7:00:00','21:00:00', '22:00:00'],
                                 'GBP_USD' : ['7:00:00','8:00:00','21:00:00', '22:00:00'],
                                 'USD_SGD' : ['0:00:00','19:00:00','20:00:00','21:00:00', '22:00:00']}

        hour_skip_oa_live_all = {'AUD_USD' : ['1:30:00','7:00:00','21:00:00', '22:00:00'],
                                 'EUR_CHF' : ['7:00:00','21:00:00', '22:00:00'],
                                 'EUR_GBP' : ['7:00:00','8:00:00','21:00:00', '22:00:00'],
                                 'EUR_USD' : ['7:00:00','21:00:00', '22:00:00'],
                                 'GBP_USD' : ['7:00:00','8:00:00','21:00:00', '22:00:00'],
                                 'USD_SGD' : ['0:00:00','19:00:00','20:00:00','21:00:00', '22:00:00']}

        path_live = '/Users/jerryhuang/Trading/data/log_live/gp_trade/'
        path_tick = '/Users/jerryhuang/Trading/data/log_live/gp_tick/'
        path_duka = '/Users/jerryhuang/Trading/data/Dukascopy/data_feed/'

    output_all = pd.DataFrame()
    for j in range(0, len(symbols)):
        tc = pd.read_csv('/Users/jerryhuang/Trading/data/Dukascopy/tc_ge/tc_' + symbols[j].replace('_', '') + '.csv', header=0)
        TC_by_hour = (tc.ix[:, 0] * 10000).tolist()


        ##############################################################################################################
        # gp live
        files = os.listdir(path_live)
        if '.DS_Store' in files:
            files.remove('.DS_Store')
        files.sort()
        data_ori = pd.read_csv(path_live + files[j], header=0)
        data_limit = data_ori[np.logical_and(data_ori['event'] != 'One_Sec_Tick', np.logical_and(data_ori['event'] != 'event', data_ori['event'] != 'Tick'))]
        data_limit.time = data_limit.date.astype(str) + '-' + data_limit.time
        data_limit.index = pd.to_datetime(data_limit.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
        data_limit = data_limit.sort_index()
        data_limit[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
            data_limit[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']].astype(float)
        data_limit['mid'] = 0.5 * data_limit['bid'] + 0.5 * data_limit['ask']

        tmp = data_limit[np.logical_and(data_limit['trade price'] == 0, data_limit['traded position'] != 0)]['mid']
        data_limit.loc[
            np.logical_and(data_limit['trade price'] == 0, data_limit['traded position'] != 0), 'trade price'] = tmp

        data_limit['holding_return'] = (data_limit['mid'] - data_limit['mid'].shift(1))\
                                       * data_limit['current position'].shift(1)
        data_limit['slippage'] = (data_limit['mid'] - data_limit['trade price']) * data_limit['traded position']\
                                 - commission / 10000 * np.abs(data_limit['traded position'])
        data_limit['actual_return'] = data_limit['holding_return'] + data_limit['slippage']
        data_limit['target position'].where(data_limit['target position'] != 0, None, inplace=True)
        data_limit['target position'].fillna(method='ffill', inplace=True)
        data_limit['theory_return'] = (data_limit['mid'] - data_limit['mid'].shift(1)) * data_limit['target position'].shift(1)\
                                      - commission / 10000 * np.abs(data_limit['target position'].diff()) * data_limit['mid']
        ######################################################################################################
        # gp tick
        files = os.listdir(path_tick)
        if '.DS_Store' in files:
            files.remove('.DS_Store')
        files.sort()
        data_ori = pd.read_csv(path_tick + files[j], header=0)
        data_limit1 = data_ori[data_ori['event'] == 'Dummy_Tick']
        data_limit1.time = data_limit1.date.astype(str) + '-' + data_limit1.time
        data_limit1.index = pd.to_datetime(data_limit1.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
        data_limit1 = data_limit1.sort_index()
        data_limit1[['ask','bid']] = data_limit1[['ask','bid']].astype(float)
        bid_ask = data_limit1[['ask','bid']].resample('1S').last().fillna(method='pad').shift(1)

        # 58s base
        p = data_limit1['bid'].resample('60S', base= 58).last().fillna(method='pad').shift(1)
        ask = bid_ask['ask'].resample('60S', base=58).last().fillna(method='pad').shift(1)
        bid = bid_ask['bid'].resample('60S', base=58).last().fillna(method='pad').shift(1)
        strategy_tick = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
        strategy_tick.get_strategy_weight_for_sample(order_type='mkt',
                                                 signal=p.pct_change(),
                                                 estimated_tc=TC_by_hour,
                                                 ask_low=p, bid_high=p, mid_price=p, bid_price=bid, ask_price=ask,
                                                 tick_incremental=pd.Series(0.0000, index=p.index),
                                                 commission=commission, nav=1,
                                                 returns=pd.Series(p.pct_change().values, p.index),
                                                 alpha_type='simple',
                                                 tc_type='market',
                                                 hour_select_ao=hour_skip_ao_live_all[symbols[j]],
                                                 hour_select_oa=hour_skip_oa_live_all[symbols[j]],
                                                 scale_factor=10)

        # 00 base
        p = data_limit1['bid'].resample('60S', base= 0).last().fillna(method='pad').shift(1)
        ask = bid_ask['ask'].resample('60S', base=0).last().fillna(method='pad').shift(1)
        bid = bid_ask['bid'].resample('60S', base=0).last().fillna(method='pad').shift(1)
        strategy_tick00 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
        strategy_tick00.get_strategy_weight_for_sample(order_type='mkt',
                                                 signal=p.pct_change(),
                                                 estimated_tc=TC_by_hour,
                                                 ask_low=p, bid_high=p, mid_price=p, bid_price=bid, ask_price=ask,
                                                 tick_incremental=pd.Series(0.0000, index=p.index),
                                                 commission=commission, nav=1,
                                                 returns=pd.Series(p.pct_change().values, p.index),
                                                 alpha_type='simple',
                                                 tc_type='market',
                                                 hour_select_ao=hour_skip_ao_live_all[symbols[j]],
                                                 hour_select_oa=hour_skip_oa_live_all[symbols[j]],
                                                 scale_factor=10)

        ######################################################################################################
        # gp uta
        files = os.listdir(uta_path)
        if '.DS_Store' in files:
            files.remove('.DS_Store')
        files.sort()
        data_ori = pd.read_csv(uta_path + files[j], header=0)
        data_uta = data_ori[np.logical_and(data_ori['event'] != 'One_Sec_Tick',
                                             np.logical_and(data_ori['event'] != 'event', data_ori['event'] != 'Tick'))]
        data_uta.time = data_uta.date.astype(str) + '-' + data_uta.time
        data_uta.index = pd.to_datetime(data_uta.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
        data_uta = data_uta.sort_index()
        data_uta[
            ['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
            data_uta[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price',
                        'trade price']].astype(float)
        data_uta['mid'] = 0.5 * data_uta['bid'] + 0.5 * data_uta['ask']
        data_uta['holding_return'] = (data_uta['mid'] - data_uta['mid'].shift(1)) \
                                       * data_uta['current position'].shift(1)
        data_uta['slippage'] = (data_uta['mid'] - data_uta['trade price']) * data_uta['traded position'] \
                                 - commission / 10000 * np.abs(data_uta['traded position'])
        data_uta['actual_return'] = data_uta['holding_return'] + data_uta['slippage']
        data_uta['target position'].where(data_uta['target position'] != 0, None, inplace=True)
        data_uta['target position'].fillna(method='ffill', inplace=True)
        data_uta['theory_return'] = (data_uta['mid'] - data_uta['mid'].shift(1)) * data_uta[
            'target position'].shift(1) \
                                      - commission / 10000 * np.abs(data_uta['target position'].diff()) * data_uta[
                                          'mid']

        ######################################################################################################
        # weekly plot
        f = plt.figure(figsize =(20,20))
        symbol = symbols[j]
        f.suptitle(symbol)
        multic = multi[j] * nav
        ratec = rate[j]
        output = pd.DataFrame(index=date, columns=['symbol','duka', 'duka_skip', 'live','tick','tick_00'])
        output['symbol'] = symbol
        for x in range(0, 5):
            try:
                start = date[x] + ' 21:00:00'
                end = date[x + 1] + ' 21:00:00'
                ax = f.add_subplot(2, 3, x + 1)

                # ax.plot(strategy.net_return[start:end].cumsum() * multic, label='duka')
                # ax.plot(strategy1.net_return[start:end].cumsum() * multic, label='duka_skip')

                # ax.plot(data_limit['actual_return'][start:end].cumsum(), label='live')
                ax.plot(strategy_tick.net_return[start:end].cumsum()*multic, label='tick58')

                ax1 = ax.twinx()
                ax1.plot(data_uta['actual_return'][start:end].cumsum(), label='uta')

                plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05), ncol=4)
                f.savefig(fig_path + '/' + venue[i] + '_' + symbol + '.pdf')

                output['live'][date[x + 1]] = data_limit['actual_return'][start:end].cumsum()[-1:].values[0]
                output['tick'][date[x + 1]] = strategy_tick.net_return[start:end].cumsum()[-1:].values[0][0] * multic
                output['tick_00'][date[x + 1]] = strategy_tick00.net_return[start:end].cumsum()[-1:].values[0][0] * multic

            except:
                pass

        plt.close()
        output_all = pd.concat([output_all, output])
        output_all.to_clipboard()

