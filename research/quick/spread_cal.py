from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt
import warnings

if __name__ == '__main__':
    symbols = ['AUD_CAD.csv', 'AUD_JPY.csv', 'AUD_NZD.csv', 'NZD_USD.csv']
    tick_log_path = '/Users/jerryhuang/Downloads/test/'

    folds = os.listdir(tick_log_path)
    if '.DS_Store' in folds:
        folds.remove('.DS_Store')

    data = pd.DataFrame()

    for current_fold in folds:
        log_files = os.listdir(tick_log_path + current_fold)

        if '.DS_Store' in log_files:
            log_files.remove('.DS_Store')

        log_file = log_files[log_files.index(symbols[2])]

        tmp = pd.read_csv(tick_log_path + '/' + current_fold + '/' + log_file, header=0)

        data = pd.concat([data, tmp])

    hour_skip_ao = ['21:00:00', '22:00:00']
    hour_skip_oa = ['21:00:00', '22:00:00']

    commission = 0.06
    hour_skip_ao_live_all = {'AUD_USD': ['1:30:00', '7:00:00', '21:00:00', '22:00:00'],
                             'EUR_CHF': ['7:00:00', '21:00:00', '22:00:00'],
                             'EUR_GBP': ['7:00:00', '8:00:00', '21:00:00', '22:00:00'],
                             'EUR_USD': ['7:00:00', '21:00:00', '22:00:00'],
                             'GBP_USD': ['7:00:00', '8:00:00', '21:00:00', '22:00:00'],
                             'USD_SGD': ['0:00:00', '19:00:00', '20:00:00', '21:00:00', '22:00:00']}

    hour_skip_oa_live_all = {'AUD_USD': ['1:30:00', '7:00:00', '21:00:00', '22:00:00'],
                             'EUR_CHF': ['7:00:00', '21:00:00', '22:00:00'],
                             'EUR_GBP': ['7:00:00', '8:00:00', '21:00:00', '22:00:00'],
                             'EUR_USD': ['7:00:00', '21:00:00', '22:00:00'],
                             'GBP_USD': ['7:00:00', '8:00:00', '21:00:00', '22:00:00'],
                             'USD_SGD': ['0:00:00', '19:00:00', '20:00:00', '21:00:00', '22:00:00']}

    tc = pd.read_csv('/Users/jerryhuang/Trading/data/Dukascopy/tc_ge/tc_' + symbols[2][:-4].replace('_', '') + '.csv',
                     header=0)
    TC_by_hour = (tc.ix[:, 0] * 10000).tolist()

    data_limit1 = data[data['event'] == 'Dummy_Tick']
    data_limit1.time = data_limit1.date.astype(str) + '-' + data_limit1.time
    data_limit1.index = pd.to_datetime(data_limit1.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
    data_limit1 = data_limit1.sort_index()
    data_limit1[['ask', 'bid']] = data_limit1[['ask', 'bid']].astype(float)
    bid_ask = data_limit1[['ask', 'bid']].resample('1S').last().fillna(method='pad').shift(1)

    # 58s base
    p = data_limit1['bid'].resample('60S', base=58).last().fillna(method='pad').shift(1)
    ask = bid_ask['ask'].resample('60S', base=58).last().fillna(method='pad').shift(1)
    bid = bid_ask['bid'].resample('60S', base=58).last().fillna(method='pad').shift(1)
    strategy_tick = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
    strategy_tick.get_strategy_weight_for_sample(order_type='mkt',
                                                 signal=p.pct_change(),
                                                 estimated_tc=TC_by_hour,
                                                 ask_low=p, bid_high=p, mid_price=p, bid_price=bid, ask_price=ask,
                                                 tick_incremental=pd.Series(0.0000, index=p.index),
                                                 commission=commission, nav=1,
                                                 returns=pd.Series(p.pct_change().values, p.index),
                                                 alpha_type='simple',
                                                 tc_type='market',
                                                 hour_select_ao=hour_skip_ao_live_all['AUD_USD'],
                                                 hour_select_oa=hour_skip_oa_live_all['AUD_USD'],
                                                 scale_factor=10)

    f = plt.figure(figsize=(20, 20))
    ax = f.add_subplot(1, 1, 1)
    ax.plot(strategy_tick.net_return.cumsum().values)

    plt.show()