import pandas as pd

# raw tick data processing
path = "C:\\Users\\Jerry Huang\\Documents\\gp_log\\tick\\18Dec-22Dec-2017\\EUR_GBP.csv"
data_pool = pd.read_csv(path, header=0)
data_pool = data_pool[data_pool['event'] == 'Dummy_Tick']
data_pool['time_index'] = data_pool['date'].astype(str) + '-' + data_pool['time']
data_pool.index = pd.to_datetime(data_pool['time_index'].str[:-4], format='%Y%m%d-%H:%M:%S')
data_pool[['bid', 'ask']] = data_pool[['bid', 'ask']].astype(float)
data_pool = data_pool[['bid', 'ask']]
# up size tick to second without fillna
data_pool = data_pool[['bid', 'ask']].resample('1S').last().shift(1)
data_pool = data_pool[data_pool.index.second == 58]
# delete the additional data which is longer
data_pool = data_pool.drop(data_pool.index[-11:])

trade_log = pd.read_csv('C:\\Users\\Jerry Huang\\Documents\\gp_log\\18Dec-22Dec-2017\\EUR_GBP.market.csv', header=0)
trade_log = trade_log[trade_log['event'] == 'One_Min_Tick']
trade_log.index = pd.to_datetime((trade_log['date'] + '-' + trade_log['timpstamp'].str[:-7]), format='%Y%m%d-%H:%M:%S')
trade_log[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
    trade_log[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']].astype(float)
trade_log = trade_log[['bid', 'ask']]

combined_log = pd.DataFrame()
combined_log[['trade_bid', 'trade_ask']] = trade_log[['bid', 'ask']]
combined_log[['tick_bid', 'tick_ask']] = data_pool[['bid', 'ask']]

mask = combined_log['tick_bid'].isna()
combined_log.loc[mask, 'tick_bid'] = combined_log.loc[mask, 'trade_bid']
combined_log.loc[mask, 'tick_ask'] = combined_log.loc[mask, 'trade_ask']

combined_log.to_csv('C:\\Users\\Jerry Huang\\Documents\\gp_log\\tick\\18Dec-22Dec-2017\\combined_log.csv', header=True)

from datetime import datetime

count = 0
hit = 0
sec = 0

while 1:
    n = datetime.now().second
    if(sec != n):
        sec = n
        count +=1
        print('sec:', count)

        while 1:
            c = datetime.now()
            s = c.second
            m = c.microsecond
            if s ==sec:
                if m >900000 and m < 999999:
                    hit += 1
                    print('hit:', hit)
                    break
            else:
                break
            

