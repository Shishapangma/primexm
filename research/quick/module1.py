import os
from research.quick.OrderMatch import *
import pandas as pd
import numpy as nps
import matplotlib.pyplot as plt

if __name__ == '__main__':
    path = '/Users/jerryhuang/Trading/data/log_live/gp_trade/'
    log_raw = 'AUD_USD.market.csv'

    log_name = [log_raw]

    commission_fee = round(0.06 / 10000, 7)

    for i in range(len(log_name)):

        data_log = pd.read_csv(path + log_name[i], header=0)
        #data_log = pd.concat([ori, data_log])

        data_log = data_log[data_log['event'] != 'event']
        data_log['time'] = data_log['date'].astype(str) + '-' + data_log['time']
        data_log.index = pd.to_datetime(data_log['time'], format='%Y%m%d-%H:%M:%S.%f')

        data_log[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']] = \
            data_log[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price']].astype(float)

        # Programming P&L calc
        traded_list = data_log[data_log['traded position'] != 0]
        traded_list = traded_list[['bid', 'ask', 'traded position', 'trade price']]
        inventory_actual = Inventory()
        inventory_ideal = Inventory()
        pl_actual = []
        pl_no_commissoin = []
        pl_ideal = []

        fee = 0
        turnover = 0

        for i in range(len(traded_list)):
            position = Position(int(traded_list.iloc[i]['traded position']), round(traded_list.iloc[i]['trade price'], 5))
            pl = inventory_actual.matchFill(position)
            pl_no_commissoin.append(pl)
            pl_actual.append(pl - abs(position.quantity) * commission_fee)
            turnover += abs(int(traded_list.iloc[i]['traded position']))
            fee += abs(position.quantity) * commission_fee
    
            p =Position(int(traded_list.iloc[i]['traded position']), round((traded_list.iloc[i]['bid'] + traded_list.iloc[i]['ask']) / 2, 5))
            pl_ideal.append(inventory_ideal.matchFill(p))

        no_commission_calc_pl = pd.Series(pl_no_commissoin, index=traded_list.index)
        theory_calc_pl = pd.Series(pl_ideal, index=traded_list.index)
        actual_calc_pl = pd.Series(pl_actual, index=traded_list.index)

        f = plt.figure()
        ax1 = f.add_subplot(1, 1, 1)

        ax1.plot(actual_calc_pl['2018-06-27': '2018-06-27'].cumsum().values, label='actual')
        # ax1.plot(no_commission_calc_pl['2018-06-11': '2018-06-22'].cumsum().values, label='no commission')
        # print(actual_calc_pl['2018-06-11': '2018-06-22'].sum())

    plt.show()


