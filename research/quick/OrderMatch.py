from collections import deque
import numpy as np

class POS(enumerate):
    BUY = 1
    SELL = -1

class Position:
    def __init__(self, quantity, price):
        self.quantity = quantity
        self.price = price
        self.pos = POS.BUY if np.sign(quantity) == 1 else POS.SELL

class Inventory:

    def __init__(self):

        # Inventory 
        self.pos = np.nan
        # position meet rule FIFO
        self.pos_list = deque()

    def matchFill(self, position):
        #print('\n')
        #print('filling position: ', position.quantity, ' ', position.price)
        #self.listState('Before: ')

        # inital fill
        if len(self.pos_list) == 0:
            self.pos_list.append(position)
            self.pos = position.pos
            return 0

        # affect current position
        else:
            # increase current position
            if self.pos == position.pos:
                self.pos_list.append(position)
                self.listState('After: ')
                return 0

            # reduce current position, affect realized R&L
            else:
                # get a tempoary value
                left_quantity = position.quantity
                left_inventory_position = self.pos_list.popleft()
                pl = 0

                while abs(left_quantity) > 0:
                    # complete the fill in first inventory
                    if abs(left_quantity) <= abs(left_inventory_position.quantity):
                        pl += self.pos * abs(left_quantity) * (position.price - left_inventory_position.price)
                        # update first inventory position quantity
                        left_inventory_position.quantity += left_quantity
                        left_quantity = 0
                        if abs(left_inventory_position.quantity) > 0:
                            self.pos_list.appendleft(left_inventory_position)
                    # fill position quantity is greater
                    else:
                        pl += self.pos * abs(left_inventory_position.quantity) * (position.price - left_inventory_position.price)
                        left_quantity += left_inventory_position.quantity

                        # still have inventory position
                        if len(self.pos_list):
                            left_inventory_position = self.pos_list.popleft()
                        # reserve position
                        else:
                            position.quantity = left_quantity
                            self.pos = position.pos
                            self.pos_list.append(position)
                            left_quantity = 0
            #self.listState('After: ')
            #print('realized P&L: ', pl)
            return pl

    def calcAvgPrice(self):
        total_amount = 0
        total_quantity = 0

        for i in range(len(self.pos_list)):
            total_amount += self.pos_list[i].price * np.abs(self.pos_list[i].quantity)
            total_quantity += np.abs(self.pos_list[i].quantity)
        
        return total_amount / total_quantity

    def commission(self, position):
        return 6 * position / 1000000


    def listState(self, s):
        pass
        #for i in range(len(self.pos_list)):
        #    s += str(i) + ': '
        #    s += str(self.pos_list[i].quantity) + '-'
        #    s += str(self.pos_list[i].price)
        #print(s)
       
