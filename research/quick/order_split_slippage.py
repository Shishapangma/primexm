import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

data_ori = pd.read_csv('/Users/jerryhuang/Trading/data/log_live/gp_trade/AUD_USD.market.csv', header=0)

data_ori = data_ori[data_ori['event'] != 'event']

data_ori['bid'] = data_ori['bid'].astype(float)
data_ori['ask'] = data_ori['ask'].astype(float)
data_ori['current position'] = data_ori['current position'].astype(float)
data_ori['traded position'] = data_ori['traded position'].astype(float)
data_ori['target price'] = data_ori['target price'].astype(float)
data_ori['trade price'] = data_ori['trade price'].astype(float)

data_ori.time = data_ori.date.astype(str) + '-' + data_ori.time
data_ori.index = pd.to_datetime(data_ori.time.str[:-4], format='%Y%m%d-%H:%M:%S')

data_ori = data_ori['2018-06-25': '2018-06-29']

class Trade:
    """
    save sub-trades in an entire split order
    """
    def __init__(self):
        self.benchmarkPrice = 0.0
        self.timeSlot = ''
        self.subTradeList = []


class SubTrade:
    """
    Sub-trade information of an entire split order
    """
    def __init__(self, t, p, s):
        # fill timeslot
        self.timeSlot = t
        # position
        self.position = p
        # slippage - position * (benchmark price - traded price)
        self.slippage = s

normalOrderPosition = []
normalOrderSlippage = []

splitOrderPosition = []
splitOrderSlippage = []

splitTradeList = []
normalTradeList = []
i = 0


while i < len(data_ori) - 1:

    if data_ori.iloc[i]['event'] == 'One_Min_Tick':
        mid_price = data_ori.iloc[i]['bid'] * 0.5 + data_ori.iloc[i]['ask'] * 0.5
        t = data_ori.iloc[i]['timpstamp']
        i += 1

        # feasible to trigger trade
        if data_ori.iloc[i]['event'] == 'Order_Create':

            j = i + 1

            # find the trigger block
            while data_ori.iloc[j]['event'] != 'One_Min_Tick' and j < len(data_ori) - 1:
                j += 1

            # get the trade logs snip
            data_snip = data_ori[i:j]

            # trade instance
            trade = Trade()
            trade.benchmarkPrice = mid_price
            trade.timeSlot = t

            # order split
            if data_snip[data_snip['event'] == 'Order_Create'].shape[0] > 1:
                trades = data_snip[(data_snip['event'] == 'Order_Filled') | (data_snip['event'] == 'Order_Filling')]

                # sub trade
                for index, row in trades.iterrows():
                    timeSlot = row['timpstamp']
                    position = row['traded position']
                    slippage = (mid_price - row['trade price']) * row['traded position']

                    splitOrderPosition.append(abs(position))
                    splitOrderSlippage.append(slippage)

                    if np.sign(row['traded position']) == 1:
                        trade.subTradeList.append(SubTrade(timeSlot, abs(position), mid_price - row['trade price']))
                    else:
                        trade.subTradeList.append(SubTrade(timeSlot, abs(position), row['trade price'] - mid_price))

                # entire split order
                splitTradeList.append(trade)

            # single order
            else:
                trades = data_snip[(data_snip['event'] == 'Order_Filled') | (data_snip['event'] == 'Order_Filling')]

                for index, row in trades.iterrows():
                    timeSlot = row['timpstamp']
                    position = row['traded position']
                    slippage = (mid_price - row['trade price']) * row['traded position']

                    normalOrderPosition.append(abs(position))
                    normalOrderSlippage.append(slippage)

                    if np.sign(row['traded position']) == 1:
                        trade.subTradeList.append(SubTrade(timeSlot, abs(position), mid_price - row['trade price']))
                    else:
                        trade.subTradeList.append(SubTrade(timeSlot, abs(position), row['trade price'] - mid_price))

                # normal order
                normalTradeList.append(trade)

            i = j

deltaTime = []
fillingOrder = []

for n in normalTradeList:

    baseTime = n.timeSlot

    t = n.subTradeList

    try:
        if len(t) > 1:
            fillingOrder.append((pd.to_datetime(t[-1].timeSlot) - pd.to_datetime(baseTime)).microseconds / 1000000)
        else:
            deltaTime.append((pd.to_datetime(t[-1].timeSlot) - pd.to_datetime(baseTime)).microseconds / 1000000)
    except:
        pass

f = plt.figure()
ax = f.add_subplot(1, 1, 1)
ax.scatter(np.arange(len(fillingOrder)), fillingOrder)
plt.show()

pd.Series(fillingOrder).sum() / len(fillingOrder)
pd.Series(deltaTime).sum() / len(deltaTime)