# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:21:09 2015

@author: pauld
"""

###############################################################################
# basic statistics functions
###############################################################################
import pandas as pd
import numpy as np



###############################################################################


def maxdd(x):
    x.where(pd.notnull(x), 0, inplace=True)
    running_max = np.maximum.accumulate(x)
    dd = (x / running_max) - 1
    dd.where(dd != -1, 0, inplace=True)
    return dd.min()


def AnnRet(x, freq):
    AnnRet = np.exp(np.log(x + 1).mean() * freq) - 1
    return AnnRet


def AnnVol(x, freq):
    AnnVol = x.std() * np.sqrt(freq)
    return AnnVol


def TotRet(x):
    TotRet = (1 + x).cumprod()[-1:].T - 1
    return TotRet


def PerformanceStatistics(x, freq, cashrate):
    AnnualReturn = x.mean() *freq
    print('Annual Return:', AnnualReturn)
    AnnualVol = AnnVol(x, freq)
    print('Annual Vol:', AnnualVol)
    SharpeRatio = (AnnualReturn - cashrate) / AnnualVol
    print('SR:', SharpeRatio)
    # MaxDD = maxdd((1 + x).cumprod())
    # return (AnnualReturn, AnnualVol, SharpeRatio)

def resample(x, freq):
    return (1+x).cumprod().resample(freq).pct_change()

def pivot_year_month(x):
    x = pd.DataFrame(x)
    x['y'] = x.index.year
    x['m'] = x.index.month
    return x.pivot(index = 'y', columns = 'm')


def fx_data_cleaning(data):
    # remove weekend
    data = data[data.index.weekday != 5]
    data = data[~np.logical_and(data.index.weekday == 6, data.index.hour < 20)]
    data = data[~np.logical_and(data.index.weekday == 4, data.index.hour >= 20)]
    return data

def time_series_standardise(data, st, lt):
    return (pd.ewma(data, halflife=st) - pd.ewma(data, halflife=lt)) / pd.ewmstd(data, halflife=lt)

def event_study(return_matrix, event_filter, window):
    event_pos = return_matrix[event_filter].stack(level=-1, dropna=True)
    event_pre = return_matrix[event_filter].stack(level=-1, dropna=True)
    for i in range(1, window):
        event_pos = pd.concat([event_pos, return_matrix.shift(-i)[event_filter].stack(level=-1, dropna=True)], axis=1)
    for i in range(1, window):
        event_pre = pd.concat([event_pre, return_matrix.shift(i)[event_filter].stack(level=-1, dropna=True)], axis=1)

    event_pos = pd.DataFrame(event_pos.values, columns=range(0, window))
    event_pre = pd.DataFrame(event_pre.ix[:, 1:].ix[:, ::-1].values, columns=range(-window + 1, 0))

    return pd.concat([event_pre, event_pos], axis=1)


def iqr_clean(data, number_iqr=2, truncate_or_windzor=0, universe=None, inplace=False, nan_ex_universe=False):
        """ Clean data outside iqr range
        truncate_or_windzor=1 for truncate, 0 for windzor, not defined otherwise
        RP, Sep 15
        """

        # remove from universe if needed
        if inplace:
            new_data_ = data
        else:
            new_data_ = data.copy()

        if isinstance(new_data_, pd.DataFrame):
            new_data = new_data_.values
        else:
            new_data = new_data_

        value_data = new_data.copy()
        if (universe is not None) and nan_ex_universe:
            mask_assign(new_data, np.nan, np.logical_not(universe > 0))
        if universe is not None:
            mask_assign(value_data, np.nan, np.logical_not(universe > 0))

        # compute inter-quartile range and median
        size_data = data.shape[1]
        bad_dates = np.all(np.isnan(value_data), axis=1)
        value_data[bad_dates, 0] = 99.

        percentiles = np.nanpercentile(value_data, [75, 50, 25], axis=1)
        percentiles[bad_dates, :] = np.nan
        iqr = percentiles[:, 0] - percentiles[:, 2]

        value_data[bad_dates, 0] = np.nan

        # truncate or windzor
        lower_bound = np.kron(percentiles[:, 2] - number_iqr * iqr, np.ones((size_data, 1))).T
        upper_bound = np.kron(percentiles[:, 0] + number_iqr * iqr, np.ones((size_data, 1))).T
        if truncate_or_windzor == 1:
            mask_assign(new_data, np.nan, np.logical_or(new_data <= lower_bound, upper_bound <= new_data))
        elif truncate_or_windzor == 0:
            mask_assign(new_data, lower_bound, new_data <= lower_bound)
            mask_assign(new_data, upper_bound, upper_bound <= new_data)
        else:
            new_data[:, :] = np.nan

        return new_data_

def mask_assign(lhs, rhs, mask, apply_mask_lhs_only=False):
        """ Assign 2 matrices with a mask
        Equivalent of lhs(mask) = rhs(mask) or lhs(mask) = rhs in Matlab
        Modify in place (nothing is returned)
        RP, Sep 15
        """

        # load values
        if isinstance(lhs, pd.DataFrame):
            lhs_value = lhs.values
        else:
            lhs_value = lhs
        if isinstance(rhs, pd.DataFrame):
            rhs_value = rhs.values
        else:
            rhs_value = rhs
        if isinstance(mask, pd.DataFrame):
            mask_value = mask.values
        else:
            mask_value = mask

        # do assign
        lhs_value_flatten = lhs_value.flatten()
        mask_value_flatten = mask_value.flatten()

        if isinstance(rhs, pd.DataFrame) or isinstance(rhs, np.ndarray):
            rhs_value_flatten = rhs_value.flatten()
            if apply_mask_lhs_only:
                lhs_value_flatten[mask_value_flatten] = rhs_value_flatten
            else:
                lhs_value_flatten[mask_value_flatten] = rhs_value_flatten[mask_value_flatten]
        else:
            lhs_value_flatten[mask_value_flatten] = rhs_value

        lhs[:] = lhs_value_flatten.reshape(lhs.shape)