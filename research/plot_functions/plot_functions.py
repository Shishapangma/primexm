
import matplotlib.pyplot as plt


def market_trade_monitor_plot(ax, bid, ask, mid, trade_price, trade_position, x):
    ax.plot(bid,linestyle = '')
    ax.plot(ask,linestyle = '')
    ax.plot(mid, color = '#08192D')
    ax.fill_between(x, bid, ask, color = '#0089A7')
    ax.plot(trade_price[trade_position>0], marker = 'o', linestyle = '', color = '#86C166', fillstyle='full', markeredgewidth=0.0, markersize=7)
    ax.plot(trade_price[trade_position<0], marker = 'o', linestyle = '', color = '#D05A6E', fillstyle='full', markeredgewidth=0.0, markersize=7)
    ax.grid()
    ax.set_title("trade price and bid-ask spread")


def position_monitor_plot(ax, target_position, current_position, trade_position, x):
    ax.fill_between(x, current_position, target_position, where= current_position > target_position, color = '#227D51')
    ax.fill_between(x, current_position, target_position, where= current_position < target_position, color = '#E83015')
    ax.plot(trade_position[trade_position>0], marker = 'o', linestyle = '', color = '#86C166', fillstyle='full', markeredgewidth=0.0, markersize=5)
    ax.plot(trade_position[trade_position<0], marker = 'o', linestyle = '', color = '#D05A6E', fillstyle='full', markeredgewidth=0.0, markersize=5)
    ax.grid()
    ax.set_title("trade position and positions difference between target and current")

def peformance_monitor_plot(ax, system_PnL, theory_return, actual_return, trading_return, holding_return, slippage):
    ax.plot(system_PnL.cumsum(), label = 'system_PnL')
    ax.plot(theory_return.cumsum(), label = 'theory_return')
    ax.plot(actual_return.cumsum(), label = 'actual_return')
    ax.plot(trading_return.cumsum(), label = 'trading_return')
    ax.plot(holding_return.cumsum(), label = 'holding_return')
    ax.plot(slippage.cumsum(), label = 'slippage')
    ax.grid()
    ax.set_title("cumulative performance")
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),ncol=3)

def plot_news(currency, time, finish_time, color, news, data):
    data[currency].cumsum().plot()
    plt.axvspan(time, finish_time, color=color, alpha=0.5, lw=0)
    plt.suptitle(news)