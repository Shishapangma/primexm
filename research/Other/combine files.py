# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.fx_performance as fx
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt

for d in range(0, 5):
    symbols_all = ['audusd', 'eurchf', 'eurgbp', 'nzdusd', 'usdsgd']
    symbols = symbols_all[d]
    data = pd.DataFrame()
    for x in [24, 25, 26, 27, 28]:
        path = r'C:\Research\data\log_live_archive\historical data\29-Jul-2017\tick\\' + str(x) + '-Jul-2017\\'
        data_ori = pd.read_csv(path + os.listdir(path)[d], header=0)
        data = pd.concat([data, data_ori])

    data.to_csv(r'C:\Research\data\log_live_archive\historical data\29-Jul-2017\tick\combine\\' + symbols + '.csv')


##############################################################################
# combine combined files
# symbols = 'usdsgd'
# data1 = pd.read_csv(r'C:\Research\data\log_live_archive\historical data\\research\\' + symbols +'.csv', header=0)
# data2 = pd.read_csv(r'C:\Research\data\log_live_archive\historical data\22-Apr-2017\combine\\' + symbols +'.csv', header=0)
# data = pd.concat([data1, data2])
# data.to_csv(r'C:\Research\data\log_live_archive\historical data\research\\' + symbols +'.csv')
