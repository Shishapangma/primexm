import matplotlib.pyplot as plt

def plot_news(currency, time, finish_time, news, data):

    data[currency].cumsum().plot()
    plt.axvspan(time, finish_time, color='y', alpha=0.5, lw=0)
    plt.suptitle(news)

news = 'ECB President Draghi Speech '
time = '2017-02-06 12:00:00'
finish_time = '2017-02-06 17:00:00'

plot_news(currency = 'eurusd', time=time, finish_time=finish_time, news=news, data = all['2017-02-06':])
plot_news(currency = 'eurchf', time=time, finish_time=finish_time, news=news, data = all['2017-02-06':])


news = 'AUD Reserve Bank of Australia Rate Decision (FEB 07)'
time = '2017-02-07 01:30:00'
finish_time = '2017-02-07 06:30:00'
plot_news(currency='audcad', time=time, finish_time=finish_time, news=news, data = all['2017-02-06':])
plot_news(currency='audnzd', time=time, finish_time=finish_time, news=news, data = all['2017-02-06':])
plot_news(currency='audusd', time=time, finish_time=finish_time, news=news, data = all['2017-02-06':])

news = 'RBNZ interest rate decision'
time = '2017-02-08 18:00:00'
finish_time = '2017-02-08 23:00:00'
plot_news(currency='audnzd', time=time, finish_time=finish_time, news=news, data = all['2017-02-06':])


news = 'CAD Unemployment Rate (JAN)'
time = '2017-02-10 11:30:00'
finish_time = '2017-02-10 16:30:00'
plot_news(currency='audcad', time=time, finish_time=finish_time, news=news, data = all['2017-02-06':])

################################################

news = 'EUR German Consumer Price Index (YoY) (JAN P)'
time = '2017-01-30 11:00:00'
finish_time = '2017-01-30 16:30:00'
plot_news(currency='eurusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])
plot_news(currency='eurchf', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])


news = 'EUR German Unemployment Change (JAN)'
time = '2017-01-31 06:55:00'
finish_time = '2017-01-31 13:00:00'
plot_news(currency='eurusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])
plot_news(currency='eurchf', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])

news = 'CAD Gross Domestic Product (YoY) (NOV)'
time = '2017-01-31 11:30:00'
finish_time = '2017-01-31 16:30:00'
plot_news(currency='audcad', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])


news = 'USD Consumer Confidence (JAN)'
time = '2017-01-31 12:00:00'
finish_time = '2017-01-31 18:00:00'
plot_news(currency='audusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])
plot_news(currency='eurusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])


news = 'NZD Unemployment Rate (4Q)'
time = '2017-01-31 18:45:00'
finish_time = '2017-02-01 00:45:00'
plot_news(currency='audnzd', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])


news = 'USD ISM Manufacturing (JAN)'
time = '2017-02-01 12:00:00'
finish_time = '2017-02-01 22:00:00'
plot_news(currency='audusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])
plot_news(currency='eurusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])


news = 'USD ISM Manufacturing (JAN)'
time = '2017-02-03 10:30:00'
finish_time = '2017-02-03 18:30:00'
plot_news(currency='audusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])
plot_news(currency='eurusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-29':'2017-02-06'])

################################################

news = 'AUD Consumer Prices Index (YoY) (4Q)'
time = '2017-01-24 21:30:00'
finish_time = '2017-01-25 3:30:00'
plot_news(currency='audusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-23':'2017-01-28'])
plot_news(currency='audcad', time=time, finish_time=finish_time, news=news, data = all['2017-01-23':'2017-01-28'])
plot_news(currency='audnzd', time=time, finish_time=finish_time, news=news, data = all['2017-01-23':'2017-01-28'])

news = 'NZD Consumer Prices Index (YoY) (4Q)'
time = '2017-01-25 18:45:00'
finish_time = '2017-01-26 0:45:00'
plot_news(currency='audnzd', time=time, finish_time=finish_time, news=news, data = all['2017-01-23':'2017-01-28'])

news = 'USD Advance Goods Trade Balance (DEC)'
time = '2017-01-26 10:30:00'
finish_time = '2017-01-26 16:30:00'
plot_news(currency='audusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-23':'2017-01-28'])
plot_news(currency='eurusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-23':'2017-01-28'])

news = 'USD Gross Domestic Product (Annualized) (4Q A)'
time = '2017-01-27 10:30:00'
finish_time = '2017-01-27 16:30:00'
plot_news(currency='audusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-23':'2017-01-28'])
plot_news(currency='eurusd', time=time, finish_time=finish_time, news=news, data = all['2017-01-23':'2017-01-28'])


################################################