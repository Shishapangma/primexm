import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf
from statsmodels.tsa.stattools import acf, pacf
import statsmodels.formula.api as smf

########################################################################
d = 11
path = r'C:\Research\data\Dukascopy\2017\\'
files = os.listdir(path)
data_ori = pd.read_csv(path + files[d], header=0)
data_ori.index = pd.to_datetime(data_ori['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
test = data_ori
p = test['Close']
ret = p.pct_change()
##############################################################################
# original model
# alpha = -pm.time_series_standardise(ret, st = 20, lt = 200)
alpha = (pd.ewma(ret, halflife=200) - pd.ewma(ret, halflife=20)) / pd.ewmstd(ret, halflife=200) * 10
str_ret = (alpha.shift(1) * ret)
# str_ret.cumsum().plot()
print(pm.PerformanceStatistics(str_ret, 60 * 24 * 300, 0))
print(pd.DataFrame(alpha).corrwith(pd.DataFrame(alpha).shift(1)))
##############################################################################
# model3
alpha = (pd.ewma(ret, halflife=200) - pd.ewma(ret, halflife=20)) / pd.ewmstd(
        (pd.ewma(ret, halflife=400) - pd.ewma(ret, halflife=10)), halflife=40)
str_ret = (alpha.shift(1) * ret)
# str_ret.cumsum().plot()
print(pm.PerformanceStatistics(str_ret, 60 * 24 * 300, 0))
print(pd.DataFrame(alpha).corrwith(pd.DataFrame(alpha).shift(1)))

##############################################################################
# backtest
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_' + 'usdsgd' + '.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()
# TC_by_hour = np.zeros(24).tolist()
com = 0.13

strategy1 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy1.get_strategy_weight_for_sample(order_type='mkt', signal=p.pct_change(),
                                         estimated_tc=TC_by_hour,
                                         ask_low=p, bid_high=p, mid_price=p, tick_incremental=0.0000,
                                         commission=com, nav=1, returns=pd.Series(p.pct_change().values, p.index),
                                         alpha_type='simple')
# (strategy1.net_return['2010-01-01 23:00:00':]).cumsum().plot()
print(pm.PerformanceStatistics(strategy1.net_return, 300 * 60 * 24, 0))

strategy = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2.5)))
strategy.get_strategy_weight_for_sample(order_type='mkt', signal=p.pct_change(),
                                        estimated_tc=TC_by_hour,
                                        ask_low=p, bid_high=p, mid_price=p, tick_incremental=0.0000,
                                        commission=com, nav=1, returns=pd.Series(p.pct_change().values, p.index),
                                        alpha_type='score')
# (strategy.net_return['2010-01-01 23:00:00':]).cumsum().plot()
print(pm.PerformanceStatistics(strategy.net_return, 300 * 60 * 24, 0))
