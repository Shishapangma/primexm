import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf

########################################################################
# load some data
d = 1
files = os.listdir(r'C:\Research\data\Dukascopy Tick\\')
data = pd.read_csv('C:\\Research\\data\\Dukascopy Tick\\' + files[d], skiprows=100000000, nrows=10000000, header=0)
# data = pd.read_csv('C:\\Research\\data\\Dukascopy Tick\\2017\\' + files[d], header=0)
data.columns = ['time', 'ask', 'bid', 'ask_size', 'bid_size']
data.index = pd.to_datetime(data['time'])
data['mid'] = (data['ask'] + data['bid']) / 2
########################################################################
# d = 1
# path = r'C:\Research\data\log_live_archive\historical data\15-Apr-2017\combine\\'
# files = os.listdir(path)
# data_ori = pd.read_csv(path + files[d], header=0)
# data = data_ori[data_ori['event'] == 'Tick']
# data.index = pd.to_datetime(data.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
# data = data[['bid', 'ask', 'bid vol', 'ask vol']].astype(float)
# data.rename(columns={'bid vol': 'bid_size', 'ask vol': 'ask_size'}, inplace=True)
# data = data[:'2017-04-13 21:00:00']
#########################################################################################################################
# is there a frequency where momentum exists? shorter or longer term? - momentum exist in 1-5 second
# market order arrival, order imbalance, bid-ask spread widening that predicts information arrival
# a = []
# for i in range(0,61):
#     data_lf = data.resample('60S', base=i).last()
#     ret = pd.DataFrame(data_lf['Close'].pct_change())
#     a.append(ret.shift(1).corrwith(ret))
# pd.DataFrame(a).plot(kind='bar')
# run regression
# alpha = (pd.ewma(ret, halflife=3) - pd.ewma(ret, halflife=10)) / pd.ewmstd(ret, halflife=100)
#
# regdata = pd.DataFrame(pd.concat([ret, alpha.shift(1)], axis=1))
# regdata.columns = ['ret', 'ret_lag']
# import statsmodels.formula.api as smf
# results = smf.ols('ret ~ ret_lag', data=regdata).fit()
# print(results.summary())
# # volatility breakout - predict trend
# freq = '1T'
# close = data['mid'].resample(freq).last()
# open = data['mid'].resample(freq).first()
# high = data['mid'].resample(freq).max()
# low = data['mid'].resample(freq).min()
# vol = data['mid'].resample(freq).std()
# true_range = pd.DataFrame(high - low)
# vol = pd.DataFrame(vol)
# ret = pd.DataFrame(close.pct_change())
# alpha = (pd.ewma(ret, halflife=200) - pd.ewma(ret, halflife=20)) / pd.ewmstd(ret, halflife=200)
# str_ret = (alpha.shift(1) * ret)
# pm.PerformanceStatistics(str_ret, 60 * 24 * 300, 0)
# a = []
# for i in range(0, 20):
#     a.append(vol.shift(i).corrwith(str_ret))
# pd.DataFrame(a).plot(kind='bar')
#
# str_ret.cumsum().plot()
# ###############################################################################
# # 1 min mean-reversion correct for 10 second momentum
# data_s = data.resample('10S').last()
# data_s.fillna(method='pad', inplace=True)
# data_m = data.resample('60S', base=30).last()
# data_m.fillna(method='pad', inplace=True)
# data_ms = pd.merge(pd.DataFrame(data_m['mid'].pct_change()), pd.DataFrame(data_s['mid'].pct_change()), how='right',
#                    left_index=True, right_index=True)
# data_ms.fillna(method='pad', inplace=True)
# data_ms['mid_new'] = data_ms['mid_x'] - data_ms['mid_y']
# a = []
# data_m = data.resample('60S', base=30).last()
# ret = pd.DataFrame(data_m['mid'].pct_change())
# for i in range(1, 20):
#     a.append(ret.shift(i).corrwith(ret))
# pd.DataFrame(a).plot(kind='bar')
#
# ###############################################################################
# # 1 min mean-reversion at 10s interval
# data_s = data.resample('10S').last()
# data_s.fillna(method='pad', inplace=True)
# signal = pd.rolling_sum(data_s['mid'].pct_change(), window=6).shift(1)
# signal = pd.DataFrame(signal.resample('1T').first())
#
# ret = pd.DataFrame(data.resample('1T').last()['mid'].pct_change())
# a = []
# for i in range(1, 20):
#     a.append(signal.shift(i).corrwith(ret))
# pd.DataFrame(a).plot(kind='bar')

################################################################################
# Order imbalance
test = data
test['mid'] = 0.5 * test['bid'] + 0.5 * test['ask']
test['Ia'], test['Ib'], test['Ic'], test['Id'] = 0, 0, 0, 0
test['Ia'].where(~(test['bid'] >= test['bid'].shift(1)), 1, inplace=True)
test['Ib'].where(~(test['bid'] <= test['bid'].shift(1)), 1, inplace=True)
test['Ic'].where(~(test['ask'] <= test['ask'].shift(1)), 1, inplace=True)
test['Id'].where(~(test['ask'] >= test['ask'].shift(1)), 1, inplace=True)
test['oel'] = test['Ia'] * test['bid_size'] - test['Ib'] * test['bid_size'].shift(1) - test['Ic'] * test['ask_size'] + \
              test['Id'] * test['bid_size'].shift(1)

freq = '60S'
oel = test.oel.resample(freq).sum()
ret = pd.DataFrame(data['mid'].pct_change())
vol = data['mid'].resample(freq).std()
high = data['mid'].resample(freq).max()
low = data['mid'].resample(freq).min()
range1 = pd.DataFrame(high - low)
intense = test['mid'].resample(freq).count()

oel = (pd.ewma(oel, halflife=1) - pd.ewma(oel, halflife=20)) / pd.ewmstd(oel, halflife=20)

regdata = pd.DataFrame(pd.concat([ret.shift(-1), ret, vol, oel, intense, range1], axis=1))
regdata.columns = ['ret', 'ret_lag', 'vol', 'oel', 'intense', 'range']
import statsmodels.formula.api as smf

results = smf.ols('ret ~  ret_lag', data=regdata).fit()
print(results.summary())
#
# a=[]
# oel = pd.DataFrame(oel.values, index=oel.index, columns=['mid'])
# for i in range(0, 20):
#     a.append(pd.DataFrame(results.resid, columns = ['mid']).corrwith((ret*oel).shift(i)))
# pd.DataFrame(a).plot(kind='bar')

results = smf.ols('ret ~  ret_lag*oel', data=regdata).fit()
print(results.summary())
results = smf.ols('ret ~  ret_lag*oel + ret_lag*vol ', data=regdata).fit()
print(results.summary())

#

oel = test.oel.resample(freq).sum()
oel = (oel - pd.ewma(oel, halflife=20)) / pd.ewmstd(oel, halflife=20)
oel = ((0.5 + 0.5 * np.tanh(-oel)) + 1)/1
oel = pd.DataFrame(oel.values, index=oel.index, columns=['mid'])
alpha = ret
alpha = (pd.ewma(alpha, halflife=200) - pd.ewma(alpha, halflife=20)) / pd.ewmstd(alpha, halflife=200) * oel

# oel = oel.where(np.logical_or(oel < -0.5, oel > 0.5), np.nan)

# ret_z = (ret - pd.ewma(ret, halflife=20)) / pd.ewmstd(ret, halflife=20)
# ret_z = (0.5 + 0.5 * np.tanh(ret_z))
# alpha = ret_z * oel
# alpha.where(alpha.notnull(), 0, inplace = True)

str_ret = (alpha.shift(1) * ret)
print(pm.PerformanceStatistics(str_ret, 60 * 24 * 300, 0))
str_ret.cumsum().plot()
alpha = (pd.ewma(ret, halflife=200) - pd.ewma(ret, halflife=20)) / pd.ewmstd(ret, halflife=200)
str_ret = (alpha.shift(1) * ret)
print(pm.PerformanceStatistics(str_ret, 60 * 24 * 300, 0))
str_ret.cumsum().plot()
#
#
# oel = [0]
# for i in range(1, len(test)):
#     oel_i = get_e_order_flow_imbalance(bid_t=test['bid'].ix[i], bid_t1=test['bid'].ix[i - 1], ask_t=test['ask'].ix[i],
#                                ask_t1=test['ask'].ix[i - 1], bid_q_t = test['bid_size'].ix[i], bid_q_t1=test['bid_size'].ix[i-1],
#                                ask_q_t=test['ask_size'].ix[i], ask_q_t1 = test['ask_size'].ix[i-1])
#     oel.append(oel_i)
# def get_e_order_flow_imbalance(bid_t, bid_t1, ask_t, ask_t1, bid_q_t, bid_q_t1, ask_q_t, ask_q_t1):
#     Ia, Ib, Ic, Id = 0, 0, 0, 0
#
#     if bid_t >= bid_t1:
#         Ia = 1
#     if bid_t <= bid_t1:
#         Ib = 1
#     if ask_t <= ask_t1:
#         Ic = 1
#     if ask_t >= ask_t1:
#         Id = 1
#
#     e_ofl = Ia * bid_q_t - Ib * bid_q_t1 - Ic * ask_t1 + Id * ask_q_t1
#
#     return e_ofl
