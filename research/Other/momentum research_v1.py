import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf
from statsmodels.tsa.stattools import acf, pacf
import statsmodels.formula.api as smf
########################################################################
d = 0
sym = 'audusd'
########################################################################
# path = r'C:\Research\data\log_live_archive\historical data\research\\'
# files = os.listdir(path)
# data_ori = pd.read_csv(path + files[d], header=0)
# data = data_ori[data_ori['event'] == 'Tick']
# # data.time = data.date.astype(str) + '-'+ data.time
# data.index = pd.to_datetime(data.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
# data = data[['bid', 'ask', 'bid vol', 'ask vol']].astype(float)
# data.rename(columns={'bid vol': 'bid_size', 'ask vol': 'ask_size'}, inplace=True)
# data = data[:'2017-04-13 21:00:00']
########################################################################
path = r'C:\Research\data\LMAX data\Research\\'
files = os.listdir(path)
data = pd.read_csv(path + files[d], header=0)
data.index = pd.to_datetime(data['TIMESTAMP'], unit='ms')
del data['Unnamed: 0']
data.columns = ['time','bid','bid_size','ask','ask_size']
########################################################################
# load some data
# files = os.listdir(r'C:\Research\data\Dukascopy Tick\\')
# data = pd.read_csv('C:\\Research\\data\\Dukascopy Tick\\' + files[d], skiprows=110000000, nrows=10000000, header=0)
# # data = pd.read_csv('C:\\Research\\data\\Dukascopy Tick\\2017\\' + files[d], header=0)
# data.columns = ['time', 'ask', 'bid', 'ask_size', 'bid_size']
# data.index = pd.to_datetime(data['time'])
########################################################################
test = data.copy()
test['mid'] = 0.5 * test['bid'] + 0.5 * test['ask']

# test = test.resample('250L').last()
# test.fillna(method='pad',inplace=True)
########################################################################
# d = 7
# path = r'C:\Research\data\Dukascopy Tick\2017\\'
# files = os.listdir(path)
# data_ori = pd.read_csv(path + files[d], header=0)
# data_ori.index = pd.to_datetime(data_ori['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
# test = data_ori[:'2016-09-01']
# test['mid'] = test['Close']
################################################################################
# Order imbalance
test['Ia'], test['Ib'], test['Ic'], test['Id'] = 0, 0, 0, 0
test['Ia'].where(~(test['bid'] >= test['bid'].shift(1)), 1, inplace=True)
test['Ib'].where(~(test['bid'] <= test['bid'].shift(1)), 1, inplace=True)
test['Ic'].where(~(test['ask'] <= test['ask'].shift(1)), 1, inplace=True)
test['Id'].where(~(test['ask'] >= test['ask'].shift(1)), 1, inplace=True)
test['oel'] = test['Ia'] * test['bid_size'] - test['Ib'] * test['bid_size'].shift(1) - test['Ic'] * test['ask_size'] + \
              test['Id'] * test['bid_size'].shift(1)
###############################################################################
# Order revision
test['bid_revision'], test['ask_revision'] = 0, 0
test['bid_revision'].where(~np.logical_or(test['bid'].diff()!=0,test['bid_size'].diff()!=0), 1, inplace = True)
test['ask_revision'].where(~np.logical_or(test['ask'].diff()!=0,test['ask_size'].diff()!=0), 1, inplace = True)
################################################################################
# Create variables
freq = '60S'
oel = test.oel.resample(freq).sum()
oel.fillna(method='pad', inplace=True)
p = test.mid.resample(freq).last()
p.fillna(method='pad', inplace=True)
ret = p.pct_change()
vol = test['mid'].resample(freq).std()
high = test['mid'].resample(freq).max()
low = test['mid'].resample(freq).min()
True_range = pd.DataFrame(high - low)
bid_revision = test['bid_revision'].resample(freq).sum()
ask_revision = test['ask_revision'].resample(freq).sum()
spread = (test['ask']-test['bid']).resample(freq).mean()

corr = pd.ewmcorr(ret, ret.shift(1), halflife=200, min_periods=100)
corr.where(corr < 0.3, 0.3, inplace=True)
corr.where(corr > -0.3, -0.3, inplace=True)
regdata = pd.DataFrame(pd.concat([ret.shift(-1), ret, vol, oel, corr, ask_revision, True_range], axis=1))
regdata.columns = ['ret_t1', 'ret', 'vol', 'oel', 'corr', 'ask_revision','True_range']
dummy = pd.DataFrame(pd.get_dummies(regdata.index.hour).values,index = regdata.index)
regdata = pd.concat([regdata, dummy], axis=1)
##############################################################################
# regression model
results = smf.ols('ret_t1 ~ ret*oel', data=regdata).fit()
print(results.summary())

##############################################################################
# summary statstics
# pd.DataFrame(acf(ret.fillna(0)))[1:].plot(kind='bar')
pd.DataFrame(oel).corrwith(pd.DataFrame(oel).shift(1))
##############################################################################
# original model
alpha = -pm.time_series_standardise(ret, st = 20, lt = 200)
# alpha =(pd.ewma(ret, halflife=200) - pd.ewma(ret, halflife=20)) / pd.ewmstd(ret, halflife=200)
str_ret = (alpha.shift(1) * ret)
# str_ret.cumsum().plot()
print(pm.PerformanceStatistics(str_ret, 60 * 24 * 300, 0))
print(pd.DataFrame(alpha).corrwith(pd.DataFrame(alpha).shift(1)))
# ##############################################################################
# # model1
# a = -pd.DataFrame(ret, columns = ['mid'])
# b = pm.time_series_standardise(oel, st = 5, lt = 200)
# b = pd.DataFrame(b.values, index = a.index, columns = ['mid'])
# b = 0.5 + 0.5*np.tanh(-b)
# ret = pd.DataFrame(ret)
# alpha = pm.time_series_standardise(a, st = 20, lt = 200).multiply(b)
# alpha = pd.DataFrame(alpha, index = ret.index)
# alpha.columns = ['mid']
# str_ret1 = (alpha.shift(1) * ret)
# str_ret1.cumsum().plot()
# print(pm.PerformanceStatistics(str_ret1, 60 * 24 * 300, 0))
# str_ret1.corrwith(str_ret)
# print(pd.DataFrame(alpha).corrwith(pd.DataFrame(alpha).shift(1)))
##############################################################################
# model3
# alpha = (pd.ewma(ret, halflife=200) - pd.ewma(ret, halflife=20)) / pd.ewmstd((pd.ewma(ret, halflife=200) - pd.ewma(ret, halflife=20)), halflife=30)
# str_ret = (alpha.shift(1) * ret)
# # str_ret.cumsum().plot()
# print(pm.PerformanceStatistics(str_ret, 60 * 24 * 300, 0))
# print(pd.DataFrame(alpha).corrwith(pd.DataFrame(alpha).shift(1)))
##############################################################################
# regression model
# oel_new = oel/pd.ewmstd(oel,halflife=200)
var_new = pd.DataFrame(True_range.values, index=oel.index, columns=['mid'])
ret = pd.DataFrame(ret)
a = ret*var_new
alpha1 = pm.time_series_standardise(a, st=20, lt=200)
alpha1 = pd.DataFrame(alpha1, index=ret.index)
alpha1.columns = ['mid']
str_ret = (alpha1.shift(1) * ret)
# str_ret.cumsum().plot()
print(pm.PerformanceStatistics(str_ret, 60 * 24 * 300, 0))
print(pd.DataFrame(alpha1).corrwith(pd.DataFrame(alpha1).shift(1)))
print(pd.DataFrame(alpha).corrwith(pd.DataFrame(alpha1)))

#######################################################
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_' + sym + '.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()
# TC_by_hour = np.zeros(24).tolist()
com = 0.13
alpha[alpha > 1] = 1
alpha[alpha < -1] = -1
strategy1 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy1.get_strategy_weight_for_sample(order_type='mkt', signal=alpha*10,
                                         estimated_tc=TC_by_hour,
                                         ask_low=p, bid_high=p, mid_price=p, tick_incremental=0.0000,
                                         commission=com, nav=1, returns=pd.Series(p.pct_change().values, p.index),
                                         alpha_type='custom')
# (strategy1.net_return['2010-01-01 23:00:00':]).cumsum().plot()
print(pm.PerformanceStatistics(strategy1.net_return, 300 * 60 * 24, 0))

s = pm.time_series_standardise(a, st=5, lt=200)
s[s > 1] = 3
s[s < -1] = -3
s = (s['mid']*3 + alpha*10)
strategy2 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy2.get_strategy_weight_for_sample(order_type='mkt', signal=s,
                                         estimated_tc=TC_by_hour,
                                         ask_low=p, bid_high=p, mid_price=p, tick_incremental=0.0000,
                                         commission=com, nav=1, returns=pd.Series(p.pct_change().values, p.index),
                                         alpha_type='custom')
# (strategy2.net_return['2010-01-01 23:00:00':]).cumsum().plot()
print(pm.PerformanceStatistics(strategy2.net_return, 300 * 60 * 24, 0))

plt.figure()
plt.plot((strategy1.net_return['2010-01-01 23:00:00':]).cumsum())
plt.plot((strategy2.net_return['2010-01-01 23:00:00':]).cumsum())

plt.figure()
plt.plot(strategy2.net_return.cumsum() - strategy1.net_return.cumsum())


strategy3 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy3.get_strategy_weight_for_sample(order_type='mkt', signal=p.pct_change(),
                                         estimated_tc=TC_by_hour,
                                         ask_low=p, bid_high=p, mid_price=p, tick_incremental=0.0000,
                                         commission=com, nav=1, returns=pd.Series(p.pct_change().values, p.index),
                                         alpha_type='simple')
(strategy3.net_return).cumsum().plot()
print(pm.PerformanceStatistics(strategy3.net_return, 300 * 60 * 24, 0))

strategy4 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy4.get_strategy_weight_for_sample(order_type='mkt', signal=p.pct_change(),
                                         estimated_tc=TC_by_hour,
                                         ask_low=p, bid_high=p, mid_price=p, tick_incremental=0.0000,
                                         commission=com, nav=1, returns=pd.Series(p.pct_change().values, p.index),
                                         conditional_signal=a['mid'],
                                         alpha_type='reversionTrend')
(strategy4.net_return).cumsum().plot()
print(pm.PerformanceStatistics(strategy4.net_return, 300 * 60 * 24, 0))
plt.figure()
plt.plot(strategy4.net_return.cumsum() - strategy3.net_return.cumsum())

print(strategy1.turnover.sum())
print(strategy2.turnover.sum())
print(strategy3.turnover.sum())
print(strategy4.turnover.sum())