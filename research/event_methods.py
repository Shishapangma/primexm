import pandas as pd
import numpy as np

def get_event_filter_price_jumps(return_matrix, hl_mean, std_mult, return_threshold, direction):
    avg = pd.ewma(return_matrix, halflife=hl_mean)
    stdev = pd.ewmstd(return_matrix, halflife=hl_mean)
    upper = avg + stdev * std_mult
    lower = avg - stdev * std_mult

    if direction == 'down':
        event_filter = np.logical_and(return_matrix < lower, return_matrix < -return_threshold)
    elif direction == 'up':
        event_filter = np.logical_and(return_matrix > upper, return_matrix > return_threshold)
    else:
        event_filter = np.logical_or(np.logical_and(return_matrix < lower, return_matrix < -return_threshold),
                                     np.logical_and(return_matrix > upper, return_matrix > return_threshold))

    return_sigma = (return_matrix.subtract(avg)).div(stdev)
    return event_filter, return_sigma


def event_study(return_matrix, event_filter, window):
    event_pos = return_matrix[event_filter].stack(level=-1, dropna=True)
    event_pre = return_matrix[event_filter].stack(level=-1, dropna=True)
    for i in range(1, window):
        event_pos = pd.concat([event_pos, return_matrix.shift(-i)[event_filter].stack(level=-1, dropna=True)], axis=1)
    for i in range(1, window):
        event_pre = pd.concat([event_pre, return_matrix.shift(i)[event_filter].stack(level=-1, dropna=True)], axis=1)

    event_pos = pd.DataFrame(event_pos.values, columns=range(0, window))
    event_pre = pd.DataFrame(event_pre.ix[:, 1:].ix[:, ::-1].values, columns=range(-window + 1, 0))

    return pd.concat([event_pre, event_pos], axis=1)


# test code
from Research.strategy_wrapper_research import *
import os


d = 3
data = pd.read_csv('C:\\Research\\data\\Dukascopy\\' + files[d], header=0)
data.index = pd.to_datetime(data['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
p = data['Close']

ret = pd.DataFrame(p.pct_change())

event_filter, return_sigma = get_event_filter_price_jumps(return_matrix=ret, hl_mean=1000, std_mult=10, return_threshold=0, direction='up')
event_result = event_study(return_matrix = ret, event_filter = event_filter, window = 200)

event_result.mean().cumsum().plot()