# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt
d = 1
files = os.listdir(r'C:\Research\data\Dukascopy\2017\bid\\')
data_bid = pd.read_csv('C:\\Research\\data\\Dukascopy\\2017\\bid\\' + files[d], header=0)
data_bid.index = pd.to_datetime(data_bid['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")

files = os.listdir(r'C:\Research\data\Dukascopy\2017\ask\\')
data_ask = pd.read_csv('C:\\Research\\data\\Dukascopy\\2017\\ask\\' + files[d], header=0)
data_ask.index = pd.to_datetime(data_ask['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")

data = data_bid.merge(data_ask, how='left', left_index = True, right_index = True)
spread = (data['Close_y'] - data['Close_x']) / (data['Close_y'] + data['Close_x'])

spread.groupby(by=[spread.index.date,spread.index.hour]).mean().to_clipboard()
#############################################################################################
# LMAX quote
quote_data = pd.read_csv(r'C:\Research\data\LMAX data\Research\audusd.csv', header=0)
quote_data.index = pd.to_datetime(quote_data['TIMESTAMP'], unit='ms')
del quote_data['Unnamed: 0']
quote_data.columns = ['time', 'bid', 'bid_size', 'ask', 'ask_size']
del quote_data['time']
quote_data_1t = quote_data.resample('1T').last().fillna(method='pad').shift(1)


spread = quote_data_1t.merge(pd.DataFrame(spread), how='left', left_index = True, right_index = True)
spread['sp'] = (spread['ask'] - spread['bid']) / (spread['ask'] + spread['bid'])


