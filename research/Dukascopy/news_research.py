import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime, timedelta
import research.plot_functions.plot_functions as pf
from research.strategy_wrapper_research import *
import os
import numpy as np
import research.fx_performance as fx
import research.PerformanceMeasures as pm

d=3
symbol = 'eurchf'
country_a = 'European Monetary Union'
country_b = 'Switzerland'
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_' + symbol+'.csv', header=0)
TC_by_hour=(tc['tc']*10000).tolist()

################################################################################################################################
files = os.listdir(r'C:\Research\data\Dukascopy\2016')
data = pd.read_csv('C:\\Research\\data\\Dukascopy\\2016\\' + files[d], header=0)
data.index = pd.to_datetime(data['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
data = data[np.logical_and(np.logical_or(data.index.weekday != 4, data.index.hour < 21), np.logical_and(data.index.weekday != 5, np.logical_or(data.index.weekday != 6, data.index.hour >= 21)))]
p = data['Close']
ret = p.pct_change()
w_all = [0]
alpha_sig = [0]
tc_all = [0]
alpha = AlphaSignal()
transaction_cost = TransactionCostModel(kappa=2)
One_fx_Strategy = FxStrategy(alpha, transaction_cost)
for i in range(1, len(ret)):
    alpha_scale = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour, minute=ret.index[i].minute)
    alpha.get_simple_reversion_alpha(return_t=ret.ix[i], short_ema_pre=alpha.short_ema_t, long_ema_pre=alpha.long_ema_t,
                                     sigma_ema_pre=alpha.sigma_ema_t, alpha_scale=alpha_scale)
    transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=ret.index[i].hour, tc=TC_by_hour, price_t=p.ix[i], commission=0)
    One_fx_Strategy.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy.target_weight_t)
    One_fx_Strategy.target_weight_t = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour, minute=ret.index[i].minute,
                                    scale_factor=One_fx_Strategy.target_weight_t, decay_half_life_pre=45, decay_half_life_post=15, cut_off_minute_pre=0)
    w_all.append(One_fx_Strategy.target_weight_t)
    tc_all.append(transaction_cost.tc)
w = pd.DataFrame(w_all, index=ret.index, columns=['Close'])
tc = pd.DataFrame(tc_all, index=ret.index)
ret = pd.DataFrame(ret, index=ret.index)
strategy_perf = fx.PerformanceCalc(w, ret)
strategy_perf.get_performance_stats(tc[1:].values / 10000)
strategy_perf.show_performance_stats()
net_ret = pd.DataFrame(strategy_perf._net_ret, index = ret[1:].index)
pm.PerformanceStatistics(net_ret,310*60*24,0)
net_ret.columns = [symbol]
################################################################################################################################

net_ret_filter = net_ret.copy()
news = pd.read_csv(r'C:\Research\data\Dukascopy\news_cat.csv', index_col = 'DateTime', parse_dates = True)
news = news[news['Volatility']==3]
news['Country'].unique()
news_sb = news[np.logical_or(news['Country']==country_a, news['Country']==country_b)]
start_time = news_sb.index - timedelta(hours=3)
finish_time = news_sb.index + timedelta(hours=3)
# for i in range(len(start_time)):
#     pf.plot_news(currency=symbol, time=start_time[i], finish_time=finish_time[i], color = 'y', news='news', data = net_ret)
for i in range(len(start_time)):
    net_ret_filter[symbol][start_time[i]:finish_time[i]] = 0

pm.PerformanceStatistics(net_ret,310*60*24,0)
f,ax = plt.subplots()
ax.plot(net_ret_filter.cumsum())
ax.plot(net_ret.cumsum())
###############################################################################################################################
f1,ax1 = plt.subplots()
ax1.plot(p)


net_ret_filter = net_ret.copy()
net_ret_filter[symbol][net_ret_filter.index.hour==21] = 0
pm.PerformanceStatistics(net_ret_filter,310*60*24,0)
net_ret_filter.groupby(net_ret_filter.index.hour).sum().plot(kind='bar')
