import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf

symbols = ['audcad', 'audusd', 'eurchf', 'eurgbp', 'eurusd', 'usdsgd']
files = os.listdir('C:\\Research\\data\\Dukascopy Tick\\')
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_audusd.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()
d = 1
####################################################################################################
data = pd.read_csv('C:\\Research\\data\\Dukascopy Tick\\' + files[d], skiprows=110000000, nrows=1000000, header=0)
data.columns = ['time', 'ask', 'bid', 'ask_size', 'bid_size']
data.index = pd.to_datetime(data['time'].str[0:16])
p = data.bid
high = p.groupby(by=data.index).max()
low = p.groupby(by=data.index).min()
close = p.groupby(by=data.index).last()
open_p = p.groupby(by=data.index).first()
bid = pd.concat([open_p, close, high, low], axis=1)
bid.columns = ['open', 'close', 'high', 'low']
p = data.ask
high = p.groupby(by=data.index).max()
low = p.groupby(by=data.index).min()
close = p.groupby(by=data.index).last()
open_p = p.groupby(by=data.index).first()
ask = pd.concat([open_p, close, high, low], axis=1)
ask.columns = ['open', 'close', 'high', 'low']
####################################################################################################
tick_incremental = -0.0000
p = np.round((ask['close'] + bid['close']) / 2,5)
# p = p[np.logical_and(np.logical_or(p.index.weekday != 4, p.index.hour < 21),np.logical_and(p.index.weekday != 5,
# np.logical_or(p.index.weekday != 6, p.index.hour >= 21)))]
ret = p.pct_change()
alpha = AlphaSignal()
transaction_cost = TransactionCostModel(kappa=2)
# try three different strategies
# 1. limit order
# 2. limit order assume full execution - to asset degree of slippage
# 3. market order
One_fx_Strategy_limit = FxStrategy(alpha, transaction_cost)
w_all_limit, tc_all, alpha_signal_all, fill = ([0], [0], [0], [0])
for i in range(1, len(ret)):
    alpha_scale = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour, minute=ret.index[i].minute)
    alpha.get_simple_reversion_alpha(return_t=ret.ix[i], short_ema_pre=alpha.short_ema_t, long_ema_pre=alpha.long_ema_t,
                                     sigma_ema_pre=alpha.sigma_ema_t, alpha_scale=alpha_scale)
    # transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=ret.index[i].hour, tc=TC_by_hour, price_t=p.ix[i], commission=0)
    transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=0, tc=[0], price_t=p.ix[i], commission=0.12)
    One_fx_Strategy_limit.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy_limit.target_weight_t)

    One_fx_Strategy_limit.get_fill_simulated_weight_tick(weight_target=One_fx_Strategy_limit.target_weight_t,
                                                         weight_current=w_all_limit[-1],
                                                         buy_price_t=p.ix[i - 1] + tick_incremental,
                                                         sell_price_t=p.ix[i - 1] - tick_incremental,
                                                         ask_low_t1=ask['low'].ix[i],
                                                         bid_high_t1=bid['high'].ix[i])

    One_fx_Strategy_limit.target_weight_t = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour,
                                                                                           minute=ret.index[i].minute,
                                                                                           scale_factor=One_fx_Strategy_limit.target_weight_t,
                                                                                           decay_half_life_pre=45,
                                                                                           decay_half_life_post=15)
    fill.append(One_fx_Strategy_limit.filled)
    w_all_limit.append(One_fx_Strategy_limit.target_weight_t)
    tc_all.append(transaction_cost.tc)

w_all_limit = pd.DataFrame(w_all_limit, index=ret.index, columns=['Close'])
fill = pd.DataFrame(fill, index=ret.index, columns=['Close'])
ret = pd.DataFrame(ret, index=ret.index)
tc = pd.DataFrame(tc_all, index=ret.index)
strategy_perf = fx.PerformanceCalc(w_all_limit.shift(1), ret)
strategy_perf.get_performance_stats(tc[1:].values / 10000)
strategy_perf.show_performance_stats()
strategy_perf.store_performance_stats()
net_ret_limit = pd.DataFrame(strategy_perf._net_ret, index=ret[1:].index)
net_ret_limit.cumsum().plot()

ret = p.pct_change()
alpha = AlphaSignal()
One_fx_Strategy_limit = FxStrategy(alpha, transaction_cost)
w_all_limit_exe, tc_all, alpha_signal_all = ([0], [0], [0])
for i in range(1, len(ret)):
    alpha_scale = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour, minute=ret.index[i].minute)
    alpha.get_simple_reversion_alpha(return_t=ret.ix[i], short_ema_pre=alpha.short_ema_t, long_ema_pre=alpha.long_ema_t,
                                     sigma_ema_pre=alpha.sigma_ema_t, alpha_scale=alpha_scale)
    transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=0, tc=[0], price_t=p.ix[i], commission=0.12)
    One_fx_Strategy_limit.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy_limit.target_weight_t)
    One_fx_Strategy_limit.target_weight_t = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour,
                                                                                           minute=ret.index[i].minute,
                                                                                           scale_factor=One_fx_Strategy_limit.target_weight_t,
                                                                                           decay_half_life_pre=45,
                                                                                           decay_half_life_post=15)
    w_all_limit_exe.append(One_fx_Strategy_limit.target_weight_t)
    tc_all.append(transaction_cost.tc)

w_all_limit_exe = pd.DataFrame(w_all_limit_exe, index=ret.index, columns=['Close'])
ret = pd.DataFrame(ret, index=ret.index)
tc = pd.DataFrame(tc_all, index=ret.index)
strategy_perf = fx.PerformanceCalc(w_all_limit_exe.shift(1), ret)
strategy_perf.get_performance_stats(tc[1:].values / 10000)
strategy_perf.show_performance_stats()
strategy_perf.store_performance_stats()
net_ret_limit_exe = pd.DataFrame(strategy_perf._net_ret, index=ret[1:].index)

ret = p.pct_change()
alpha = AlphaSignal()
One_fx_Strategy_limit = FxStrategy(alpha, transaction_cost)
w_all_limit_mkt, tc_all, alpha_signal_all = ([0], [0], [0])
for i in range(1, len(ret)):
    alpha_scale = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour, minute=ret.index[i].minute)
    alpha.get_simple_reversion_alpha(return_t=ret.ix[i], short_ema_pre=alpha.short_ema_t, long_ema_pre=alpha.long_ema_t,
                                     sigma_ema_pre=alpha.sigma_ema_t, alpha_scale=alpha_scale)
    transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=ret.index[i].hour, tc=TC_by_hour,
                                                                   price_t=p.ix[i], commission=0)
    One_fx_Strategy_limit.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy_limit.target_weight_t)
    One_fx_Strategy_limit.target_weight_t = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour,
                                                                                           minute=ret.index[i].minute,
                                                                                           scale_factor=One_fx_Strategy_limit.target_weight_t,
                                                                                           decay_half_life_pre=45,
                                                                                           decay_half_life_post=15)
    w_all_limit_mkt.append(One_fx_Strategy_limit.target_weight_t)
    tc_all.append(transaction_cost.tc)

w_all_limit_mkt = pd.DataFrame(w_all_limit_mkt, index=ret.index, columns=['Close'])
ret = pd.DataFrame(ret, index=ret.index)
tc = pd.DataFrame(tc_all, index=ret.index)
strategy_perf = fx.PerformanceCalc(w_all_limit_mkt.shift(1), ret)
strategy_perf.get_performance_stats(tc[1:].values / 10000)
strategy_perf.show_performance_stats()
strategy_perf.store_performance_stats()
net_ret_limit_mkt = pd.DataFrame(strategy_perf._net_ret, index=ret[1:].index)

####################################################################################################
# analysis
returns = pd.concat([net_ret_limit, net_ret_limit_exe, net_ret_limit_mkt], axis=1)
returns.columns = ['limit', 'limit_exe', 'mkt']
pm.PerformanceStatistics(returns, 310 * 60 * 24, 0)
(50000*0.2 * returns.cumsum()).plot()

fill_rate = (w_all_limit.diff() != 0).sum() / (w_all_limit_exe.diff() != 0).sum()
print('fill rate : ' + str(fill_rate))
f, ax = plt.subplots()
ax.plot(w_all_limit)
ax.plot(w_all_limit_exe)
ax.plot(w_all_limit_mkt)

f, ax = plt.subplots()
pf.position_monitor_plot(ax, w_all_limit_exe['Close'], w_all_limit['Close'], w_all_limit.diff()['Close'],
                         w_all_limit.index)

# net_ret1 = net_ret.copy()
# w1 = w.copy()
# ################################################################################################################################
# path = 'C:\\Research\\data\\log limit\\'
# files = os.listdir(path)
# data_ori = pd.read_csv(path + files[d], header=0)
# data_limit = data_ori[np.logical_and(data_ori['event'] != 'event', data_ori['event'] != 'Tick')]
# data_limit.index = pd.to_datetime(data_limit.time.str[:-1], format='%Y%m%d-%H:%M:%S.%f')
# data_limit[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price', 'tc','alpha','alpha scale']] = \
#     data_limit[['bid', 'ask', 'current position', 'target position', 'traded position', 'target price', 'trade price', 'tc','alpha','alpha scale']].astype(float)
# data_limit['tc'].where(data_limit['tc']!=0,np.nan, inplace = True)
# data_limit['tc'].fillna(method = 'ffill', inplace=True)
# data_limit['mid'] = 0.5 * data_limit['bid'] + 0.5 * data_limit['ask']
# data_limit['holding_return'] = (data_limit['mid'] - data_limit['mid'].shift(1)) * data_limit['current position'].shift(1)
# data_limit['slippage'] = (data_limit['mid'] - data_limit['trade price']) * data_limit['traded position']
# data_limit['actual_return'] = data_limit['holding_return'] + data_limit['slippage']
# data_limit['target position'].where(data_limit['target position'] != 0, None, inplace=True)
# data_limit['target position'].fillna(method='ffill', inplace=True)
# data_limit['theory_return'] = (data_limit['mid'] - data_limit['mid'].shift(1)) * data_limit['target position'].shift(1)
#
# ################################################################################################################################
#
# start = '2017-03-07 14:30:00'
# end = '2017-03-07 15:00:00'
# f,ax = plt.subplots()
# ax.plot((net_ret[start:end].cumsum()*50000*1))
# ax.plot((data_limit['actual_return'].cumsum()-data_limit['theory_return'].cumsum()))
# ax.plot((data_limit['theory_return'].cumsum()))
#
# f,ax = plt.subplots()
# ax.plot((w[start:end]*50000*1))
# ax.plot((data_limit['current position'])-(data_limit['target position']))
# ax.plot()
#
# f,ax = plt.subplots()
# ax.plot(p[start:end])
# ax.plot(ask['low'][start:end])
# ax.plot(bid['high'][start:end])
#
# f,ax = plt.subplots()
# ax.plot(data_limit['current position'][start:end])
# ax.plot(data_limit['target position'][start:end])
