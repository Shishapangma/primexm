# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.fx_performance as fx
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt


files = os.listdir(r'C:\Research\data\Dukascopy\2017')
all = pd.DataFrame()

symbols = ['audusd','eurchf','usdsgd']
multi = [0.3, 1, 0.5]


for d in range(0,3):
    tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_'+symbols[d]+'.csv', header=0)
    TC_by_hour=(tc.ix[:,0]*10000).tolist()
    data = pd.read_csv('C:\\Research\\data\\Dukascopy\\2017\\' + files[d], header=0)
    data.index = pd.to_datetime(data['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
    data = data[np.logical_and(np.logical_or(data.index.weekday != 4, data.index.hour < 21),
                               np.logical_and(data.index.weekday != 5,
                                              np.logical_or(data.index.weekday != 6, data.index.hour >= 21)))]
    p = data['Close']
    ret = p.pct_change()
    hr = p.index.hour
    w_all = [0]
    alpha_sig = [0]
    tc_all = [0]
    alpha = AlphaSignal()
    transaction_cost = TransactionCostModel(kappa=2)
    One_fx_Strategy = FxStrategy(alpha, transaction_cost)

    for i in range(1, len(ret)):
        alpha_scale = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour,
                                                                     minute=ret.index[i].minute,
                                                                     scale_factor=10,
                                                                     overnight_hour_pre=21,
                                                                     overnight_hour_post=22,
                                                                     decay_half_life_pre=10,
                                                                     decay_half_life_post=10,
                                                                     cut_off_minute_pre=45,
                                                                     cut_off_minute_post=15)
        alpha.get_simple_reversion_alpha(return_t=ret.ix[i],
                                         short_ema_pre=alpha.short_ema_t,
                                         long_ema_pre=alpha.long_ema_t,
                                         sigma_ema_pre=alpha.sigma_ema_t,
                                         short_half_life=20,
                                         long_half_life=200,
                                         long_half_life_risk=200,
                                         alpha_winsorize=10,
                                         alpha_scale=alpha_scale)

        transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=ret.index[i].hour, tc=TC_by_hour, price_t=p.ix[i],
                                                                       commission=0)

        One_fx_Strategy.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy.target_weight_t)

        One_fx_Strategy.target_weight_t = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour,
                                                                     minute=ret.index[i].minute,
                                                                     scale_factor=One_fx_Strategy.target_weight_t,
                                                                     overnight_hour_pre=21,
                                                                     overnight_hour_post=22,
                                                                     decay_half_life_pre=45,
                                                                     decay_half_life_post=15,
                                                                     cut_off_minute_pre=45,
                                                                     cut_off_minute_post=15)
        w_all.append(One_fx_Strategy.target_weight_t)
        alpha_sig.append(alpha.alpha_signal_t)
        tc_all.append(transaction_cost.tc)

    w = pd.DataFrame(w_all, index=ret.index, columns=['Close'])

    alpha_sig = pd.DataFrame(alpha_sig, index=ret.index, columns=['Close'])
    tc = pd.DataFrame(tc_all, index=ret.index)
    ret = pd.DataFrame(ret, index=ret.index)
    strategy_perf = fx.PerformanceCalc(w, ret)
    strategy_perf.get_performance_stats(tc[1:].values / 10000)
    strategy_perf.show_performance_stats()
    strategy_perf.store_performance_stats()

    all = pd.concat([all, pd.DataFrame(strategy_perf._net_ret,index=ret[1:].index)], axis=1)

all.columns = symbols
sr = pm.PerformanceStatistics(all, 60 * 24 * 310, 0)[2].values

w = multi
w = [0.3/0.77,1/0.71,0.5/1.09]
w = [0,1,0]
((all*w)['2016-01-25 22:00:00':'2017-03-02 23:00:00'].sum(axis=1).cumsum()*50000).plot()
