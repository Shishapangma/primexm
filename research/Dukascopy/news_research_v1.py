import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf
from statsmodels.tsa.stattools import acf, pacf
import statsmodels.formula.api as smf
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
sym = 'eurchf'
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_' + sym + '.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()
###############################################################################################################################
d=1
files = os.listdir(r'C:\Research\data\Dukascopy\2017Apr')
data_duka = pd.read_csv('C:\\Research\\data\\Dukascopy\\2017Apr\\' + files[d], header=0)
data_duka.index = pd.to_datetime(data_duka['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
data_duka = pm.fx_data_cleaning(data_duka)
p = data_duka['Close']
strategy = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy.get_strategy_weight_for_sample(order_type='mkt', signal=p.pct_change(), estimated_tc=TC_by_hour,
                                        ask_low=p, bid_high=p, mid_price=p, tick_incremental=0.0000,
                                        commission=0.13, nav=1, returns=pd.Series(p.pct_change().values, p.index),
                                        alpha_type='simple')

(strategy.net_return).cumsum().plot()
print(pm.PerformanceStatistics(strategy.net_return, 300 * 60 * 24, 0))
#########################################################################################################
# country = 'United States'
# path = r'C:\Research\data\LMAX data\Research\\'
# files = os.listdir(path)
# data = pd.read_csv(path + sym+ '.csv', header=0)
# data.index = pd.to_datetime(data['TIMESTAMP'], unit='ms')
# del data['Unnamed: 0']
# data.columns = ['time','bid','bid_size','ask','ask_size']

#########################################################################################################
country = 'United States1'
country1 = 'European Monetary Union'
# important_news = ['RBA Interest Rate Decision','Fed Interest Rate Decision','Nonfarm Payrolls','FOMC Minutes','Consumer Price Index (YoY)']
important_news = ['Nonfarm Payrolls','FOMC Minutes','Consumer Price Index (YoY)']
news = pd.read_csv(r'C:\Research\data\Dukascopy\events.csv', index_col = 'DateTime', parse_dates = True)
news = news[np.logical_or(news['Country']==country, news['Country']==country1)]
news = news[news['Volatility']==3]

select_news = pd.DataFrame()
for i in range(0,len(important_news)):
    select_news  = pd.concat([select_news, news[news['Name']==important_news[i]]],axis=0)
event = select_news.index.unique()
start_time = event - timedelta(hours=1)
finish_time = event + timedelta(hours=1)


net_ret_filter = strategy.net_return.copy()
for i in range(len(start_time)):
    net_ret_filter[start_time[i]:finish_time[i]] = 0
pm.PerformanceStatistics(net_ret_filter,310*60*24,0)


for i in range(len(start_time)):
    pf.plot_news(currency='net_return', time=start_time[i], finish_time=finish_time[i], color = 'y', news='news', data = strategy.net_return)

plt.figure()
plt.plot(net_ret_filter.cumsum())
plt.plot(strategy.net_return.cumsum())

net_ret_filter1 = net_ret_filter['2016-03-26':]
net_ret_filter1.groupby(net_ret_filter1.index.hour).sum().plot(kind='bar')