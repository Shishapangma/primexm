# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.fx_performance as fx
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt

d = 6
symbols = ['audusd', 'eurchf', 'eurgbp', 'eurusd', 'gbpusd', 'nzdusd', 'usdsgd']
multi = [0.25, 0.9, 0.3, 0.25, 0.75]
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_gb\tc_' + symbols[d] + '.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()

hour_skip_ao = ['0:00:00', '18:00:00', '19:00:00', '20:00:00', '21:00:00', '22:00:00']
hour_skip_oa = ['0:00:00', '19:00:00', '20:00:00', '21:00:00', '22:00:00', '23:00:00']
#
# hour_skip_ao = ['7:00:00', '8:00:00', '21:00:00', '22:00:00']
# hour_skip_oa = ['8:00:00', '9:00:00', '22:00:00', '23:00:00']

# hour_skip_ao = ['21:00:00', '22:00:00']
# hour_skip_oa = ['22:00:00', '23:00:00']
###############################################################################################################################
files = os.listdir(r'C:\Research\data\Dukascopy\2017\bid\\')
data_bid = pd.read_csv('C:\\Research\\data\\Dukascopy\\2017\\bid\\' + files[d], header=0)
data_bid.index = pd.to_datetime(data_bid['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
data_bid = pm.fx_data_cleaning(data_bid)
p = data_bid['Close']


strategy= CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy.get_strategy_weight_for_sample(order_type='mkt', signal=p.pct_change(), estimated_tc=TC_by_hour,
                                        ask_low=p, bid_high=p, mid_price=p, bid_price=p, ask_price=p,
                                        tick_incremental=pd.Series(0.0000, index=p.index),
                                        commission=0.05, nav=1, returns=pd.Series(p.pct_change().values, p.index),
                                        alpha_type='simple', tc_type='hour', hour_select_ao=hour_skip_ao, hour_select_oa=hour_skip_oa)
(strategy.net_return).cumsum().plot()
print(pm.PerformanceStatistics(strategy.net_return, 300 * 60 * 24, 0))

strategy.net_return.resample('1D').sum().to_clipboard()



