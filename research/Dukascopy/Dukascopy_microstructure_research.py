import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx

import research.PerformanceMeasures as pm
files = os.listdir(r'C:\Research\data\Dukascopy Tick')

d = 0
data = pd.read_csv('C:\\Research\\data\\Dukascopy Tick\\' + files[d], skiprows=100000000, nrows=10000000, header=0)
data.columns = ['time', 'ask', 'bid', 'ask_size', 'bid_size']
data.index = pd.to_datetime(data['time'].str[0:19])

data = data['2016-03-08':'2016-03-12']

p = data.bid
ret =p.resample('60S').pct_change()

w_all = [0]
alpha_sig = [0]
tc_all = [0]
ss = [0]
bs = [0]
alpha = AlphaSignal()
transaction_cost = TransactionCostModel(kappa=2)
One_fx_Strategy = FxStrategy(alpha, transaction_cost)

for i in range(1, len(ret)):
    alpha_scale = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour,
                                                                 minute=ret.index[i].minute,
                                                                 scale_factor=10,
                                                                 overnight_hour_pre=21,
                                                                 overnight_hour_post=22,
                                                                 decay_half_life_pre=10,
                                                                 decay_half_life_post=10,
                                                                 cut_off_minute_pre=45,
                                                                 cut_off_minute_post=15)
    alpha.get_simple_reversion_alpha(return_t=ret.ix[i],
                                     short_ema_pre=alpha.short_ema_t,
                                     long_ema_pre=alpha.long_ema_t,
                                     sigma_ema_pre=alpha.sigma_ema_t,
                                     short_half_life=20,
                                     long_half_life=200,
                                     long_half_life_risk=200,
                                     alpha_winsorize=10,
                                     alpha_scale=alpha_scale)

    transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=0, tc=[0.1],
                                                                   price_t=p.ix[i],
                                                                   commission=0)

    One_fx_Strategy.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy.target_weight_t)

    One_fx_Strategy.target_weight_t = alpha.get_alpha_scale_decay_for_overnight_hour(hour=ret.index[i].hour,
                                                                                     minute=ret.index[i].minute,
                                                                                     scale_factor=One_fx_Strategy.target_weight_t,
                                                                                     overnight_hour_pre=21,
                                                                                     overnight_hour_post=22,
                                                                                     decay_half_life_pre=45,
                                                                                     decay_half_life_post=15,
                                                                                     cut_off_minute_pre=45,
                                                                                     cut_off_minute_post=15)
    w_all.append(One_fx_Strategy.target_weight_t)
    ss.append(One_fx_Strategy.sell_price_stop)
    bs.append(One_fx_Strategy.buy_price_stop)
    alpha_sig.append(alpha.alpha_signal_t)
    tc_all.append(transaction_cost.tc)

w = pd.DataFrame(w_all, index=ret.index, columns=['Close'])
w.where(w < 5, 5, inplace=True)
w.where(w > -5, -5, inplace=True)
alpha_sig = pd.DataFrame(alpha_sig, index=ret.index, columns=['Close'])
tc = pd.DataFrame(tc_all, index=ret.index)
ret = pd.DataFrame(ret, index=ret.index)
strategy_perf = fx.PerformanceCalc(w, ret)
strategy_perf.get_performance_stats(tc[1:].values / 10000)
strategy_perf.show_performance_stats()
strategy_perf.store_performance_stats()


var_s = pd.ewmvar(p.resample('60S').pct_change(), halflife=20)
var_m = pd.ewmvar(p.resample('300S').pct_change(), halflife=4)
var_m = pd.DataFrame(var_m, index = var_s.index).fillna(method='pad')

(pd.DataFrame(var_s)/var_m)

import matplotlib.pyplot as plt
fig1 = plt.figure()
ax1 = fig1.add_subplot(311)
ax1.plot()
ax2 = fig1.add_subplot(312)
ax2.plot((p))
ax3 = fig1.add_subplot(313)
ax3.plot(pd.DataFrame(strategy_perf._net_ret, index=ret[1:].index).cumsum())