import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from Research.strategy_wrapper_research import *
import Research.fx_performance as fx
import matplotlib.pyplot as plt
import Research.plot_functions.plot_functions as pf

files = os.listdir(r'C:\Research\data\Dukascopy Tick')

d = 0
TC = [0.1]
data = pd.read_csv('C:\\Research\\data\\Dukascopy Tick\\' + files[d], skiprows=130000000, nrows=4000000, header=0)
data.columns = ['time', 'ask', 'bid', 'ask_size', 'bid_size']
data.index = pd.to_datetime(data['time'].str[0:16])
data['bid'] = data['bid'] + 0.00007
data['ask'] = data['ask'] - 0.00007

p = data.bid
high = p.groupby(by=data.index).max()
low = p.groupby(by=data.index).min()
close = p.groupby(by=data.index).last()
open_p = p.groupby(by=data.index).first()
avg = p.groupby(by=data.index).mean()
bid = pd.concat([open_p, close, high, low, avg], axis=1)
bid.columns = ['open', 'close', 'high', 'low', 'avg']
p = data.ask
high = p.groupby(by=data.index).max()
low = p.groupby(by=data.index).min()
close = p.groupby(by=data.index).last()
open_p = p.groupby(by=data.index).first()
avg = p.groupby(by=data.index).mean()
ask = pd.concat([open_p, close, high, low, avg], axis=1)
ask.columns = ['open', 'close', 'high', 'low', 'avg']

# fill simulate or not
tick_incremental = -0.0000

p = (ask['close'] + bid['close']) / 2
p = p[np.logical_and(np.logical_or(p.index.weekday != 4, p.index.hour < 21), np.logical_and(p.index.weekday != 5,
                                                                                            np.logical_or(
                                                                                                    p.index.weekday != 6,
                                                                                                    p.index.hour >= 21)))]
ret = p.pct_change()
alpha = AlphaSignal()
transaction_cost = TransactionCostModel(kappa=2)
One_fx_Strategy = FxStrategy(alpha, transaction_cost)
w_all = [0]
tc_all = [0]
alpha_signal_all = [0]
for i in range(1, len(ret)):
    alpha.get_simple_reversion_alpha(return_t=ret.ix[i],
                                     short_ema_pre=alpha.short_ema_t,
                                     long_ema_pre=alpha.long_ema_t,
                                     sigma_ema_pre=alpha.sigma_ema_t,
                                     short_half_life=20,
                                     long_half_life=200,
                                     long_half_life_risk=200,
                                     alpha_winsorize=10,
                                     alpha_scale=10)

    # transaction_cost.get_simple_linear_transaction_cost_hour_based(hour=0, TC_by_hour=TC,
    #                                                                price_t=bid['close'].ix[i - 1],
    #                                                                commission=0)

    transaction_cost.get_simple_linear_transaction_cost_market_based(bid_pre_avg=bid['avg'].ix[i - 1],
                                                                     ask_pre_avg=ask['avg'].ix[i - 1], commission=0)

    One_fx_Strategy.get_weight_simple_alpha_with_tc(weight_current=One_fx_Strategy.target_weight_t)
    w_all.append(One_fx_Strategy.target_weight_t)
    tc_all.append(transaction_cost.tc)
    alpha_signal_all.append(alpha.alpha_signal_t)

w = pd.DataFrame(w_all, index=ret.index, columns=['target position'])
w.where(w < 5, 5, inplace=True)
w.where(w > -5, -5, inplace=True)
out_data = pd.merge(data, w.shift(1), how='left', left_index=True, right_index=True)
out_data.index = pd.to_datetime(data['time'])
out_data['mid'] = 0.5 * out_data['bid'] + 0.5 * out_data['ask']
out_data['target trade position'] = out_data['target position'].diff()
out_data['target price'] = np.nan
out_data['target price'].where(out_data['target trade position'] == 0, out_data['mid'], inplace=True)
out_data['target price'].fillna(method='ffill', inplace=True)

# w_current = [0]
#
# for i in range(1, len(out_data)):
#     One_fx_Strategy.get_fill_simulated_weight_tick(weight_target=out_data['target position'].ix[i],
#                                                    weight_current=w_current[-1],
#                                                    buy_price_t=out_data['target price'].ix[i] + tick_incremental,
#                                                    sell_price_t=out_data['target price'].ix[i] - tick_incremental,
#                                                    ask_low_t1=out_data['ask'].ix[i],
#                                                    bid_high_t1=out_data['bid'].ix[i])
#     w_current.append(One_fx_Strategy.target_weight_t)

# out_data['current position'] = pd.DataFrame(w_current, index=out_data.index, columns=['current position'])
out_data['current position'] = out_data['target position']
out_data['traded position'] = out_data['current position'].diff()
out_data['trade price'] = out_data['target price']
out_data['trade price'].where(out_data['traded position'] != 0, 0, inplace=True)
out_data['trade price'].where(out_data['traded position'] <= 0, out_data.ask, inplace=True)
out_data['trade price'].where(out_data['traded position'] >= 0, out_data.bid, inplace=True)
out_data['holding_return'] = (out_data['mid'] - out_data['mid'].shift(1)) * out_data['current position'].shift(1)
out_data['slippage'] = (out_data['mid'].shift(1) - out_data['trade price'].shift(1)) * out_data[
    'traded position'].shift(1)
out_data['trading_return'] = (out_data['mid'] - out_data['mid'].shift(1)) * out_data['traded position'].shift(1)
out_data['actual_return'] = out_data['holding_return'] + out_data['trading_return'] + out_data['slippage']
out_data['theory_return'] = (out_data['mid'] - out_data['mid'].shift(1)) * out_data['target position'].shift(1)

f, (ax1, ax2, ax3) = plt.subplots(3, figsize=(22, 15))
f.suptitle(files[d][0:7], fontsize=14)
bid = out_data['bid']
ask = out_data['ask']
mid = out_data['mid']
trade_price = out_data['trade price']
trade_price.where(trade_price != 0, np.nan, inplace=True)
x = out_data.index
trade_position = out_data['traded position']
current_position = out_data['current position']
target_position = out_data['target position']
system_PnL = out_data['actual_return']
theory_return = out_data['theory_return']
actual_return = out_data['actual_return']
trading_return = out_data['trading_return']
holding_return = out_data['holding_return']
slippage = out_data['slippage']

pf.market_trade_monitor_plot(ax1, bid, ask, mid, trade_price, trade_position, x)
pf.position_monitor_plot(ax2, target_position, current_position, trade_position, x)
pf.peformance_monitor_plot(ax3, system_PnL, theory_return, actual_return, trading_return, holding_return, slippage)
