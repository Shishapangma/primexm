# test code
from research.strategy_wrapper_research import *
import os
import pandas as pd
import numpy as np
import research.fx_performance as fx
import research.PerformanceMeasures as pm
import matplotlib.pyplot as plt

data = pd.read_clipboard()
sym = 'audusd'
temp = 'audusd1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 1, data['hour'] == 2), data['hour'] == 7), data['hour'] == 14))] = 0
data[temp][np.logical_and(np.logical_or(data['month'] > 10, data['month'] <= 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 2, data['hour'] == 3), data['hour'] == 8), data['hour'] == 15))] = 0

sym = 'eurchf'
temp = 'eurchf1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 7, data['hour'] == 8), data['hour'] == 7), data['hour'] == 8))] = 0

sym = 'usdchf'
temp = 'usdchf1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 7, data['hour'] == 8), data['hour'] == 7), data['hour'] == 8))] = 0
data[temp][np.logical_and(np.logical_or(data['month'] > 10, data['month'] <= 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 8, data['hour'] == 9), data['hour'] == 8), data['hour'] == 9))] = 0

sym = 'usdchf'
temp = 'usdchf1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 7, data['hour'] == 8), data['hour'] == 7), data['hour'] == 8))] = 0
data[temp][np.logical_and(np.logical_or(data['month'] > 10, data['month'] <= 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 8, data['hour'] == 9), data['hour'] == 8), data['hour'] == 9))] = 0


sym = 'usdsgd'
temp = 'usdsgd1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 0, data['hour'] == 0), data['hour'] == 0), data['hour'] == 0))] = 0
data[temp][np.logical_and(np.logical_or(data['month'] > 10, data['month'] <= 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 0, data['hour'] == 0), data['hour'] == 0), data['hour'] == 0))] = 0

sym = 'eurusd'
temp = 'eurusd1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 7, data['hour'] == 8), data['hour'] == 13), data['hour'] == 14))] = 0
data[temp][np.logical_and(np.logical_or(data['month'] > 10, data['month'] <= 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 8, data['hour'] == 9), data['hour'] == 14), data['hour'] == 15))] = 0


sym = 'eurgbp'
temp = 'eurgbp1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 7, data['hour'] == 8), data['hour'] == 13), data['hour'] == 14))] = 0
data[temp][np.logical_and(np.logical_or(data['month'] > 10, data['month'] <= 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 8, data['hour'] == 9), data['hour'] == 14), data['hour'] == 15))] = 0

sym = 'gbpusd'
temp = 'gbpusd1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 7, data['hour'] == 8), data['hour'] == 13), data['hour'] == 14))] = 0
data[temp][np.logical_and(np.logical_or(data['month'] > 10, data['month'] <= 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 8, data['hour'] == 9), data['hour'] == 14), data['hour'] == 15))] = 0

sym = 'nzdusd'
temp = 'nzdusd1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 2, data['hour'] == 2), data['hour'] == 13), data['hour'] == 14))] = 0
data[temp][np.logical_and(np.logical_or(data['month'] > 10, data['month'] <= 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 1, data['hour'] == 1), data['hour'] == 14), data['hour'] == 15))] = 0

sym = 'audnzd'
temp = 'audnzd1'
data[temp] = data[sym]
data[temp][np.logical_and(np.logical_and(data['month'] <= 10, data['month'] > 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 1, data['hour'] == 2), data['hour'] == 14), data['hour'] == 14))] = 0
data[temp][np.logical_and(np.logical_or(data['month'] > 10, data['month'] <= 4), np.logical_or(
        np.logical_or(np.logical_or(data['hour'] == 2, data['hour'] == 3), data['hour'] == 15), data['hour'] == 15))] = 0

data[data['hour']==22]=0
data[data['hour']==23]=0
start = 1
print(pm.PerformanceStatistics(data[temp].ix[start:], 300 * 24, 0))
print(pm.PerformanceStatistics(data[sym].ix[start:], 300 * 24, 0))
print(pm.PerformanceStatistics(data[temp][start:]*2 - data[sym][start:], 300 * 24, 0))
plt.plot(data[temp].ix[start:].cumsum())
plt.plot(data[sym].ix[start:].cumsum())
plt.plot(data[temp][start:].cumsum() - data[sym][start:].cumsum())

data.index = pd.to_datetime(data['Time (UTC)'])
data.resample('1D').sum().corr().to_clipboard()

del data['Time (UTC)']
del data['month']
del data['hour']
r = pm.PerformanceStatistics(data,300*24,0)



