import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf


files = os.listdir('C:\\Research\\data\\Dukascopy Tick\\')
d = 1
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_audusd.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()
TC_by_hour = np.zeros(24).tolist()
####################################################################################################
# data = pd.read_csv('C:\\Research\\data\\Dukascopy Tick\\' + files[d], skiprows=110000000, nrows=10000000, header=0)
# data.columns = ['time', 'ask', 'bid', 'ask_size', 'bid_size']
# data.index = pd.to_datetime(data['time'])
# Data_Minute = data.resample('10S').last()
# ####################################################################################################
data1 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-12-21-05-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data2 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-13-21-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data3 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-14-21-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data4 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-15-21-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data5 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-16-21-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data = pd.concat([data1,data2,data3,data4,data5],axis=0)
# data1 = pd.read_csv(r'C:\Research\output\limit order simulation\2017-03-07-22-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
# data2 = pd.read_csv(r'C:\Research\output\limit order simulation\2017-03-08-22-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
# data = pd.concat([data1,data2],axis=0)
data.index = pd.to_datetime(data['TIMESTAMP'], unit='ms')
data.columns = ['time', 'bid', 'bid_q','ask','ask_q']
Data_Minute = data.resample('60S').last().shift(1)
ask_low = data.resample('60S').min()['ask'].shift(1)
bid_high = data.resample('60S').max()['bid'].shift(1)
# ####################################################################################################
p = np.round((Data_Minute['ask'] + Data_Minute['bid']) / 2, 5)
TC_by_hour = np.zeros(24).tolist()

strategy = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=4)))
strategy.get_strategy_weight_for_sample(order_type='lmt', signal=p.pct_change(), estimated_tc=TC_by_hour,
                                        ask_low=ask_low, bid_high=bid_high, mid_price=p, tick_incremental=0,
                                        commission=0.12, nav=50000, returns=p.diff())
strategy.net_return.cumsum().plot()
strategy.turnover.sum()
pm.PerformanceStatistics(strategy.net_return,60*24*250,0)
#########################################################################################################################
# data_1s = pd.concat([data['bid'].resample('1S').max(),data['ask'].resample('1S').min()],axis=1)

data_weight = data.join(strategy.wgt, how='outer')
data_weight.fillna(method='pad', inplace=True)
data_weight['position'] = np.round(data_weight['position'], 0)
data_weight['position'].where(data_weight['position'].notnull(),0,inplace=True)
current = [0]
order_price = [0]
trade_quantity = [0]
cancelled_trade = [0]
data_weight = data_weight
fill_simulate = FillSimulator()
for i in range(1, len(data_weight)):
    print(i)
    isrebalance = (data_weight['position'][i] - data_weight['position'][i - 1]) != 0
    # isrebalance = data_weight.index[i].second in [0,10,20,30,40,50]
    fill_simulate.get_fill_simulated_position(isrebalance=isrebalance,
                                              target_position_t=data_weight['position'][i],
                                              target_position_tm1=data_weight['position'][i - 1],
                                              current_position_tm1=current[-1],
                                              tick=0.0000,
                                              ask_t=data_weight['ask'][i], bid_t=data_weight['bid'][i])
    current.append(fill_simulate.current_position_t)
    order_price.append(fill_simulate.order_price)
    trade_quantity.append(fill_simulate.trade_quantity)
    cancelled_trade.append(fill_simulate.cancelled)

current = pd.DataFrame(current, index=data_weight.index, columns=['current'])
order_price = pd.DataFrame(order_price, index=data_weight.index, columns=['order price'])
trade_quantity = pd.DataFrame(trade_quantity, index=data_weight.index, columns=['trade quantity'])
cancelled_trade = pd.DataFrame(cancelled_trade, index=data_weight.index, columns=['cancelled_trade'])

data_weight = pd.concat([data_weight, current, order_price, trade_quantity, cancelled_trade], axis=1)
data_weight['mid'] = np.round(0.5 * data_weight['bid'] + 0.5 * data_weight['ask'], 5)

data_weight['holding_return'] = (data_weight['mid'] - data_weight['mid'].shift(1)) * (data_weight['current'].shift(1))
data_weight['slippage'] = (data_weight['mid'] - data_weight['order price']) * (data_weight['current'].diff()) - 0.000012 * (data_weight['current'].diff().abs())
data_weight['actual_return'] = data_weight['holding_return'] + data_weight['slippage']
data_weight['theory_slippage'] = (data_weight['mid'] - data_weight['ask']) * (data_weight['position'].diff().abs()) - 0.000012 * (data_weight['position'].diff().abs())
data_weight['theory_return'] = (data_weight['mid'] - data_weight['mid'].shift(1)) * data_weight['position'].shift(1) + data_weight['theory_slippage']

data_weight['actual_return'].cumsum().plot()
data_weight['theory_return'].cumsum().plot()


data_weight['actual_return'].sum()
data_weight['theory_return'].sum()

(data_weight['position'].diff().abs()).sum()
(data_weight['current'].diff().abs()).sum()

data_weight['theory_slippage'].sum()
data_weight['slippage'].sum()

pm.PerformanceStatistics(data_weight['theory_return'],60*60*24*300,0)
pm.PerformanceStatistics(data_weight['actual_return'],60*60*24*300,0)

data_weight['position'].plot()
data_weight['current'].plot()

f, ax = plt.subplots()
ax.plot(data_weight['theory_return'].cumsum())
ax.plot(strategy.net_return.cumsum())