import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf

#############################################################################################
# LMAX quote
quote_data = pd.read_csv(r'C:\Research\data\LMAX data\Research\eurchf.csv', header=0)
quote_data.index = pd.to_datetime(quote_data['TIMESTAMP'], unit='ms')
del quote_data['Unnamed: 0']
quote_data.columns = ['time', 'bid', 'bid_size', 'ask', 'ask_size']
del quote_data['time']
quote_data_1t = quote_data.resample('1T').last().fillna(method='pad').shift(1)

#############################################################################################
# Duka quote
data_duka = pd.read_csv('C:\\Research\\data\\Dukascopy\\2017\\NZDUSD_UTC_1 Min_Bid_2017.01.01_2017.05.19.csv', header=0)
data_duka.index = pd.to_datetime(data_duka['Time (UTC)'], format="%Y.%m.%d %H:%M:%S")
data_duka = pm.fx_data_cleaning(data_duka)
p = pd.DataFrame(data_duka['Close'])


#############################################################################################
# get trade data
trade_data = pd.read_csv(r'C:\Research\data\LMAX data\trade data\jan-may-2017\data_all.csv', header=0)
trade_data = trade_data[trade_data['symbol'] == 'NZD/USD']
trade_data.index = pd.to_datetime(trade_data['time'])
trade_data_1t = trade_data.resample('1T').sum().shift(1)

#############################################################################################
# merge
quote_data_1t = p.merge(trade_data_1t, how='left', left_index = True, right_index = True)
quote_data_1t['quantity'][quote_data_1t['quantity'].isnull()]=0

# Analysis
exvol = pm.time_series_standardise(quote_data_1t['quantity'], st = 1, lt = 5)
ret = quote_data_1t['Close'].pct_change()
alpha = -pm.time_series_standardise(ret, st = 20, lt = 200)
b = 0.5 + 0.5*np.tanh(-exvol)
ret_b = quote_data_1t['Close'].pct_change() * b
alpha_b =  -pm.time_series_standardise(ret_b, st = 20, lt = 200)

str_ret_a = (alpha.shift(1) * ret)
str_ret_b = (alpha_b.shift(1) * ret)
str_ret_a.cumsum().plot()
str_ret_b.cumsum().plot()
(str_ret_b - str_ret_a).cumsum().plot()
print(pm.PerformanceStatistics(str_ret_a, 60 * 24 * 300, 0))
print(pm.PerformanceStatistics(str_ret_b, 60 * 24 * 300, 0))

print(pd.DataFrame(alpha).corrwith(pd.DataFrame(alpha).shift(1)))
print(pd.DataFrame(alpha_b).corrwith(pd.DataFrame(alpha_b).shift(1)))


#############################################################################################
# backtest
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_lmax\tc_nzdusd.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()

hour_skip_ao = ['21:00:00', '22:00:00']
hour_skip_oa = ['22:00:00', '23:00:00']
strategy = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy.get_strategy_weight_for_sample(order_type='mkt', signal=alpha, estimated_tc=TC_by_hour,
                                        ask_low=p['Close'], bid_high=p['Close'], mid_price=p['Close'], tick_incremental=0.0000,
                                        commission=0.09, nav=1, returns=pd.Series(p['Close'].pct_change().values, p.index),
                                        alpha_type='custom', hour_select_ao=hour_skip_ao, hour_select_oa=hour_skip_oa)

(strategy.net_return).cumsum().plot()
print(pm.PerformanceStatistics(strategy.net_return, 300 * 60 * 24, 0))