import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf


files = os.listdir('C:\\Research\\data\\Dukascopy Tick\\')
d = 1
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_audusd.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()
####################################################################################################
# data = pd.read_csv('C:\\Research\\data\\Dukascopy Tick\\' + files[d], skiprows=110000000, nrows=10000000, header=0)
# data.columns = ['time', 'ask', 'bid', 'ask_size', 'bid_size']
# data.index = pd.to_datetime(data['time'])
# Data_Minute = data.resample('10S').last()

# ####################################################################################################
data1 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-12-21-05-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data2 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-13-21-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data3 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-14-21-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data4 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-15-21-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data5 = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\2017-03-16-21-00-00-000-4007-bid-ask-tick-depth1.csv', header=0)
data = pd.concat([data1,data2,data3,data4,data5],axis=0)
data.index = pd.to_datetime(data['TIMESTAMP'], unit='ms')
data.columns = ['time','bid','bid_size','ask','ask_size']
trade = pd.read_csv(r'C:\Research\output\limit order simulation\Tick AUDUSD\AUDUSD trade data - 13th-17th March.csv', header=0)
trade.index = pd.to_datetime(trade['time'], format='%d/%m/%Y %H:%M:%S.%f')
data = data.join(trade[['rate','quantity']], how='outer')
################################################################################################
f, ax = plt.subplots()
ax.plot(data.tail(1000000)[['BID_PRICE_1','ASK_PRICE_1']])
ax.plot(data.tail(1000000)[['rate']],marker ='o')
################################################################################################

files = os.listdir(r'C:\Research\data\LMAX data\Tick data 0207-0707\USDSGD\\')
data = pd.DataFrame()
for i in range(0,len(files)):
    data1 = pd.read_csv(r'C:\Research\data\LMAX data\Tick data 0207-0707\USDSGD\\'+files[i], header = 0)
    data = pd.concat([data, data1], axis=0)

data.index = pd.to_datetime(data['TIMESTAMP'], unit='ms')
data.columns = ['time','bid','bid_size','ask','ask_size']


p = data['bid'].resample('60S', base = 0).last().fillna(method='pad').shift(1)
strategy2 = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy2.get_strategy_weight_for_sample(order_type='mkt', signal=p.pct_change(), estimated_tc=TC_by_hour,
                                        ask_low=p, bid_high=p, mid_price=p, tick_incremental=0.0000,
                                        commission=0.09, nav=1, returns=pd.Series(p.pct_change().values, p.index),
                                        alpha_type='simple', hour_select_ao=hour_skip_ao, hour_select_oa=hour_skip_oa)