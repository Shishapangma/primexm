import os
import pandas as pd
import numpy as np
import datetime
import production.stat_functions as sf
from research.strategy_wrapper_research import *
import research.fx_performance as fx
import matplotlib.pyplot as plt
import research.PerformanceMeasures as pm
import research.plot_functions.plot_functions as pf

#############################################################################################
# LMAX quote
quote_data = pd.read_csv(r'C:\Research\data\LMAX data\Research\audusd.csv', header=0)
quote_data.index = pd.to_datetime(quote_data['TIMESTAMP'], unit='ms')
del quote_data['Unnamed: 0']
quote_data.columns = ['time', 'bid', 'bid_size', 'ask', 'ask_size']
del quote_data['time']
quote_data = pm.fx_data_cleaning(quote_data)

quote_data_1t = quote_data.resample('1T').last().fillna(method='pad').shift(1)
max = quote_data.resample('1T').max().fillna(method='pad').shift(1)
min = quote_data.resample('1T').min().fillna(method='pad').shift(1)
rangee = max-min

all_sample = pd.DataFrame(index = quote_data_1t.index)
all_sample['bid'] = quote_data_1t['bid']
all_sample['ask'] = quote_data_1t['ask']
all_sample['max_bid_next'] = max['bid'].shift(-1)
all_sample['min_ask_next'] = min['ask'].shift(-1)
all_sample['Range_bid'] = rangee['bid']
all_sample['Range_ask'] = rangee['ask']

delta = 0.0000
p_fill_buy = ((all_sample['ask'] - delta) >= all_sample['min_ask_next']).sum()/len(all_sample)
p_fill_sell = ((all_sample['bid'] + delta) <= all_sample['max_bid_next']).sum()/len(all_sample)

print('p_fill_buy: ' + str(p_fill_buy))
print('p_fill_sell: ' + str(p_fill_sell))

##############################################################################################
# sub-sample

output_fill_buy = pd.DataFrame(np.zeros((9, 9)))
output_fill_sell = pd.DataFrame(np.zeros((9, 9)))
output_obs_buy = pd.DataFrame(np.zeros((9, 9)))
output_obs_sell = pd.DataFrame(np.zeros((9, 9)))

for i in range(0, 9):
        range_threshold = 0.00005*i
        for j in range(0, 9):
            delta = 0.00001*j
            sub_sample = all_sample[all_sample['Range_ask'] > range_threshold]
            p_fill_buy = ((sub_sample['ask'] - delta) >= sub_sample['min_ask_next']).sum()/len(sub_sample)
            print(str(i))
            print(str(j))
            print('p_fill_buy: ' + str(p_fill_buy))
            print('percent of obs: ' + str(len(sub_sample)/len(all_sample)))
            output_fill_buy.ix[i,j] = p_fill_buy
            output_obs_buy.ix[i,j] = len(sub_sample)/len(all_sample)

            sub_sample = all_sample[all_sample['Range_bid'] > range_threshold]
            p_fill_sell = ((sub_sample['bid'] + delta) <= sub_sample['max_bid_next']).sum()/len(sub_sample)
            print('p_fill_sell: ' + str(p_fill_sell))
            print('percent of obs: ' + str(len(sub_sample)/len(all_sample)))
            output_fill_sell.ix[i,j] = p_fill_sell
            output_obs_sell.ix[i,j] = len(sub_sample)/len(all_sample)

##############################################################################################
d = 0
hour_skip_ao = ['21:00:00', '22:00:00']
hour_skip_oa = ['22:00:00', '23:00:00']
symbols = ['audusd', 'eurchf', 'eurgbp', 'nzdusd', 'usdsgd']
multi = [0.25, 0.9, 0.3, 0.25, 0.75]
tc = pd.read_csv(r'C:\Research\data\Dukascopy\tc_lmax\tc_' + symbols[d] + '.csv', header=0)
TC_by_hour = (tc.ix[:, 0] * 10000).tolist()

range_threshold = 0.0004
p = all_sample['bid']
tick_incremental = pd.Series(0.0000, index=p.index)
tick_incremental[all_sample['Range_bid'] > range_threshold]=0.00001

strategy = CreateStrategy(FxStrategy(AlphaSignal(), TransactionCostModel(kappa=2)))
strategy.get_strategy_weight_for_sample(order_type='lmt',
                                        signal=p.pct_change(),
                                        estimated_tc=TC_by_hour,
                                        ask_low=all_sample['min_ask_next'], bid_high=all_sample['max_bid_next'],
                                        mid_price=p, bid_price=all_sample['bid'], ask_price=all_sample['ask'],
                                        tick_incremental=tick_incremental,
                                        commission=0.09, nav=1,
                                        returns=pd.Series(p.pct_change().values, p.index),
                                        alpha_type='simple',
                                        tc_type='hour',
                                        hour_select_ao=hour_skip_ao,
                                        hour_select_oa=hour_skip_oa,
                                        scale_factor=10)


(strategy.net_return['2010-01-01 23:00:00':]).cumsum().plot()
print(pm.PerformanceStatistics(strategy.net_return, 300 * 60 * 24, 0))