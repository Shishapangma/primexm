FROM python:3.5-slim

RUN mkdir -p /usr/primexm

COPY . /usr/primexm

WORKDIR /usr/primexm

RUN pip install --upgrade pip && pip install -r requirements.txt

CMD ["python", "main.py"]