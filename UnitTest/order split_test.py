def split_order(units):
    """
    split a big order into several smaller orders and put them into queue
    :param units:
    :return:
    """
    order_list = []
    MAX_SIZE = 1000000
    MIN_SIZE = 1000

    # units = round(units / MIN_SIZE, 0) * MIN_SIZE

    steps = units / MAX_SIZE
    # print(steps)

    import math
    n = math.modf(steps)

    t = int(round(n[0] * MAX_SIZE / MIN_SIZE, 0) * MIN_SIZE)
    if t != 0:
        order_list.append(t)

    for i in range(0, int(n[1])):
        order_list.append(MAX_SIZE)

    return order_list


def send_order(order_list):
    while len(order_list) != 0:
        unit = order_list.pop()
        print(unit)


# assert split_order(1000889) == [1000, 1000000]
# assert split_order(7000) == [7000]
# assert split_order(7800) == [8000]
# assert split_order(7400) == [7000]
# assert split_order(9009) == [9000]
# assert split_order(999990) == [1000000]
# assert split_order(979999) == [980000]
# assert split_order(978999) == [979000]
# assert split_order(1978999) == [979000, 1000000]
# assert split_order(4000874) == [1000, 1000000, 1000000, 1000000, 1000000]
# assert split_order(10) == []
# assert split_order(900) == [1000]

send_order(split_order(1978999))